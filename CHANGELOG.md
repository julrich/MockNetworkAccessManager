# Changelog {#changelog}

\brief The changelog of the MockNetworkAccessManager library.

This project adheres to [Semantic Versioning](http://semver.org/).

This changelog follows the [Keep a Changelog](http://keepachangelog.com) format.


---


## [0.12.0] - 2023-11-24 ##

### Added ###
- [#56] Methods to reset the state of the `Manager`: `clearRules()`, `clearRequests()`, `clearUnusedConnections()`,
  `clearAuthenticationCache()` and `resetRuntimeState()`.

### Fixed ###
- [#59] The source code of the examples is now visible in the documentation again.
- [#60] Elements from the `detail` namespaces are now hidden from the API documentation.
- [#61] Replaced usage of `qAsConst()` because it is deprecated since Qt 6.6.
- Incorrect export of `cpp.includePaths` for Qbs >= 1.22.0.


---


## [0.11.1] - 2023-09-13 ##

### Changed ###
- Use Qt 6.2 as default for building and testing on the CI.

### Fixed ###
- [#58] Conversion warnings.


---


## [0.11.0] - 2023-08-11 ##

Major refactoring of the internal architecture (see
[issue #15](https://gitlab.com/julrich/MockNetworkAccessManager/-/issues/15) for details).

### Added ###
- The `contentType` parameter to `MockReplyBuilder::withBody()` to conveniently set the `Content-Type` header along
  with the reply body.
- Support for aborting network replies.

### Changed ###
- Mocked replies no longer have the `QNetworkRequest::OriginalContentLengthAttribute` set by default as it only makes
  sense if compression was used.
- Switched to using `main` as the name of the release timeline branch.

#### ⚠️ Breaking Changes ⚠️ ####
- `Rule::clone()` now returns a `std::unique_ptr` instead of a raw pointer.
- The properties of replies for HTTP and FTP requests are no longer set immediately when the reply is returned from the
  MockNetworkAccess::Manager. Instead, they are set asynchronously after the `QNetworkReply::metaDataChanged()` signal
  is emitted, similar to how a real reply behaves. For unit tests this means that entering the event loop (for example,
  using `QTest::qWait(1)` or `QTRY_VERIFY()`) is now mandatory before the mocked replies have their final properties.   
  Note that this is not the case for file and qrc requests. Their properties are already available when the reply is
  returned.
- Renamed the "pass through" feature to "forwarding". Accordingly, all related methods, types and enums have been
  renamed and some of those names have been improved a bit.

### Fixed ###
- Mocked network replies now also emit the `encrypted()` signal when mocking an HTTPS connection.
- [#15] It is now possible to mock replies in a chain of redirections even if a previous requests was passed through to
  another network access manager.
- Replies for file-like GET and HEAD requests now also emit signals.
- Fixed the behavior of the mocked reply in some edge cases, especially when providing an invalid reply configuration.
- The attributes of mocked network replies now match the attributes of real replies.
- Behavior of aborting and closing the replies.
- [#57] API documentation is now updated again when releasing a new version.

### Removed ###

#### ⚠️ Breaking Changes ⚠️ ####
- Support for older compilers: GCC < 5 and clang < 3.9.
- Support for Qt 5.2 - 5.5.


---


## [0.10.1] - 2022-10-19 ##

### Fixed ###
- [#55] Compiler warnings about deprecated implicitly-declared copy operator.


---


## [0.10.0] - 2022-03-17 ##

### Added ###
- [#49] Testing against Qt 6.2 on the CI.

### Added ###
- `const` overload of `Manager::unmatchedRequestBuilder()`.
 
### Changed ###
- [#48] Use Qt 5.15 as default for building and testing on the CI.
- [#49] Updated [HTTP status codes library][http-status-codes-cpp] to version 1.5.0.

#### ⚠️ Breaking Changes ⚠️ ####
- [#50] Use `std::unique_ptr` instead of `QScopedPointer`. Due to this, C++11 is now required and
  C++98 is no longer supported.

### Fixed ###
- APIs only available with C++11 or later were not included in the Doxygen API documentation.
- `HttpReasonPhraseAttribute` was a `QString` instead of a `QByteArray` when using `MockReplyBuilder::withStatus()`.

### Removed ###
- Support for C++98.

---


## [0.9.1] - 2021-08-19 ##

Hot fix release.

### Fixed ###
- Incompatibility with Qt versions before 5.4.
- GitLab Release process not being executed.


---


## [0.9.0] - 2021-08-19 ##

**⚠️ Warning ⚠️**    
This release has incompatibilities with Qt versions before 5.6. If you are using a version of Qt before 5.6,
**use version [0.9.1] instead**.

### Added ###
- [#23] Support for file://, qrc:// and data:// URL requests.
- [#47] `MockReplyBuilder::withFinishDelayUntil()` to delay the `QNetworkReply::finished()` signal.


---


## [0.8.1] - 2021-05-21 ##

### Fixed ###
- [#46] Fixed broken/lost documentation of `BehaviorFlag` enum and `Predicates::Header` class.
  Also improved documentation in several places.


---


## [0.8.0] - 2021-03-18 ##

API refactoring: the chainable interfaces now use references instead of pointers (see #36).

### Added ###
- [#31] Metatype declaration of MockNetworkAccess::MockReplyBuilder.
  Note: This can be a source breaking change in case the caller code also declare this meta type.
- [#41] Running the tests with address sanitizer, undefined behavior sanitizer and leak sanitizer on the CI.

### Changed ###

#### ⚠️ Breaking Changes ⚠️ ####
- [#35] Unmatched requests are now replied with an error response by default instead of being passed through. The
  default reply builder can be modified via `Manager::unmatchedRequestBuilder()`.
- [#36] The chaining interfaces (`Manager`, `Rule` and `MockReplyBuilder`) now return references
  instead of `QSharedPointer`s. They can also take references in addition to `QSharedPointer`s.

### Removed ###
- [#40] Method `MockReply::errorStringForErrorCode()` which was deprecated in [0.6.0].


---


## [0.7.2] - 2021-03-18 ##

### Fixed ###
- [#45] `Predicates::JsonBodyContaining` incorrectly matched in some situations if a value was expected
  multiple times but only occurred once.


---


## [0.7.1] - 2021-02-19 ##

### Fixed ###
- [#42] Fixed the default error string of `QNetworkReply::ProtocolInvalidOperationError`.


---


## [0.7.0] - 2021-01-21 ##

### Added ###
- [#39] Support for `QT_DISABLE_DEPRECATED_BEFORE`. Usage of deprecated APIs is disabled accordingly to avoid
  deprecation warnings.


---


## [0.6.0] - 2021-01-08 ##

### Added ###
- [#38] Support for `QNetworkAccessManager::setAutoDeleteReplies()` and
  `QNetworkRequest::AutoDeleteReplyOnFinishAttribute` introduced in Qt 5.14.
- `FtpUtils` namespace with methods `FtpUtils::ftpScheme()` and `FtpUtils::ftpsScheme()`.

### Changed ###
- The error strings of the replies (`QIODevice::errorString()`) now match better with error strings returned
  by real QNetworkReplies. In many cases, they will match exactly but this can still not be ensured.
- If the HTTP status code (`MockReplyBuilder::withStatus()`) and the error code
  (`MockReplyBuilder::withError()`) of a `MockReplyBuilder` do not match, a warning is issued to indicate
  this invalid state.

### Deprecated ###
- `MockReply::errorStringForErrorCode()` because the error strings are now set by the Manager before
  returning the reply. This allows to set error strings which better match the one's of real `QNetworkReply`s which
  contain details of the request like the URL.

### Fixed ###
- [#37] Replies always had error string `"Unknown Error"` even if an error string was explicitly set with
  `MockReplyBuilder::withError()`.


---


## [0.5.0] - 2020-12-03 ##

Compatibility release which makes the library compatible with Qt 6.

### Added ###
- [#27] Testing against Qt 5.15.
- [#28] Build files for building with Qbs.
- [#32] Predicates `JsonBody` and `JsonBodyContaining`.
- [#34] `StringDecoder` class to abstract from `QTextCodec` and `QStringDecoder`.

### Changed ###
- [#22] Warning messages are now logged with the logging category "MockNetworkAccessManager".
- [#34] Made library compatible with Qt 6.


---


## [0.4.3] - 2020-09-17 ##

### Added ###
- [#29] Configuration for release-it tool.

### Fixed ###
- [#33] Emit `QNetworkReply::errorOccurred()` for Qt 5.15.0 and later.


---


## [0.4.2] - 2020-07-02 ##

### Changed ###
- [#21] Extended CI testing to more Qt versions.

### Fixed ###
- [#26] Fixed deprecation warning with Qt 5.15.
- [#30] Removed unneeded private members from the documentation.
  Also cleaned and fixed code examples and several other small issues in the documentation.


---


## [0.4.1] - 2020-06-04 ##

### Changed ###
- [#13] Extended unit tests to cover automatic redirection after authentication and vice versa.
- [#20] Assert warning messages in tests using `QTest::ignoreMessage()`.

### Fixed ###
- [#25] MockNetworkAccessManager now also compiles when certain `QT_NO_*` defines are set.
- [!32] Fixed SonarCloud analysis and many code quality issues.


---


## [0.4.0] - 2019-02-04 ##

### Added ###
- [#7] List of known Qt bugs to document flaws of `Behavior_Expected`.
- [#17] Support for multi-valued query parameters (list/array type for query parameter).
- [#18] `passThroughManager` parameter on `Rule::passThrough()` method to allow passing requests through
  to different network access manager based on the rule.

### Fixed ###
- [#7] Handling of relative redirects did not match QNetworkAccessManager's behavior.
- [!25] `MockReply::clone()` and therefore `MockReplyBuilder::createReply()` did not correctly handle
  relative `QNetworkRequest::LocationHeader`s due to QTBUG-41061.


---


## [0.3.0] - 2019-01-05 ##

Improves compatibility with Qt and extends API documentation.

### Added ###
- [#2] Missing API documentation
- [#3] Qt version detection to set default behavior
- `Behavior_NoAutomatic308Redirect` and `Behavior_RedirectWithGet` flags

### Changed ###

#### ⚠️ Breaking Changes ⚠️ ####
- Automatic redirect following now follows redirects with a GET request when an unknown redirect HTTP status code is
  encountered. Before, the same request method as the initial request was used. This makes the behavior now conform to
  Qt 5.9.

### Fixed ###
- [#14] Issues with Qt versions before 5.6

### Removed ###
- `Behavior_Qt_5_6_Redirect` flag in favor of `Behavior_NoAutomatic308Redirect` and
  `Behavior_RedirectWithGet`.


---


## [0.2.0] - 2018-11-13 ##

Predicate enhancements.

### Added ###
- [#8] `Generic` predicate and `Rule::isMatching()` and `Rule::isNotMatching()` methods to allow
  callables as predicates.
- [#12] Predicates `QueryParameter`, `QueryParameterMatching`, `QueryParameterTemplates` and
  `RawHeaderTemplates`

### Removed ###
- [#16] Comparison operators for all predicates.


---


## [0.1.0] - 2018-10-27 ##

### Added ###
- [#1] Cookie support: `MockReplyBuilder::withCookie()`, `Cookie` and `CookieMatching` predicates
- [#6] `\issue` documentation alias

### Changed ###

#### ⚠️ Breaking Changes ⚠️ ####
- [#9] Refactored `Rule::passThrough()` and related methods. Also changed the default behavior if
  `passThrough()` is called without parameters.
- [#10] Renamed `MockConfig` to `Rule`

### Fixed ###
- [#11] Clarified documentation of `Url` predicate regarding empty `QUrl`s


---


## [0.0.2] - 2018-09-30 ##

Initial release.

### Added ###
- Basic predicates
- Support for automatic redirection following
- Support for HSTS
- Support for HTTP Basic authentication


---


[0.12.0]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.12.0
[0.11.1]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.11.1
[0.11.0]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.11.0
[0.10.1]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.10.1
[0.10.0]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.10.0
[0.9.1]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.9.1
[0.9.0]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.9.0
[0.8.1]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.8.1
[0.8.0]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.8.0
[0.7.2]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.7.2
[0.7.1]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.7.1
[0.7.0]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.7.0
[0.6.0]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.6.0
[0.5.0]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.5.0
[0.4.3]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.4.3
[0.4.2]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.4.2
[0.4.1]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.4.1
[0.4.0]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.4.0
[0.3.0]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.3.0
[0.2.0]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.2.0
[0.1.0]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.1.0
[0.0.2]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.0.2
[Unreleased]: https://gitlab.com/julrich/MockNetworkAccessManager/-/compare/main...develop

[http-status-codes-cpp]: https://github.com/j-ulrich/http-status-codes-cpp
