import qbs
import qbs.Utilities

Project {

	property bool enableTests: false

	StaticLibrary {
		name: "MockNetworkAccessManager"
		version: "0.12.0"

		Depends { name: "Qt"
			submodules: [
				"core",
				"network",
			]
		}
		Depends { name: "qbs" }



		Export {
			Depends { name: "cpp" }
			Depends { name: "Qt"
				submodules: [
					"core",
					"network",
				]
			}

			cpp.includePaths: [ Utilities.versionCompare( qbs.version, '1.18.0' ) >= 0 ? exportingProduct.sourceDirectory : product.sourceDirectory ]
		}

		cpp.defines: [ "QT_DEPRECATED_WARNINGS" ]
		cpp.cxxLanguageVersion: [ 'c++11' ]
		cpp.treatWarningsAsErrors: true
		cpp.driverFlags: {
			if( qbs.configurationName === "sanitize" ) {
				if( qbs.toolchain.contains( "msvc" ) ) {
					return [ "/fsanitize=address" ];
				}
				if( qbs.toolchain.contains( "gcc" ) ) {
					return [ "-fsanitize=address,undefined,leak" ];
				}
			}
		}
		cpp.commonCompilerFlags: {
			if( qbs.configurationName === "sanitize" ) {
				if( qbs.toolchain.contains( "gcc" ) ) {
					return [
						"-fno-omit-frame-pointer",
						"-fno-sanitize-recover",
						"-fno-sanitize=alignment"
					];
				}
			}
		}

		files: [
			"MockNetworkAccessManager.hpp"
		]

	}

	references: enableTests ? [ "tests/tests_project.qbs" ] : []
}