# MockNetworkAccessManager #

> Mocking network communication for Qt applications

[![Linux build status](https://gitlab.com/julrich/MockNetworkAccessManager/badges/master/pipeline.svg)](https://gitlab.com/julrich/MockNetworkAccessManager/pipelines?scope=branches)
[![Windows build status](https://ci.appveyor.com/api/projects/status/m0lbburoi1yve1oc/branch/master?svg=true)](https://ci.appveyor.com/project/j-ulrich/mocknetworkaccessmanager/branch/master)
[![Test coverage report](https://julrich.gitlab.io/MockNetworkAccessManager/coverage/test-coverage.svg)](https://julrich.gitlab.io/MockNetworkAccessManager/coverage/)
[![Documentation coverage report](https://julrich.gitlab.io/MockNetworkAccessManager/doc-coverage/doc-coverage.svg)](https://julrich.gitlab.io/MockNetworkAccessManager/doc-coverage/)
[![SonarCloud Status](https://sonarcloud.io/api/project_badges/measure?project=julrich_MockNetworkAccessManager&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=julrich_MockNetworkAccessManager)

[![Doxygen documentation](https://img.shields.io/badge/docs-Doxygen-blue.svg)](https://julrich.gitlab.io/MockNetworkAccessManager/docs/)


## Introduction ##

Dependencies to external services make unit tests fragile and increase the setup effort of the test environments since it must be ensured the service is reachable for the tests and provides the expected (test) data.

Mocking or faking external services using third party software like proxies or local mock/fake servers helps but doesn't solve those issues completely. It can even increase the setup effort of the test environment or make the tests slower due to waits and timeouts.

MockNetworkAccessManager solves these issues by mocking the QNetworkAccessManager in the unit tests and thus, mocking out the network communication completely.


## Features ##

* Mock QNetworkAccessManager and QNetworkReply including headers, attributes and body
* Supports HTTP(S), FTP, `file`, `qrc`, Android `asset` and `data` requests by default
* Flexibly match requests with the help of [over 20 predicates](https://julrich.gitlab.io/MockNetworkAccessManager/docs/namespace_mock_network_access_1_1_predicates.html)
* Forward requests to a real QNetworkAccessManager or another `MockNetworkAccess::Manager`
* Emits signals when a request was received, handled, matched, did not match or was forwarded
* Records all received requests
* Easy to include since it's a single-header-file library
* Simulates HTTP Basic authentication
* Supports automatic redirection handling
* Simulates HTTP Strict Transport Security (HSTS) (requires Qt 5.9 or later)
* Extendable with custom predicates and customizable reply generation


## Example ##

```cpp
#include "MyNetworkClient.hpp"
#include "MockNetworkAccessManager.hpp"
#include <QtTest>

class MyNetworkClientTest : public QObject
{
	Q_OBJECT

private slots:
	void testFetchHello()
	{
		// Create and configure the mocked network access manager
		MockNetworkAccess::Manager< QNetworkAccessManager > mnam;

		mnam.whenGet( QUrl( "http://example.com/hello" ) )
		    .has( MockNetworkAccess::Predicates::HeaderMatching( QNetworkRequest::UserAgentHeader,
		                                                          QRegularExpression( ".*MyNetworkClient/.*" ) ) )
		    .reply().withBody( QJsonDocument::fromJson( "{\"hello\":\"world\"}" ) );

		// Call the method under test
		MyNetworkClient client;
		client.setNetworkAccessManager( &mnam );
		client.fetchHello();

		QTRY_VERIFY( client.fetchFinished() );

		// Verify expectations
		QCOMPARE( mnam.matchedRequests().length(), 1 );
		QCOMPARE( client.hello(), QString( "world" ) );
	}
};
```

(The source code of `MyNetworkClient.hpp` can be found in the [tests directory](tests/MyNetworkClient.hpp))


## Limitations ##

The `MockNetworkAccess::Manager` is intended to be used in unit tests.
It is written to be easy to use and **not** to be secure or performant.
Therefore, it should not be used in a production environment.

Additionally, there are some feature limitations:
- The mocked replies do not emit the implementation specific signals of a real HTTP based QNetworkReply
  (that is the signals of QNetworkReplyHttpImpl).
- Out of the box, only HTTP Basic authentication is supported. However, this should not be a problem in most cases since
  the handling of authentication is normally done internally between the `MockNetworkAccess::Manager` and the
  mocked reply.   
  This is only a limitation if you manually create `Authorization` headers and have to rely on HTTP Digest or NTLM
  authentication.    
  Note that it is still possible to work with any authentication method by matching the `Authorization` header
  manually (for example using [`Predicates::RawHeaderMatching`][RawHeaderMatching]) or by implementing a [custom predicate].
  This is especially easy for bearer token authentication when using dummy tokens. See the [BearerAuthentication example][bearerAuthExample].
- Proxy authentication is not supported at the moment.
- `QNetworkRequest::UserVerifiedRedirectPolicy` is not supported at the moment (see [issue #53][issue-53]).
- The default error messages of the replies (`QIODevice::errorString()`) may be different from the ones of real
  QNetworkReply objects. To get a defined error string, set an error string explicitly using
  `MockReplyBuilder::withError()`.
- `QNetworkReply::setReadBufferSize()` is ignored at the moment.
- `QNetworkAccessManager::setTransferTimeout()` is ignored at the moment.
- `QNetworkAccessManager::clearAccessCache()` and `QNetworkAccessManager::clearConnectionCache()` are ignored at the moment.
  As alternative, use `Manager::clearAuthenticationCache()` and `Manager::clearUnusedConnections()`.

Some of these limitations might be removed in future versions. Feel free to send a feature request if you hit one
these limitations.

[bearerAuthExample]: https://julrich.gitlab.io/MockNetworkAccessManager/docs/_bearer_authentication_test_8cpp-example.html
[issue-53]: https://gitlab.com/julrich/MockNetworkAccessManager/-/issues/53
[RawHeaderMatching]: https://julrich.gitlab.io/MockNetworkAccessManager/docs/class_mock_network_access_1_1_predicates_1_1_raw_header_matching.html
[custom predicate]: https://julrich.gitlab.io/MockNetworkAccessManager/docs/class_mock_network_access_1_1_rule_1_1_predicate.html


## Requirements ##

* Qt 5.6 or later
  - ✅ Qt 6 is also supported.
* Possibility to "inject" a QNetworkAccessManager into the application under test
* Compiler supporting C++11
  - Note that GCC < 5 and clang < 3.9 are not supported anymore.


## Documentation ##

- [API Documentation](https://julrich.gitlab.io/MockNetworkAccessManager/docs/)
- [Changelog](CHANGELOG.md)


## License ##

Copyright (c) 2018-2023 Jochen Ulrich

Licensed under [MIT license](LICENSE).

### Third Party Licenses ###
The tests are using the [Hippomocks](https://github.com/dascandy/hippomocks) library under the [LGPLv3](https://www.gnu.org/licenses/lgpl-3.0.html) license.
A copy of the Hippomocks library, the GPLv3 and the LGPLv3 can be found in the [tests directory](tests).
