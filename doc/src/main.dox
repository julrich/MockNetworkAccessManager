namespace MockNetworkAccess {

/*!

\mainpage %MockNetworkAccessManager

## Introduction ##
The %MockNetworkAccessManager library provides the possibility to mock network communication via
a QNetworkAccessManager or a derived class.
This is achieved by using an instance of a MockNetworkAccess::Manager instead of a real QNetworkAccessManager.
The MockNetworkAccess::Manager does not send the requests to the network but instead serves
pre-configured responses to matching requests.

The usage pattern is as follows:
- Create a MockNetworkAccess::Manager:
\snippet simpleUsage.cpp Create Manager
\note MockNetworkAccess::Manager is a mix-in class. This means it can be used "on top" of any class inheriting
QNetworkAccessManager, including customized implementations of QNetworkAccessManager. But for most cases, a regular
QNetworkAccessManager is enough.
\n

- Configure one or more \link Rule Rules \endlink to define the responses for matching requests:
\snippet simpleUsage.cpp Configure Manager
\n

- Make the unit under test (`MyNetworkClient` in this example) use the MockNetworkAccess::Manager instead of a real
QNetworkAccessManager:
\snippet simpleUsage.cpp Inject Manager
\n

- Execute the behavior to be tested:
\snippet simpleUsage.cpp Execute
\note The signals of the QNetworkReply are emitted asynchronously (Qt::QueuedConnection).
So we need to process events before we can evaluate the expectations.
\n

- Evaluate the expectations:
\snippet simpleUsage.cpp Evaluate
\n

For a complete example, see \ref MyNetworkClientTest.cpp.   

## Further Reading ##

For more details have a look at the documentation of the Manager, Rule and MockReplyBuilder classes.

For guides on more advanced topics, have a look at the [related pages](pages.html) list.


*/

} // namespace MockNetworkAccess