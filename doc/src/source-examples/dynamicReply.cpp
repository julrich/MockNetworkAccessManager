#include "MockNetworkAccessManager.hpp"

//! [Rule Definition]

using namespace MockNetworkAccess;

class EchoService : public Rule
{
public:
	std::unique_ptr< MockReply > createReply( const Request& request ) override
	{
		QJsonObject response;
		response.insert( "method", method( request ) );
		response.insert( "headers", headers( request ) );
		response.insert( "body", QString::fromLatin1( request.body.toBase64() ) );
		response.insert( "timestamp", QDateTime::currentDateTime().toString() );

		MockReplyBuilder builder;
		builder.withBody( QJsonDocument( response ) );

		return builder.createReply();
	}

	QString method( const Request &request )
	{
		switch ( request.operation )
		{
		case QNetworkAccessManager::GetOperation:     return "GET";
		case QNetworkAccessManager::PostOperation:    return "POST";
		case QNetworkAccessManager::PutOperation:     return "PUT";
		case QNetworkAccessManager::DeleteOperation:  return "DELETE";
		case QNetworkAccessManager::HeadOperation:    return "HEAD";
		case QNetworkAccessManager::CustomOperation:
			return QString::fromLatin1(
			    request.qRequest.attribute( QNetworkRequest::CustomVerbAttribute ).toByteArray() );
		default:
			return QString();
		}
	}

	QJsonObject headers( const Request &request )
	{
		QJsonObject headers;
		const auto headerNames = request.qRequest.rawHeaderList();
		for ( auto&& headerName : headerNames )
		{
			const auto headerValue = QString::fromLatin1( request.qRequest.rawHeader( headerName ) );
			headers.insert( headerName, headerValue );
		}
		return headers;
	}
};

//! [Rule Definition]

int main( int argc, char *argv[] )
{
	//! [main]

	EchoService::Ptr echo( new EchoService );
	echo->has( Predicates::UrlMatching( QRegularExpression( ".*/echo$" ) ) );

	Manager< QNetworkAccessManager > mnam;
	mnam.addRule( echo );

	//! [main]

	return 0;
}
