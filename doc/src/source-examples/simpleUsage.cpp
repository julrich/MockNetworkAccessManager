#include "MockNetworkAccessManager.hpp"
#include <QNetworkAccessManager>
#include <QPointer>
#include <QCoreApplication>
#include <iostream>
#include <memory>

class MyNetworkClient : public QObject
{
	Q_OBJECT

public:
	void setNetworkAccessManager( QNetworkAccessManager *nam )
	{
		m_nam = nam;
	}

	void sendRequest()
	{
		m_reply = m_nam->get( QNetworkRequest( QUrl( "http://example.com" ) ) );
		QObject::connect( m_reply.data(), SIGNAL( finished() ), this, SLOT( handleReplyFinished() ) );
	}

	QByteArray getResponse() const
	{
		return m_response;
	}

signals:
	void finished() const;

private slots:
	void handleReplyFinished()
	{
		if ( m_reply->error() == QNetworkReply::NoError )
		{
			m_response = m_reply->readAll();
		}
		m_reply.release()->deleteLater();
		emit finished();
	}

private:
	QPointer< QNetworkAccessManager > m_nam;
	std::unique_ptr< QNetworkReply > m_reply;
	QByteArray m_response;
};


int main( int argc, char *argv[] )
{
	QCoreApplication app( argc, argv );

	using namespace MockNetworkAccess;

	//! [Create Manager]

	MockNetworkAccess::Manager< QNetworkAccessManager > mockNam;

	//! [Create Manager]

	//! [Configure Manager]

	mockNam.whenGet( QUrl( "http://example.com" ) )
	       .reply().withBody( "foo" );

	//! [Configure Manager]

	//! [Inject Manager]

	MyNetworkClient client;
	client.setNetworkAccessManager( &mockNam );

	//! [Inject Manager]

	QObject::connect( &client, SIGNAL( finished() ), &app, SLOT( quit() ) );

	//! [Execute]

	client.sendRequest();

	const int returnCode = QCoreApplication::exec();

	//! [Execute]

	//! [Evaluate]

	Q_ASSERT( mockNam.matchedRequests().length() == 1 );
	Q_ASSERT( client.getResponse() == QByteArray( "foo" ) );

	//! [Evaluate]

	return returnCode;
}
