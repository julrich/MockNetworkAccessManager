/*! \file
 */
#include "MockNetworkAccessManager.hpp"
#include <QtTest>

namespace Tests
{

/*! This is an example class demonstrating how to mock bearer token authentication with a MockNetworkAccess::Manager.
 *
 * The \ref BearerAuthenticationTest.cpp source code is included as an example in this documentation.
 */
class BearerAuthenticationTest : public QObject
{
	Q_OBJECT

private slots:
	void testBearerAuthentication()
	{
		// Create and configure mocked network access manager
		MockNetworkAccess::Manager< QNetworkAccessManager > mnam;

		const QUrl protectedUrl( "https://example.com/protected" );

		const auto bearerAuthHeader = QByteArrayLiteral( "Bearer a-dummy-token" );
		const MockNetworkAccess::Predicates::RawHeader accessTokenHeader( MockNetworkAccess::HttpUtils::authorizationHeader(),
		                                                                  bearerAuthHeader );
		const auto protectedContent = QByteArrayLiteral( "Protected content" );

		mnam.whenGet( protectedUrl )
		    .hasNot( accessTokenHeader )
		    .reply().withStatus( MockNetworkAccess::HttpStatus::Unauthorized )
		            .withRawHeader( MockNetworkAccess::HttpUtils::wwwAuthenticateHeader(),
		                            "Bearer realm=\"example-protected\"" );

		mnam.whenGet( protectedUrl )
		    .has( accessTokenHeader )
		    .reply().withBody( protectedContent, "text/plain; charset=utf-8" );

		// Send requests
		QNetworkRequest unauthorizedRequest{ protectedUrl };
		std::unique_ptr< QNetworkReply > unAuthorizedReply{ mnam.get( unauthorizedRequest ) };
		QTRY_VERIFY( unAuthorizedReply->isFinished() );

		QNetworkRequest authorizedRequest{ protectedUrl };
		authorizedRequest.setRawHeader( MockNetworkAccess::HttpUtils::authorizationHeader(), bearerAuthHeader );
		std::unique_ptr< QNetworkReply > authorizedReply{ mnam.get( authorizedRequest ) };
		QTRY_VERIFY( authorizedReply->isFinished() );

		// Verify expectations
		QCOMPARE( unAuthorizedReply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt(),
		          static_cast< int >( MockNetworkAccess::HttpStatus::Unauthorized ) );
		QCOMPARE( authorizedReply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt(),
		          static_cast< int >( MockNetworkAccess::HttpStatus::OK ) );
		QCOMPARE( authorizedReply->readAll(), protectedContent );
	}
};

} // namespace Tests

QTEST_MAIN( Tests::BearerAuthenticationTest )
#include "BearerAuthenticationTest.moc"
