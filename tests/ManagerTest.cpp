#include "MockNetworkAccessManager.hpp"
#include "TestUtils.hpp"

#include "QSignalInspector.hpp"

#include <QNetworkAccessManager>
#include <QSharedPointer>
#include <QUuid>
#include <QtTest>

#include "hippomocks.h"

#include <cstdlib>
#include <random>

namespace Tests {

/*! Implements unit tests for the Manager class.
 */
class ManagerTest : public QObject
{
	Q_OBJECT

public:
#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
	ManagerTest( QObject* parent = Q_NULLPTR )
	    : QObject( parent )
	    , m_credentialMutex( QMutex::Recursive )
	{
	}
#else
	ManagerTest( QObject* parent = Q_NULLPTR )
	    : QObject( parent )
	{
	}
#endif

private slots:
	void initTestCase();

	void testGetDefaultBehavior();
	void testGetDefaultBehavior_data();
	void testSetAddClearRules();
	void testWhenMethods();
	void testCreateRequest();
	void testCreateRequest_data();
	void testUnmatchedRequests_Forward();
	void testUnmatchedRequests_Forward_data();
	void testUnmatchedRequests_PredefinedReply();
	void testUnmatchedRequests_PredefinedReply_data();
	void testUnmatchedRequestsDefaultReply();
	void testUnmatchedRequestsModifyDefaultReply();
	void testForward();
	void testForward_data();
	void testForwardingTargetNam();
	void testForwardingTargetNam_data();
	void testForwardRuleNam();
	void testForwardRuleNam_data();
	void testManagerSignals();
	void testManagerSignals_data();
	void testRequestEqualityOperators_data();
	void testRequestEqualityOperators();
	void testRequestRecording_data();
	void testReplyProperties();
	void testReplyProperties_data();
	void testUserDefinedAttributes();
	void testReplySignals();
	void testReplySignals_data();
	void testReadReplyData();
	void testDirectAbort();
	void testAbort();
	void testAbort_data();
	void testAbortAfterFinish();
	void testReplyOpen();
	void testReplyClose();
	void testParallelRequests();
	void testParallelRequests_data();
	void testConnectionReuse();
	void testConnectionClose();
	void testClearUnusedConnections();
	void testAbortRequestWhileConnectionIsQueued();
	void testRequestRecording();
	void testMultipleRequestsRecording();
	void testAuthentication();
	void testAuthentication_data();
	void testAuthenticationCache();
	void testAuthenticationCache_data();
	void testClearAuthenticationCache();
	void testCookies();
	void testCookies_data();
	void testUsesSafeRedirectRequestMethod();
	void testUsesSafeRedirectRequestMethod_data();
	void testAutoRedirect();
	void testAutoRedirect_data();
	void testRedirectAndAuthentication();
	void testRedirectAndAuthentication_data();
	void testForwardAndRedirect();
	void testForwardAndRedirect_data();
	void testForwardAndRedirectWithRealNam();
	void testResetRuntimeState();

#if QT_VERSION >= QT_VERSION_CHECK( 5,9,0 )
	void testHsts();
	void testHsts_data();
	void testHstsPolicyExpiry();
	void testHstsPolicyExpiry_data();
	void testHstsResponse();
	void testHstsResponse_data();
	void testInvalidHstsHeaders();
	void testInvalidHstsHeaders_data();
#endif // Qt >= 5.9.0

#if QT_VERSION >= QT_VERSION_CHECK( 5,14,0 )
	void testAutoDeleteReplies();
#endif // Qt >= 5.14.0


protected slots:
	void authenticate( QNetworkReply*, QAuthenticator* authenticator )
	{
		QMutexLocker locker( &m_credentialMutex );
		authenticator->setUser( m_username );
		authenticator->setPassword( m_password );
		m_lastAuthRealm = authenticator->realm();
	}
	void proxyAuthenticate( const QNetworkProxy&, QAuthenticator* authenticator )
	{
		QMutexLocker locker( &m_credentialMutex );
		authenticator->setUser( m_proxyUsername );
		authenticator->setPassword( m_proxyPassword );
		m_lastAuthRealm = authenticator->realm();
	}

private:
#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
	QMutex m_credentialMutex;
#else
	QRecursiveMutex m_credentialMutex;
#endif
	QString m_username;
	QString m_password;
	QString m_lastAuthRealm;
	QString m_proxyUsername;
	QString m_proxyPassword;
	QString m_lastProxyAuthRealm;
};


// ####### Helpers #######

using namespace MockNetworkAccess;
using namespace MockNetworkAccess::Predicates;

using RuleVector = QVector< Rule::Ptr >;
using RequestVector = QVector< Request >;
using ReplyPtr = std::unique_ptr< QNetworkReply >;
using ReplyVector = std::vector< ReplyPtr >;
using UrlVector = QVector< QUrl >;
using StringPair = QPair< QString, QString >;
#if QT_VERSION >= QT_VERSION_CHECK( 5,9,0 )
using HstsPolicyVector = QVector< QHstsPolicy >;
#endif // Qt >= 5.9.0

bool compareRequests( const Request& left, const Request& right )
{
	if ( left.operation != right.operation )
		return false; // LCOV_EXCL_LINE

	if ( left.qRequest != right.qRequest )
		return false; // LCOV_EXCL_LINE

	if ( left.body != right.body )
		return false; // LCOV_EXCL_LINE

	qint64 timestampDiff = qAbs( left.timestamp.msecsTo( right.timestamp ) );
	if ( timestampDiff > 100 )
		return false; // LCOV_EXCL_LINE

	return true;
}

bool compareRequestVectors( const RequestVector& left, const RequestVector& right )
{
	if ( left.size() != right.size() )
		return false; // LCOV_EXCL_LINE

	for ( int i = 0; i < left.size(); ++i )
	{
		if ( ! compareRequests( left.at( i ), right.at( i ) ) )
			return false; // LCOV_EXCL_LINE
	}

	return true;
}

void updateRequestTimestamps( RequestVector& requests )
{
	const auto now = QDateTime::currentDateTime();
	for ( auto&& request : requests )
		request.timestamp = now;
}

RequestVector getRequestsFromSignalEmissions( const QList< QList< QVariant > >& signalEmissions )
{
	RequestVector requests;

	for ( auto&& emission : signalEmissions )
		requests.append( emission.first().value< Request >() );

	return requests;
}

class NetworkReplyTestHelper : public NetworkReply
{
	Q_OBJECT

public:
	/* Hack to access protected methods of base class: https://stackoverflow.com/a/23904680/490560
	 * For this to work, NetworkReplyTestHelper may not implement methods with the same name
	 * as the protected methods of NetworkReply we want to call.
	 * Therefore, we use a "call" prefix.
	 */

	static QNetworkReply* callWrappedReply( NetworkReply* reply )
	{
		return ( reply->*&NetworkReplyTestHelper::wrappedReply )();
	}
};

} // namespace Tests


Q_DECLARE_METATYPE( Tests::RuleVector )
#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
Q_DECLARE_METATYPE( Tests::RequestVector )
#endif // Qt < 6.0.0
Q_DECLARE_METATYPE( Tests::UrlVector )
Q_DECLARE_METATYPE( Tests::StringPair )
Q_DECLARE_METATYPE( Tests::DummyNetworkAccessManager::Ptr )
Q_DECLARE_METATYPE( MockNetworkAccess::UnmatchedRequestBehavior )
Q_DECLARE_METATYPE( MockNetworkAccess::BehaviorFlags )
Q_DECLARE_METATYPE( MockNetworkAccess::AttributeHash )
Q_DECLARE_METATYPE( QNetworkReply* )  // LCOV_EXCL_LINE
Q_DECLARE_METATYPE( QAuthenticator* ) // LCOV_EXCL_LINE
Q_DECLARE_METATYPE( QMetaMethod )     // LCOV_EXCL_LINE

#if QT_VERSION >= QT_VERSION_CHECK( 5,9,0 )
Q_DECLARE_METATYPE( QHstsPolicy ) // LCOV_EXCL_LINE
Q_DECLARE_METATYPE( Tests::HstsPolicyVector )
#endif // Qt >= 5.9.0


namespace Tests {

#if QT_VERSION >= QT_VERSION_CHECK( 5,9,0 )
const int manualRedirectPolicy = static_cast< int >( QNetworkRequest::ManualRedirectPolicy );
#else
const int manualRedirectPolicy = 0;
#endif // Qt >= 5.9.0

const auto& staticReplyMetaObj = QNetworkReply::staticMetaObject;

const auto encryptedSignal = QMetaMethod::fromSignal( &QNetworkReply::encrypted );
const auto metaDataChangedSignal = QMetaMethod::fromSignal( &QNetworkReply::metaDataChanged );
const auto uploadProgressSignal = QMetaMethod::fromSignal( &QNetworkReply::uploadProgress );
const auto downloadProgressSignal = QMetaMethod::fromSignal( &QNetworkReply::downloadProgress );
const auto finishedSignal = QMetaMethod::fromSignal( &QNetworkReply::finished );
const auto aboutToCloseSignal = QMetaMethod::fromSignal( &QNetworkReply::aboutToClose );
#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
const auto errorSignal = getSignal( staticReplyMetaObj, "error(QNetworkReply::NetworkError)" );
#endif // Qt < 6.0.0
#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
const auto errorOccurredSignal = QMetaMethod::fromSignal( &QNetworkReply::errorOccurred );
#endif // Qt >= 5.15.0
const auto readyReadSignal = QMetaMethod::fromSignal( &QNetworkReply::readyRead );
const auto readChannelFinishedSignal = QMetaMethod::fromSignal( &QNetworkReply::readChannelFinished );
const auto redirectedSignal = QMetaMethod::fromSignal( &QNetworkReply::redirected );

const QUrl exampleRootUrl( "http://example.com" );
const QUrl httpsExampleRootUrl( "https://example.com" );
const QUrl httpsOtherOriginUrl( "https://example.com:8080" );
const QUrl fooUrl( "http://example.com/foo" );
const QUrl httpsFooUrl( "https://example.com/foo" );
const QUrl ftpUrl( "ftp://user:pass@example.com/path/to/file" );
const QUrl customSchemeUrl( "foo://something" );
const QUrl dataFooObjectUrl( "data:application/json;base64,eyAiZm9vIjogImJhciIgfQ==" );
const QUrl fileUrl( "file:///path/to/file" );
const QUrl qrcUrl( "qrc:///path/to/file" );
const auto noError = QNetworkReply::NoError;
const auto hostNotFoundError = QNetworkReply::HostNotFoundError;
const auto protocolUnknownError = QNetworkReply::ProtocolUnknownError;
const QByteArray statusOkJson( "{ \"status\": \"OK\" }" );
const QByteArray fooObject( "{ \"foo\": \"bar\" }" );
const QByteArray cookieCreated( "cookie created" );
const QByteArray cookieSent( "cookie sent" );
const auto statusOkVariant = QVariant{ static_cast< int >( HttpStatus::OK ) };

const auto noSupportedAuthChallengeTemplate = QStringLiteral(
    "Cannot authenticate request because there is no supported authentication challenge in reply from URL \"%1\"" );


const Request getExampleRootRequest( QNetworkRequest( exampleRootUrl ), QNetworkAccessManager::GetOperation );
const Request getFooRequest( QNetworkRequest( fooUrl ), QNetworkAccessManager::GetOperation );
const Request customSchemeRequest( QNetworkRequest( customSchemeUrl ), QNetworkAccessManager::GetOperation );
const Request httpsGetExampleRootRequest( QNetworkRequest( httpsExampleRootUrl ), QNetworkAccessManager::GetOperation );

AttributeHash defaultAttributes( int statusCode = HttpStatus::OK, QString reasonPhrase = {}, bool encrypted = false )
{
	if ( reasonPhrase.isNull() )
		reasonPhrase = HttpStatus::reasonPhrase( statusCode );
	return AttributeHash
	{
		{ QNetworkRequest::HttpStatusCodeAttribute, statusCode },
		    { QNetworkRequest::HttpReasonPhraseAttribute, reasonPhrase.toLatin1() },
		    { QNetworkRequest::ConnectionEncryptedAttribute, encrypted },
		    { QNetworkRequest::HttpPipeliningWasUsedAttribute, false },
#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		    { QNetworkRequest::SpdyWasUsedAttribute, false },
#endif
#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		    { QNetworkRequest::Http2WasUsedAttribute, false },
#elif QT_VERSION >= QT_VERSION_CHECK( 5,9,0 )
		    { QNetworkRequest::HTTP2WasUsedAttribute, false },
#endif // Qt >= 5.9.0
	};
}

Request postFooRequest()
{
	Request postFoo( QNetworkAccessManager::PostOperation, QNetworkRequest( fooUrl ), fooObject );
	postFoo.qRequest.setHeader( QNetworkRequest::ContentTypeHeader, QByteArray( "application/json" ) );
	return postFoo;
}

Request ftpRequest()
{
	Request ftpReq( QNetworkRequest( ftpUrl ), QNetworkAccessManager::CustomOperation );
	ftpReq.qRequest.setAttribute( QNetworkRequest::CustomVerbAttribute, QByteArray( "LIST" ) );
	return ftpReq;
}


Rule::Ptr getExampleRootRule()
{
	auto getExampleRoot = Rule::Ptr::create();
	getExampleRoot->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( exampleRootUrl ) )
	    .reply()
	    .withBody( statusOkJson );
	return getExampleRoot;
}

Rule::Ptr postFooRule()
{
	auto postFoo = Rule::Ptr::create();
	postFoo->has( Verb( QNetworkAccessManager::PostOperation ) )
	    .has( Url( fooUrl ) )
	    .has( Body( fooObject ) )
	    .reply()
	    .withBody( statusOkJson );
	return postFoo;
}

Rule::Ptr catchAllRule()
{
	auto catchAll = Rule::Ptr::create();
	catchAll->has( Anything() ).reply().withError( hostNotFoundError );
	return catchAll;
}

Rule::Ptr getFooRule()
{
	auto getFoo = Rule::Ptr::create();
	getFoo->has( Verb( QNetworkAccessManager::GetOperation ) ).has( Url( fooUrl ) ).reply().withStatus( HttpStatus::OK );
	return getFoo;
}

Rule::Ptr gerToFooRedirectRule()
{
	auto gerRedirect = Rule::Ptr::create();
	gerRedirect->has( Verb( QNetworkAccessManager::GetOperation ) ).has( Url( exampleRootUrl ) ).reply().withRedirect( fooUrl );
	return gerRedirect;
}

const QUrl barUrl( "http://example.com/bar" );
Rule::Ptr getFooRedirectRule()
{
	auto getFooRedirect = Rule::Ptr::create();
	getFooRedirect->has( Verb( QNetworkAccessManager::GetOperation ) ).has( Url( fooUrl ) ).reply().withRedirect( barUrl );
	return getFooRedirect;
}

const QByteArray barPayload( "bar" );
Rule::Ptr getBarRule()
{
	auto getBar = Rule::Ptr::create();
	getBar->has( Verb( QNetworkAccessManager::GetOperation ) ).has( Url( barUrl ) ).reply().withBody( barPayload );
	return getBar;
}

Rule::Ptr getFileRule()
{
	auto getFile = Rule::Ptr::create();
	getFile->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( fileUrl ) )
	    .reply()
	    .withStatus( HttpStatus::OK )
	    .withBody( statusOkJson );
	return getFile;
}

void enableAutomaticRedirect( QNetworkRequest& request, bool enable = true )
{
#if QT_VERSION < QT_VERSION_CHECK( 5,15,2 )
	request.setAttribute( QNetworkRequest::FollowRedirectsAttribute, enable );
#else
	request.setAttribute( QNetworkRequest::RedirectPolicyAttribute,
	                      enable ? QNetworkRequest::NoLessSafeRedirectPolicy : QNetworkRequest::ManualRedirectPolicy );
#endif
}

Request gerFollowRedirectRequest()
{
	auto gerFollowRedirectRequest = getExampleRootRequest;
	enableAutomaticRedirect( gerFollowRedirectRequest.qRequest );
	return gerFollowRedirectRequest;
}


// ####### Administration #######

void ManagerTest::initTestCase()
{
	qRegisterMetaType< QNetworkReply* >();
	qRegisterMetaType< QAuthenticator* >();
}


// ####### Tests #######

/*! \test %Tests the Manager::getDefaultBehavior() method and the default behavior flags of the Manager.
 */
void ManagerTest::testGetDefaultBehavior()
{
#ifdef _HIPPOMOCKS__ENABLE_CFUNC_MOCKING_SUPPORT
	QFETCH( QString, qtVersion );

	const QByteArray qtVersionLatin1 = qtVersion.toLatin1();
	HippoMocks::MockRepository mocks;
	mocks.OnCallFunc( qVersion ).Return( qtVersionLatin1.constData() );

	Manager< QNetworkAccessManager > manager;
	QTEST( MockNetworkAccess::getDefaultBehaviorFlags(), "expectedBehavior" );
	QCOMPARE( manager.behaviorFlags(), MockNetworkAccess::getDefaultBehaviorFlags() );
#endif
}

/*! Provides the data for the testGetDefaultBehavior() test.
 */
void ManagerTest::testGetDefaultBehavior_data()
{
#ifndef _HIPPOMOCKS__ENABLE_CFUNC_MOCKING_SUPPORT
	QSKIP( "This test requires HippoMocks C function mocking support" );
#endif

	QTest::addColumn< QString >( "qtVersion" );
	QTest::addColumn< BehaviorFlags >( "expectedBehavior" );

	QTest::newRow( "Qt 5.2.0" ) << "5.2.0" << static_cast< BehaviorFlags >( Behavior_Qt_5_2_0 );
	QTest::newRow( "Qt 5.6.0" ) << "5.6.0" << static_cast< BehaviorFlags >( Behavior_Qt_5_6_0 );
	QTest::newRow( "Qt 5.6.1" ) << "5.6.1" << static_cast< BehaviorFlags >( Behavior_Qt_5_6_0 );
}

/*! \test %Tests the Manager::rules(), Manager::setRules(),
 * Manager::addRule() and Manager::clearRules() methods.
 */
void ManagerTest::testSetAddClearRules()
{
	Manager< QNetworkAccessManager > mnam;

	QVector< Rule::Ptr > rules;
	Rule::Ptr getRule = Rule::Ptr::create();
	getRule->has( Verb( QNetworkAccessManager::GetOperation ) );
	rules.append( getRule );
	Rule::Ptr urlMatchingRule = Rule::Ptr::create();
	urlMatchingRule->has( UrlMatching( QRegularExpression( "^https?://example.com([/#?].*)" ) ) );
	rules.append( urlMatchingRule );

	QVERIFY( mnam.rules().isEmpty() );

	Rule::Ptr urlRule = Rule::Ptr::create();
	urlRule->has( Url( "http://example.com" ) );

	mnam.addRule( urlRule );
	QCOMPARE( mnam.rules().size(), 1 );
	QCOMPARE( mnam.rules().at( 0 ), urlRule );

	mnam.setRules( rules );
	QCOMPARE( mnam.rules(), rules );

	mnam.addRule( urlRule );
	QCOMPARE( mnam.rules().size(), rules.size() + 1 );

	mnam.setRules( QVector< Rule::Ptr >() );
	QVERIFY( mnam.rules().isEmpty() );

	mnam.addRule( *urlRule );
	QCOMPARE( mnam.rules().size(), 1 );
	QVERIFY( mnam.rules().at( 0 ).data() != urlRule.data() );

	mnam.clearRules();
	QVERIFY( mnam.rules().empty() );

	mnam.addRule( urlRule );
	QCOMPARE( mnam.rules().size(), 1 );
	QCOMPARE( mnam.rules().at( 0 ), urlRule );
}

/*! \test %Tests the MockNetworkAccessManager::whenGet(), MockNetworkAccessManager::whenPost(),
 * MockNetworkAccessManager::whenPut(), MockNetworkAccessManager::whenDelete(), MockNetworkAccessManager::whenHead()
 * and MockNetworkAccessManager::when() methods.
 */
void ManagerTest::testWhenMethods()
{
	Manager< QNetworkAccessManager > mnam;

	const QRegularExpression urlRegEx( "^https://example.com/foo$" );
	const QUrl url( "http://example.com" );
	const QByteArray customVerb( "FOO" );

	Request fixedUrlRequest = Request( QNetworkRequest( url ) );
	Request regExUrlRequest = Request( QNetworkRequest( QUrl( "https://example.com/foo" ) ) );

	QVector< Rule::Ptr > rules;
	QVector< Rule::Predicate::Ptr > predicates;

	fixedUrlRequest.operation = QNetworkAccessManager::GetOperation;
	regExUrlRequest.operation = QNetworkAccessManager::GetOperation;

	Rule* lastRule = &mnam.whenGet( urlRegEx );
	rules << mnam.rules().last();
	QVERIFY( lastRule == rules.last().data() );
	predicates = lastRule->predicates();
	QVERIFY( predicates.first().dynamicCast< Verb >() );
	QVERIFY( predicates.first().dynamicCast< Verb >()->matches( regExUrlRequest ) );
	QVERIFY( predicates.last().dynamicCast< UrlMatching >() );
	QVERIFY( predicates.last().dynamicCast< UrlMatching >()->matches( regExUrlRequest ) );

	lastRule = &mnam.whenGet( url );
	rules << mnam.rules().last();
	QVERIFY( lastRule == rules.last().data() );
	predicates = lastRule->predicates();
	QVERIFY( predicates.first().dynamicCast< Verb >() );
	QVERIFY( predicates.first().dynamicCast< Verb >()->matches( fixedUrlRequest ) );
	QVERIFY( predicates.last().dynamicCast< Url >() );
	QVERIFY( predicates.last().dynamicCast< Url >()->matches( fixedUrlRequest ) );

	fixedUrlRequest.operation = QNetworkAccessManager::PostOperation;
	regExUrlRequest.operation = QNetworkAccessManager::PostOperation;

	lastRule = &mnam.whenPost( urlRegEx );
	rules << mnam.rules().last();
	QVERIFY( lastRule == rules.last().data() );
	predicates = lastRule->predicates();
	QVERIFY( predicates.first().dynamicCast< Verb >() );
	QVERIFY( predicates.first().dynamicCast< Verb >()->matches( regExUrlRequest ) );
	QVERIFY( predicates.last().dynamicCast< UrlMatching >() );
	QVERIFY( predicates.last().dynamicCast< UrlMatching >()->matches( regExUrlRequest ) );

	lastRule = &mnam.whenPost( url );
	rules << mnam.rules().last();
	QVERIFY( lastRule == rules.last().data() );
	predicates = lastRule->predicates();
	QVERIFY( predicates.first().dynamicCast< Verb >() );
	QVERIFY( predicates.first().dynamicCast< Verb >()->matches( fixedUrlRequest ) );
	QVERIFY( predicates.last().dynamicCast< Url >() );
	QVERIFY( predicates.last().dynamicCast< Url >()->matches( fixedUrlRequest ) );

	fixedUrlRequest.operation = QNetworkAccessManager::PutOperation;
	regExUrlRequest.operation = QNetworkAccessManager::PutOperation;

	lastRule = &mnam.whenPut( urlRegEx );
	rules << mnam.rules().last();
	QVERIFY( lastRule == rules.last().data() );
	predicates = lastRule->predicates();
	QVERIFY( predicates.first().dynamicCast< Verb >() );
	QVERIFY( predicates.first().dynamicCast< Verb >()->matches( regExUrlRequest ) );
	QVERIFY( predicates.last().dynamicCast< UrlMatching >() );
	QVERIFY( predicates.last().dynamicCast< UrlMatching >()->matches( regExUrlRequest ) );

	lastRule = &mnam.whenPut( url );
	rules << mnam.rules().last();
	QVERIFY( lastRule == rules.last().data() );
	predicates = lastRule->predicates();
	QVERIFY( predicates.first().dynamicCast< Verb >() );
	QVERIFY( predicates.first().dynamicCast< Verb >()->matches( fixedUrlRequest ) );
	QVERIFY( predicates.last().dynamicCast< Url >() );
	QVERIFY( predicates.last().dynamicCast< Url >()->matches( fixedUrlRequest ) );

	fixedUrlRequest.operation = QNetworkAccessManager::DeleteOperation;
	regExUrlRequest.operation = QNetworkAccessManager::DeleteOperation;

	lastRule = &mnam.whenDelete( urlRegEx );
	rules << mnam.rules().last();
	QVERIFY( lastRule == rules.last().data() );
	predicates = lastRule->predicates();
	QVERIFY( predicates.first().dynamicCast< Verb >() );
	QVERIFY( predicates.first().dynamicCast< Verb >()->matches( regExUrlRequest ) );
	QVERIFY( predicates.last().dynamicCast< UrlMatching >() );
	QVERIFY( predicates.last().dynamicCast< UrlMatching >()->matches( regExUrlRequest ) );

	lastRule = &mnam.whenDelete( url );
	rules << mnam.rules().last();
	QVERIFY( lastRule == rules.last().data() );
	predicates = lastRule->predicates();
	QVERIFY( predicates.first().dynamicCast< Verb >() );
	QVERIFY( predicates.first().dynamicCast< Verb >()->matches( fixedUrlRequest ) );
	QVERIFY( predicates.last().dynamicCast< Url >() );
	QVERIFY( predicates.last().dynamicCast< Url >()->matches( fixedUrlRequest ) );

	fixedUrlRequest.operation = QNetworkAccessManager::HeadOperation;
	regExUrlRequest.operation = QNetworkAccessManager::HeadOperation;

	lastRule = &mnam.whenHead( urlRegEx );
	rules << mnam.rules().last();
	QVERIFY( lastRule == rules.last().data() );
	predicates = lastRule->predicates();
	QVERIFY( predicates.first().dynamicCast< Verb >() );
	QVERIFY( predicates.first().dynamicCast< Verb >()->matches( regExUrlRequest ) );
	QVERIFY( predicates.last().dynamicCast< UrlMatching >() );
	QVERIFY( predicates.last().dynamicCast< UrlMatching >()->matches( regExUrlRequest ) );

	lastRule = &mnam.whenHead( url );
	rules << mnam.rules().last();
	QVERIFY( lastRule == rules.last().data() );
	predicates = lastRule->predicates();
	QVERIFY( predicates.first().dynamicCast< Verb >() );
	QVERIFY( predicates.first().dynamicCast< Verb >()->matches( fixedUrlRequest ) );
	QVERIFY( predicates.last().dynamicCast< Url >() );
	QVERIFY( predicates.last().dynamicCast< Url >()->matches( fixedUrlRequest ) );

	fixedUrlRequest.operation = QNetworkAccessManager::CustomOperation;
	regExUrlRequest.operation = QNetworkAccessManager::CustomOperation;
	fixedUrlRequest.qRequest.setAttribute( QNetworkRequest::CustomVerbAttribute, customVerb );
	regExUrlRequest.qRequest.setAttribute( QNetworkRequest::CustomVerbAttribute, customVerb );

	lastRule = &mnam.when( QNetworkAccessManager::CustomOperation, urlRegEx, customVerb );
	rules << mnam.rules().last();
	QVERIFY( lastRule == rules.last().data() );
	predicates = lastRule->predicates();
	QVERIFY( predicates.first().dynamicCast< Verb >() );
	QVERIFY( predicates.first().dynamicCast< Verb >()->matches( regExUrlRequest ) );
	QVERIFY( predicates.last().dynamicCast< UrlMatching >() );
	QVERIFY( predicates.last().dynamicCast< UrlMatching >()->matches( regExUrlRequest ) );

	lastRule = &mnam.when( QNetworkAccessManager::CustomOperation, url, customVerb );
	rules << mnam.rules().last();
	QVERIFY( lastRule == rules.last().data() );
	predicates = lastRule->predicates();
	QVERIFY( predicates.first().dynamicCast< Verb >() );
	QVERIFY( predicates.first().dynamicCast< Verb >()->matches( fixedUrlRequest ) );
	QVERIFY( predicates.last().dynamicCast< Url >() );
	QVERIFY( predicates.last().dynamicCast< Url >()->matches( fixedUrlRequest ) );

	QCOMPARE( mnam.rules(), rules );
}


/*! \test %Tests the MockNetworkAccessManager::createRequest() method.
 */
void ManagerTest::testCreateRequest()
{
	QFETCH( Request, request );
	QFETCH( RuleVector, rules );

	Manager< DummyNetworkAccessManager > mnam;
	mnam.setUnmatchedRequestBehavior( Forward );
	mnam.setRules( rules );

	ReplyPtr reply( sendRequest( mnam, request ) );

	QVERIFY( reply.get() );
	QTRY_VERIFY( reply->isFinished() );
	QTEST( reply->error(), "error" );
	QTEST( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ), "expectedStatusCode" );
	QVERIFY( reply->isReadable() );
	QTEST( reply->readAll(), "expectedBody" );
}

/*! Provides the data for the testCreateRequest() test.
 */
void ManagerTest::testCreateRequest_data()
{
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< RuleVector >( "rules" );
	QTest::addColumn< QNetworkReply::NetworkError >( "error" );
	QTest::addColumn< QVariant >( "expectedStatusCode" );
	QTest::addColumn< QByteArray >( "expectedBody" );

	Rule::Ptr noReply( new Rule );
	noReply->has( Anything() );

	Rule::Ptr forwardNoReply( new Rule );
	forwardNoReply->has( Anything() ).forward( Rule::ForwardAndReturnMockReply );

	Rule::Ptr customSchemeReply( new Rule );
	customSchemeReply->has( Url( customSchemeUrl ) ).reply().withBody( fooObject );

	Rule::Ptr ftpReply( new Rule );
	ftpReply->has( UrlMatching( QRegularExpression( "ftp://.*/file" ) ) ).reply().withBody( fooObject );

	Request dataUrlRequest( QNetworkRequest( dataFooObjectUrl ), QNetworkAccessManager::GetOperation );

	Request fileUrlRequest( QNetworkRequest( fileUrl ), QNetworkAccessManager::GetOperation );

	// clang-format off
	//                                   // request               // rules                                                     // error                  // expectedStatusCode // expectedBody
	QTest::newRow( "simple GET" )        << getExampleRootRequest << ( RuleVector() << getExampleRootRule() )                  << noError                << statusOkVariant    << statusOkJson;
	QTest::newRow( "simple POST" )       << postFooRequest()      << ( RuleVector() << postFooRule() )                         << noError                << statusOkVariant    << statusOkJson;
	QTest::newRow( "multiple rules" )    << getExampleRootRequest << ( RuleVector() << postFooRule() << getExampleRootRule() ) << noError                << statusOkVariant    << statusOkJson;
	QTest::newRow( "catch all" )         << getExampleRootRequest << ( RuleVector() << postFooRule() << catchAllRule() )       << hostNotFoundError      << QVariant{}         << QByteArray();
	QTest::newRow( "null rule" )         << getExampleRootRequest << ( RuleVector() << Rule::Ptr::create() << catchAllRule() ) << hostNotFoundError      << QVariant{}         << QByteArray();
	QTest::newRow( "no reply rule" )     << getExampleRootRequest << ( RuleVector() << noReply << catchAllRule() )             << hostNotFoundError      << QVariant{}         << QByteArray();
	QTest::newRow( "unmatched request" ) << getExampleRootRequest << ( RuleVector() << postFooRule() )                         << operationCanceledError << QVariant{}         << QByteArray();
	QTest::newRow( "custom scheme" )     << customSchemeRequest   << ( RuleVector() << customSchemeReply )                     << noError                << statusOkVariant    << fooObject;
	QTest::newRow( "ftp" )               << ftpRequest()          << ( RuleVector() << ftpReply )                              << noError                << statusOkVariant    << fooObject;
	QTest::newRow( "data" )              << dataUrlRequest        << ( RuleVector() )                                          << noError                << QVariant{}         << fooObject;
	QTest::newRow( "forward no reply" )  << getExampleRootRequest << ( RuleVector() << forwardNoReply )                        << operationCanceledError << QVariant{}         << QByteArray();
	// clang-format on
}

/*! \test %Tests the MockNetworkAccessManager::Forward behavior.
 */
void ManagerTest::testUnmatchedRequests_Forward()
{
	QFETCH( Request, request );
	QFETCH( RuleVector, rules );

	Manager< DummyNetworkAccessManager > mnam;
	mnam.setUnmatchedRequestBehavior( Forward );

	ReplyPtr reply( sendRequest( mnam, request ) );
	request.timestamp = QDateTime::currentDateTime();

	QTRY_VERIFY( reply->isFinished() );
	QCOMPARE( reply->error(), operationCanceledError );
	QVERIFY( ! reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).isValid() );
	QCOMPARE( mnam.requests.size(), 1 );
	auto finalRequest = mnam.requests.front(); // This is the request received by the DummyNetworkAccessManager
/* The final request has automatic redirect following disabled to allow mocking responses after forwarding requests.
 */
#if QT_VERSION >= QT_VERSION_CHECK( 5,9,0 )
	finalRequest.qRequest.setAttribute( QNetworkRequest::RedirectPolicyAttribute, QVariant{} );
#else  // Qt < 5.9.0
	finalRequest.qRequest.setAttribute( QNetworkRequest::FollowRedirectsAttribute, QVariant{} );
#endif // Qt < 5.9.0
	QVERIFY( compareRequests( finalRequest, request ) );
}

/*! Provides the data for the testUnmatchedRequests_Forward() test.
 */
void ManagerTest::testUnmatchedRequests_Forward_data()
{
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< RuleVector >( "rules" );

	// clang-format off
	QTest::newRow( "no rules" )         << getExampleRootRequest << RuleVector();
	QTest::newRow( "no matching rule" ) << getExampleRootRequest << ( RuleVector() << postFooRule() );
	// clang-format on
}

/*! \test %Tests the MockNetworkAccessManager::PredefinedReply behavior.
 */
void ManagerTest::testUnmatchedRequests_PredefinedReply()
{
	QFETCH( Request, request );
	QFETCH( RuleVector, rules );

	Manager< DummyNetworkAccessManager > mnam;
	mnam.setUnmatchedRequestBehavior( PredefinedReply );
	mnam.setRules( rules );

	MockReplyBuilder builder;
	builder.withError( hostNotFoundError, "Unmatched request!" );
	mnam.setUnmatchedRequestBuilder( builder );

	auto reply = sendRequest( mnam, request );

	QTRY_VERIFY( reply->isFinished() );
	QCOMPARE( reply->error(), hostNotFoundError );
	QCOMPARE( reply->errorString(), QString( "Unmatched request!" ) );
}

/*! Provides the data for the testUnmatchedRequests_PredefinedReply() test.
 */
void ManagerTest::testUnmatchedRequests_PredefinedReply_data()
{
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< RuleVector >( "rules" );

	// clang-format off
	QTest::newRow( "no rules" )         << getExampleRootRequest << RuleVector();
	QTest::newRow( "no matching rule" ) << getExampleRootRequest << ( RuleVector() << postFooRule() );
	// clang-format on
}

/*! \test %Tests the default reply of the MockNetworkAccessManager::PredefinedReply behavior.
 */
void ManagerTest::testUnmatchedRequestsDefaultReply()
{
	Manager< DummyNetworkAccessManager > mnam;

	auto reply = sendRequest( mnam, getExampleRootRequest );

	QTRY_VERIFY( reply->isFinished() );
	QCOMPARE( reply->error(), QNetworkReply::ContentNotFoundError );
	QCOMPARE( reply->errorString(), QString( "MockNetworkAccessManager: Request did not match any rule" ) );
}

/*! \test %Tests modifying the default reply of the MockNetworkAccessManager::PredefinedReply behavior.
 */
void ManagerTest::testUnmatchedRequestsModifyDefaultReply()
{
	Manager< DummyNetworkAccessManager > mnam;

	const QString errorMessage( "no match" );
	mnam.unmatchedRequestBuilder().withError( QNetworkReply::HostNotFoundError, errorMessage );

	auto reply = sendRequest( mnam, getExampleRootRequest );

	QTRY_VERIFY( reply->isFinished() );
	QCOMPARE( reply->error(), QNetworkReply::HostNotFoundError );
	QCOMPARE( reply->errorString(), errorMessage );
}

/*! \test %Tests the forwarding functionality of the MockNetworkAccessManager.
 */
void ManagerTest::testForward()
{
	QFETCH( Request, request );
	QFETCH( RuleVector, rules );

	Manager< DummyNetworkAccessManager > mnam;
	mnam.setUnmatchedRequestBehavior( Forward );
	mnam.setRules( rules );

	auto reply = sendRequest( mnam, request );
	request.timestamp = QDateTime::currentDateTime();

	QVERIFY( reply.get() );
	QCOMPARE( mnam.requests.size(), 1 );
	QVERIFY( compareRequestEssentials( mnam.requests.last(), request ) );
	auto networkReply = dynamic_cast< NetworkReply* >( reply.get() );
	QVERIFY( networkReply );
	auto wrappedReply = NetworkReplyTestHelper::callWrappedReply( networkReply );
	QTEST( static_cast< bool >( dynamic_cast< MockReply* >( wrappedReply ) ), "expectMockReply" );
}

/*! Provides the data for the testForward() test.
 */
void ManagerTest::testForward_data()
{
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< RuleVector >( "rules" );
	QTest::addColumn< bool >( "expectMockReply" );

	Rule::Ptr gerForwardReturn = Rule::Ptr::create();
	gerForwardReturn->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( exampleRootUrl ) )
	    .forward( Rule::ForwardAndReturnDelegatedReply );

	Rule::Ptr gerForwardMock = Rule::Ptr::create();
	gerForwardMock->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( exampleRootUrl ) )
	    .forward( Rule::ForwardAndReturnMockReply )
	    .reply()
	    .withStatus( HttpStatus::OK );

	const Request putFooRequest( QNetworkAccessManager::PutOperation, QNetworkRequest( fooUrl ), QByteArray( "foo bar" ) );
	const Request headFooRequest( QNetworkAccessManager::HeadOperation, QNetworkRequest( fooUrl ) );
	const Request deleteFooRequest( QNetworkAccessManager::DeleteOperation, QNetworkRequest( fooUrl ) );
	Request customFooRequest( QNetworkAccessManager::CustomOperation, QNetworkRequest( fooUrl ) );
	customFooRequest.qRequest.setAttribute( QNetworkRequest::CustomVerbAttribute, QByteArray( "FOO" ) );

	// clang-format off
	QTest::newRow( "no rules" )             << getExampleRootRequest << RuleVector()                         << false;
	QTest::newRow( "return forwarded" )     << getExampleRootRequest << ( RuleVector() << gerForwardReturn ) << false;
	QTest::newRow( "forward, return mock" ) << getExampleRootRequest << ( RuleVector() << gerForwardMock )   << true;
	QTest::newRow( "no matching rule" )     << getExampleRootRequest << ( RuleVector() << postFooRule() )    << false;
	QTest::newRow( "forward POST" )         << postFooRequest()      << RuleVector()                         << false;
	QTest::newRow( "forward PUT" )          << putFooRequest         << RuleVector()                         << false;
	QTest::newRow( "forward HEAD" )         << headFooRequest        << RuleVector()                         << false;
	QTest::newRow( "forward DELETE" )       << deleteFooRequest      << RuleVector()                         << false;
	QTest::newRow( "forward custom verb" )  << customFooRequest      << RuleVector()                         << false;
	// clang-format on
}

/*! \test %Tests the forwarding functionality of the MockNetworkAccessManager using a separate QNetworkAccessManager.
 */
void ManagerTest::testForwardingTargetNam()
{
	QFETCH( Request, request );
	QFETCH( RuleVector, rules );

	DummyNetworkAccessManager forwardingTargetNam;

	Manager< DummyNetworkAccessManager > mnam;
	mnam.setUnmatchedRequestBehavior( Forward );
	mnam.setRules( rules );
	mnam.setForwardingTargetNam( &forwardingTargetNam );

	QCOMPARE( mnam.forwardingTargetNam(), &forwardingTargetNam );

	auto reply = sendRequest( mnam, request );
	request.timestamp = QDateTime::currentDateTime();

	QVERIFY( mnam.requests.isEmpty() );
	QVERIFY( ! forwardingTargetNam.requests.isEmpty() );
	QVERIFY( compareRequestEssentials( forwardingTargetNam.requests.last(), request ) );
}

/*! Provides the data for the testForwardingTargetNam() test.
 */
void ManagerTest::testForwardingTargetNam_data()
{
	testForward_data();
}

/*! \test %Tests the forwarding functionality of the MockNetworkAccessManager using a QNetworkAccessManager provided by the Rule.
 */
void ManagerTest::testForwardRuleNam()
{
	QFETCH( Request, request );
	QFETCH( RuleVector, rules );
	QFETCH( DummyNetworkAccessManager::Ptr, forwardingTargetNam );

	Manager< DummyNetworkAccessManager > mnam;
	mnam.setUnmatchedRequestBehavior( Forward );
	mnam.setRules( rules );

	auto reply = sendRequest( mnam, request );
	request.timestamp = QDateTime::currentDateTime();

	QVERIFY( reply.get() );
	if ( forwardingTargetNam )
	{
		QVERIFY( ! forwardingTargetNam->requests.isEmpty() );
		QVERIFY( compareRequestEssentials( forwardingTargetNam->requests.last(), request ) );
	}
	else
	{
		QVERIFY( ! mnam.requests.isEmpty() );
		QVERIFY( compareRequestEssentials( mnam.requests.last(), request ) );
	}
}

/*! Provides the data for the ManagerTest::testForwardRuleNam() test.
 */
void ManagerTest::testForwardRuleNam_data()
{
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< RuleVector >( "rules" );
	QTest::addColumn< DummyNetworkAccessManager::Ptr >( "forwardingTargetNam" );

	Rule::Ptr gerForwardNam = Rule::Ptr::create();
	DummyNetworkAccessManager::Ptr forwardMNam( new DummyNetworkAccessManager() );
	gerForwardNam->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( exampleRootUrl ) )
	    .forward( Rule::ForwardAndReturnDelegatedReply, forwardMNam.data() );

	// clang-format off
	//                                         // request               // rules                             // forwardingTargetNam
	QTest::newRow( "rule forwarding NAM" )     << getExampleRootRequest << ( RuleVector() << gerForwardNam ) << forwardMNam;
	QTest::newRow( "manager forwarding NAM" )  << getFooRequest         << ( RuleVector() << gerForwardNam ) << DummyNetworkAccessManager::Ptr();
	// clang-format on
}

/*! \test %Tests the signal emission of the MockNetworkAccess::Manager via MockNetworkAccess::SignalEmitter.
 */
void ManagerTest::testManagerSignals()
{
	QFETCH( RequestVector, requests );
	QFETCH( RuleVector, rules );
	QFETCH( RequestVector, expectedHandledRequests );
	QFETCH( RequestVector, expectedMatchedRequests );
	QFETCH( RequestVector, expectedUnmatchedRequests );
	QFETCH( RequestVector, expectedForwardings );

	Manager< DummyNetworkAccessManager > mnam;
	mnam.setUnmatchedRequestBehavior( Forward );
	mnam.setRules( rules );

	auto* emitter = mnam.signalEmitter();

	QSignalSpy receivedRequestSpy( emitter, SIGNAL( receivedRequest( MockNetworkAccess::Request ) ) );
	QSignalSpy handledRequestSpy( emitter, SIGNAL( handledRequest( MockNetworkAccess::Request ) ) );
	QSignalSpy matchedRequestSpy( emitter,
	                              SIGNAL( matchedRequest( MockNetworkAccess::Request, MockNetworkAccess::Rule::Ptr ) ) );
	QSignalSpy unmatchedRequestSpy( emitter, SIGNAL( unmatchedRequest( MockNetworkAccess::Request ) ) );
	QSignalSpy forwardedRequestSpy( emitter, SIGNAL( forwardedRequest( MockNetworkAccess::Request ) ) );

	updateRequestTimestamps( requests );
	updateRequestTimestamps( expectedHandledRequests );
	updateRequestTimestamps( expectedMatchedRequests );
	updateRequestTimestamps( expectedUnmatchedRequests );
	updateRequestTimestamps( expectedForwardings );

	/* First send all requests, then wait for the replies to finish.
	 * This should ensure that all recorded requests have (nearly) the expected timestamp.
	 */
	ReplyVector replies;
	for ( auto&& request : requests )
		replies.push_back( sendRequest( mnam, request ) );

	for ( auto&& reply : replies )
		QTRY_VERIFY( reply->isFinished() );

	replies.clear();

	const auto receivedRequests = getRequestsFromSignalEmissions( receivedRequestSpy );
	const auto handledRequests = getRequestsFromSignalEmissions( handledRequestSpy );
	const auto matchedRequests = getRequestsFromSignalEmissions( matchedRequestSpy );
	const auto unmatchedRequests = getRequestsFromSignalEmissions( unmatchedRequestSpy );
	const auto forwardedRequests = getRequestsFromSignalEmissions( forwardedRequestSpy );

	QVERIFY( compareRequestVectors( receivedRequests, requests ) );
	QVERIFY( compareRequestVectors( handledRequests, expectedHandledRequests ) );
	QVERIFY( compareRequestVectors( matchedRequests, expectedMatchedRequests ) );
	QVERIFY( compareRequestVectors( unmatchedRequests, expectedUnmatchedRequests ) );
	QVERIFY( compareRequestVectors( forwardedRequests, expectedForwardings ) );

	for ( int i = 0; i < matchedRequestSpy.size(); ++i )
	{
		const auto& matchedSpyParams = matchedRequestSpy.at( i );
		QVERIFY( rules.contains( matchedSpyParams.at( 1 ).value< Rule::Ptr >() ) );
	}
}

/*! Provides the data for the testManagerSignals() and the testRequestRecording() tests.
 */
void ManagerTest::testManagerSignals_data()
{
	QTest::addColumn< RequestVector >( "requests" );
	QTest::addColumn< RuleVector >( "rules" );
	QTest::addColumn< RequestVector >( "expectedHandledRequests" );
	QTest::addColumn< RequestVector >( "expectedMatchedRequests" );
	QTest::addColumn< RequestVector >( "expectedUnmatchedRequests" );
	QTest::addColumn< RequestVector >( "expectedForwardings" );

	QTest::newRow( "matched request" ) << ( RequestVector() << getExampleRootRequest )
	                                   << ( RuleVector() << catchAllRule() )
	                                   << ( RequestVector() << getExampleRootRequest )
	                                   << ( RequestVector() << getExampleRootRequest ) << RequestVector()
	                                   << RequestVector();

	QTest::newRow( "unmatched request" ) << ( RequestVector() << getExampleRootRequest )
	                                     << ( RuleVector() << postFooRule() )
	                                     << ( RequestVector() << getExampleRootRequest ) << RequestVector()
	                                     << ( RequestVector() << getExampleRootRequest )
	                                     << ( RequestVector() << getExampleRootRequest );

	QTest::newRow( "no rules" ) << ( RequestVector() << getExampleRootRequest ) << RuleVector()
	                            << ( RequestVector() << getExampleRootRequest ) << RequestVector()
	                            << ( RequestVector() << getExampleRootRequest )
	                            << ( RequestVector() << getExampleRootRequest );

	QTest::newRow( "multiple rules" ) << ( RequestVector() << getExampleRootRequest )
	                                  << ( RuleVector() << postFooRule() << getExampleRootRule() << catchAllRule() )
	                                  << ( RequestVector() << getExampleRootRequest )
	                                  << ( RequestVector() << getExampleRootRequest ) << RequestVector()
	                                  << RequestVector();


	Rule::Ptr gerNoReply( new Rule );
	gerNoReply->has( Verb( QNetworkAccessManager::GetOperation ) ).has( Url( exampleRootUrl ) );
	QTest::newRow( "no reply" ) << ( RequestVector() << getExampleRootRequest ) << ( RuleVector() << gerNoReply )
	                            << ( RequestVector() << getExampleRootRequest ) << RequestVector()
	                            << ( RequestVector() << getExampleRootRequest )
	                            << ( RequestVector() << getExampleRootRequest );

	Rule::Ptr gerForwardReturn( new Rule );
	gerForwardReturn->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( exampleRootUrl ) )
	    .forward( Rule::ForwardAndReturnDelegatedReply );
	QTest::newRow( "matched, return forwarded" )
	    << ( RequestVector() << getExampleRootRequest ) << ( RuleVector() << gerForwardReturn )
	    << ( RequestVector() << getExampleRootRequest ) << ( RequestVector() << getExampleRootRequest )
	    << RequestVector() << ( RequestVector() << getExampleRootRequest );

	Rule::Ptr gerForwardMock( new Rule );
	gerForwardMock->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( exampleRootUrl ) )
	    .forward( Rule::ForwardAndReturnMockReply )
	    .reply()
	    .withStatus( HttpStatus::OK );
	QTest::newRow( "matched, forward, return mock" )
	    << ( RequestVector() << getExampleRootRequest ) << ( RuleVector() << gerForwardMock )
	    << ( RequestVector() << getExampleRootRequest ) << ( RequestVector() << getExampleRootRequest )
	    << RequestVector() << ( RequestVector() << getExampleRootRequest );

	QTest::newRow( "multiple, matched requests" )
	    << ( RequestVector() << getExampleRootRequest << getFooRequest ) << ( RuleVector() << catchAllRule() )
	    << ( RequestVector() << getExampleRootRequest << getFooRequest )
	    << ( RequestVector() << getExampleRootRequest << getFooRequest ) << RequestVector() << RequestVector();

	QTest::newRow( "multiple, unmatched requests" )
	    << ( RequestVector() << getExampleRootRequest << getFooRequest ) << ( RuleVector() << postFooRule() )
	    << ( RequestVector() << getExampleRootRequest << getFooRequest ) << RequestVector()
	    << ( RequestVector() << getExampleRootRequest << getFooRequest )
	    << ( RequestVector() << getExampleRootRequest << getFooRequest );

	QTest::newRow( "multiple requests" ) << ( RequestVector() << getExampleRootRequest << getFooRequest )
	                                     << ( RuleVector() << getExampleRootRule() )
	                                     << ( RequestVector() << getExampleRootRequest << getFooRequest )
	                                     << ( RequestVector() << getExampleRootRequest )
	                                     << ( RequestVector() << getFooRequest ) << ( RequestVector() << getFooRequest );


	QNetworkRequest fooQRequest( fooUrl );
	enableAutomaticRedirect( fooQRequest );
	const auto defaultMaximumRedirectsCount = fooQRequest.maximumRedirectsAllowed();
	fooQRequest.setMaximumRedirectsAllowed( defaultMaximumRedirectsCount - 1 );
	Request gerRedirectedRequest( fooQRequest );

	QTest::newRow( "redirected" ) << ( RequestVector() << gerFollowRedirectRequest() )
	                              << ( RuleVector() << gerToFooRedirectRule() )
	                              << ( RequestVector() << gerFollowRedirectRequest() << gerRedirectedRequest )
	                              << ( RequestVector() << gerFollowRedirectRequest() )
	                              << ( RequestVector() << gerRedirectedRequest )
	                              << ( RequestVector() << gerRedirectedRequest );
}

/*! Provides the data for the testRequestEqualityOperator() test.
 */
void ManagerTest::testRequestEqualityOperators_data()
{
	QTest::addColumn< Request >( "left" );
	QTest::addColumn< Request >( "right" );
	QTest::addColumn< bool >( "equal" );

	auto gerRequest = getExampleRootRequest;
	gerRequest.timestamp = getExampleRootRequest.timestamp;

	auto httpsGerRequest = httpsGetExampleRootRequest;
	httpsGerRequest.timestamp = getExampleRootRequest.timestamp;

	Request noSaveCookieRequest( QNetworkRequest( exampleRootUrl ), QNetworkAccessManager::GetOperation );
	noSaveCookieRequest.qRequest.setAttribute( QNetworkRequest::CacheSaveControlAttribute, false );
	noSaveCookieRequest.timestamp = getExampleRootRequest.timestamp;

	Request postFooRequest( QNetworkRequest( fooUrl ), QNetworkAccessManager::PostOperation );
	postFooRequest.timestamp = getFooRequest.timestamp;

	Request postFooRequestWithBody( QNetworkRequest( fooUrl ), QNetworkAccessManager::PostOperation, fooObject );
	postFooRequestWithBody.timestamp = postFooRequest.timestamp;

	Request differentTimestampRequest = getExampleRootRequest;
	differentTimestampRequest.timestamp = getExampleRootRequest.timestamp.addSecs( 1 );

	// clang-format off
	//                                            // left                  // right                     // equal
	QTest::newRow("null requests")                << Request()             << Request()                 << true;
	QTest::newRow("equal requests")               << getExampleRootRequest << gerRequest                << true;
	QTest::newRow("different URLs")               << getExampleRootRequest << httpsGerRequest           << false;
	QTest::newRow("different request attributes") << getExampleRootRequest << noSaveCookieRequest       << false;
	QTest::newRow("different operation")          << getFooRequest         << postFooRequest            << false;
	QTest::newRow("different body data")          << postFooRequest        << postFooRequestWithBody    << false;
	QTest::newRow("different timestamps")         << getExampleRootRequest << differentTimestampRequest << false;
	// clang-format on
}

/*! \test %Tests the Request::operator==() and Request::operator!=().
 */
void ManagerTest::testRequestEqualityOperators()
{
	QFETCH( Request, left );
	QFETCH( Request, right );
	QFETCH( bool, equal );

	QVERIFY( left == left );
	QVERIFY( right == right );
	QCOMPARE( left == right, equal );
	QCOMPARE( right == left, equal );
	QCOMPARE( left != right, ! equal );
	QCOMPARE( right != left, ! equal );
}

namespace /* anonymous */
{
#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
	bool changeAndCheckOpenState( QNetworkReply* reply, QIODevice::OpenMode openMode )
#else
	bool changeAndCheckOpenState( QNetworkReply* reply, QIODeviceBase::OpenMode openMode )
#endif // Qt >= 6.0.0
	{
		auto success = false;

		[ reply, openMode, &success ]() {
			reply->open( openMode );
			QCOMPARE( reply->openMode(), openMode );
#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
			QCOMPARE( reply->isOpen(), openMode != QIODevice::NotOpen );
			QCOMPARE( reply->isReadable(), openMode.testFlag( QIODevice::ReadOnly ) );
			QCOMPARE( reply->isWritable(), openMode.testFlag( QIODevice::WriteOnly ) );
#else
			QCOMPARE( reply->isOpen(), openMode != QIODeviceBase::NotOpen );
			QCOMPARE( reply->isReadable(), openMode.testFlag( QIODeviceBase::ReadOnly ) );
			QCOMPARE( reply->isWritable(), openMode.testFlag( QIODeviceBase::WriteOnly ) );
#endif // Qt >= 6.0.0
			QVERIFY( ! reply->isFinished() );
			success = true;
		}();

		return success;
	}
} // anonymous namespace


/*! \test %Tests the NetworkReply::open() method.
 */
void ManagerTest::testReplyOpen()
{
	Manager< DummyNetworkAccessManager > mnam;
	mnam.addRule( gerToFooRedirectRule() );

	const auto reply = sendRequest( mnam, getExampleRootRequest );
	QSignalSpy finishedSpy{ reply.get(), &QNetworkReply::finished };

#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
	QVERIFY( changeAndCheckOpenState( reply.get(), QIODevice::NotOpen ) );
	QVERIFY( changeAndCheckOpenState( reply.get(), QIODevice::ReadOnly ) );
	QVERIFY( changeAndCheckOpenState( reply.get(), QIODevice::ReadWrite ) );
	QVERIFY( changeAndCheckOpenState( reply.get(), QIODevice::ReadOnly | QIODevice::Append ) );
	QVERIFY( changeAndCheckOpenState( reply.get(), QIODevice::ReadOnly | QIODevice::Truncate ) );
	QVERIFY( changeAndCheckOpenState( reply.get(), QIODevice::ReadOnly | QIODevice::Text ) );
	QVERIFY( changeAndCheckOpenState( reply.get(), QIODevice::ReadOnly | QIODevice::Unbuffered ) );
	#if QT_VERSION >= QT_VERSION_CHECK( 5,11,0 )
	QVERIFY( changeAndCheckOpenState( reply.get(), QIODevice::ReadOnly | QIODevice::NewOnly ) );
	QVERIFY( changeAndCheckOpenState( reply.get(), QIODevice::ReadOnly | QIODevice::ExistingOnly ) );
	#endif // Qt >= 5.11.0
	QVERIFY( changeAndCheckOpenState( reply.get(), QIODevice::WriteOnly ) );
#else
	QVERIFY( changeAndCheckOpenState( reply.get(), QIODeviceBase::NotOpen ) );
	QVERIFY( changeAndCheckOpenState( reply.get(), QIODeviceBase::ReadOnly ) );
	QVERIFY( changeAndCheckOpenState( reply.get(), QIODeviceBase::ReadWrite ) );
	QVERIFY( changeAndCheckOpenState( reply.get(), QIODeviceBase::ReadOnly | QIODeviceBase::Append ) );
	QVERIFY( changeAndCheckOpenState( reply.get(), QIODeviceBase::ReadOnly | QIODeviceBase::Truncate ) );
	QVERIFY( changeAndCheckOpenState( reply.get(), QIODeviceBase::ReadOnly | QIODeviceBase::Text ) );
	QVERIFY( changeAndCheckOpenState( reply.get(), QIODeviceBase::ReadOnly | QIODeviceBase::Unbuffered ) );
	QVERIFY( changeAndCheckOpenState( reply.get(), QIODeviceBase::ReadOnly | QIODeviceBase::NewOnly ) );
	QVERIFY( changeAndCheckOpenState( reply.get(), QIODeviceBase::ReadOnly | QIODeviceBase::ExistingOnly ) );
	QVERIFY( changeAndCheckOpenState( reply.get(), QIODeviceBase::WriteOnly ) );
#endif

	QTRY_VERIFY( reply->isFinished() );
	QCOMPARE( finishedSpy.size(), 1 );
}

/*! \test %Tests the NetworkReply::close() method.
 */
void ManagerTest::testReplyClose()
{
	Manager< DummyNetworkAccessManager > mnam;
	mnam.addRule( gerToFooRedirectRule() );

	const auto reply = sendRequest( mnam, getExampleRootRequest );
	QSignalSpy finishedSpy{ reply.get(), &QNetworkReply::finished };
	reply->close();

	QVERIFY( ! reply->isOpen() );
	QVERIFY( ! reply->isReadable() );
	QVERIFY( reply->isFinished() );
	QTRY_COMPARE( finishedSpy.size(), 1 );
}

/*! \test %Tests the behavior of multiple parallel requests.
 */
void ManagerTest::testParallelRequests()
{
	QFETCH( Request, request );
	QFETCH( bool, expectConnectionQueuing );

	Manager< DummyNetworkAccessManager > mnam;
	mnam.whenGet( request.qRequest.url() ).reply().withStatus( HttpStatus::OK );

	ReplyVector replies;

	const auto requestsBeyondMaxParallel = 1;
	const auto totalRequests = mnam.maxParallelConnectionsPerTarget() + requestsBeyondMaxParallel;
	for ( auto count = 0u; count < totalRequests; ++count )
		replies.push_back( sendRequest( mnam, request ) );

	QEventLoop eventLoop;
	QObject::connect( replies.at( 0 ).get(), &QNetworkReply::finished, &eventLoop, &QEventLoop::quit );
	eventLoop.exec();

	QVERIFY( std::all_of( replies.begin(),
	                      replies.end() - ( requestsBeyondMaxParallel + 1 ),
	                      []( const std::unique_ptr< QNetworkReply >& reply ) { return reply->isFinished(); } ) );
	QCOMPARE( replies.at( totalRequests - 1 )->isFinished(), ! expectConnectionQueuing );
	QTRY_VERIFY( replies.at( totalRequests - 1 )->isFinished() );
}

/*! Provides the data for the testParallelRequests() test.
 */
void ManagerTest::testParallelRequests_data()
{
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< bool >( "expectConnectionQueuing" );

	QTest::newRow( "http" ) << getExampleRootRequest << true;
	QTest::newRow( "ftp" ) << ftpRequest() << false;
	QTest::newRow( "file" ) << Request{ QNetworkRequest{ fileUrl } } << false;
	QTest::newRow( "qrc" ) << Request{ QNetworkRequest{ qrcUrl } } << false;
}

/*! \test %Tests the reusing of connections.
 */
void ManagerTest::testConnectionReuse()
{
	Manager< DummyNetworkAccessManager > mnam;
	mnam.whenGet( httpsExampleRootUrl ).reply().withBody( statusOkJson );
	const auto secsToMSecs = 1000;
	const auto connectionTimeout = 1 * secsToMSecs;
	mnam.setConnectionTimeout( connectionTimeout );

	auto reply = sendRequest( mnam, httpsGetExampleRootRequest ); // creates connection
	QTRY_VERIFY( reply->isFinished() );

	reply = sendRequest( mnam, httpsGetExampleRootRequest );
	QSignalSpy firstEncryptedSpy( reply.get(), &QNetworkReply::encrypted );
	QTRY_VERIFY( reply->isFinished() );
	QVERIFY( firstEncryptedSpy.isEmpty() ); // same connection, still connected

	const auto timeoutBuffer = 2;
	QTest::qWait( timeoutBuffer * connectionTimeout ); // connection times out

	reply = sendRequest( mnam, httpsGetExampleRootRequest );
	QSignalSpy secondEncryptedSpy( reply.get(), &QNetworkReply::encrypted );
	QTRY_VERIFY( reply->isFinished() );
	QCOMPARE( secondEncryptedSpy.size(), 1 ); // new connection
}

/*! \test %Tests the closing of connections.
 */
void ManagerTest::testConnectionClose()
{
	Manager< DummyNetworkAccessManager > mnam;
	mnam.whenGet( httpsExampleRootUrl ).reply().withBody( statusOkJson ).withRawHeader( HttpUtils::connectionHeader(), "close" );

	auto reply = sendRequest( mnam, httpsGetExampleRootRequest ); // creates first connection
	QTRY_VERIFY( reply->isFinished() );

	reply = sendRequest( mnam, httpsGetExampleRootRequest );
	QSignalSpy encryptedSpy( reply.get(), &QNetworkReply::encrypted );
	QTRY_VERIFY( reply->isFinished() );
	QCOMPARE( encryptedSpy.size(), 1 ); // first connection should be closed, so a new one should be created
}

/*! \test %Tests the Manager::clearUnusedConnections() method.
 */
void ManagerTest::testClearUnusedConnections()
{
	Manager< DummyNetworkAccessManager > mnam;
	mnam.whenGet( httpsExampleRootUrl ).reply().withBody( statusOkJson );

	auto reply = sendRequest( mnam, httpsGetExampleRootRequest ); // creates connection
	QTRY_VERIFY( reply->isFinished() );

	mnam.clearUnusedConnections();

	reply = sendRequest( mnam, httpsGetExampleRootRequest );
	QSignalSpy encryptedSpy( reply.get(), &QNetworkReply::encrypted );
	QTRY_VERIFY( reply->isFinished() );
	QCOMPARE( encryptedSpy.size(), 1 ); // new connection
}


/*! \test %Tests aborting a request while the connection is still pending.
 */
void ManagerTest::testAbortRequestWhileConnectionIsQueued()
{
	Manager< DummyNetworkAccessManager > mnam;
	mnam.addRule( getExampleRootRule() );
	mnam.setMaxParallelConnectionsPerTarget( 1 );

	ReplyVector replies;

	for ( auto count = 0; count < 4; ++count )
		replies.push_back( sendRequest( mnam, getExampleRootRequest ) );

	replies.at( 1 )->abort();

	QEventLoop eventLoop;
	for ( auto&& reply : replies )
	{
		QObject::connect( reply.get(), &QNetworkReply::finished, &eventLoop, &QEventLoop::quit );
	}

	eventLoop.exec();
	QVERIFY( replies.at( 0 )->isFinished() );
	QVERIFY( ! replies.at( 2 )->isFinished() );
	QVERIFY( ! replies.at( 3 )->isFinished() );
	eventLoop.exec();
	QVERIFY( replies.at( 2 )->isFinished() );
	QVERIFY( ! replies.at( 3 )->isFinished() );
	eventLoop.exec();
	QVERIFY( replies.at( 3 )->isFinished() );
}

/*! \test %Tests the recording and clearing of requests.
 */
void ManagerTest::testRequestRecording()
{
	QFETCH( RequestVector, requests );
	QFETCH( RuleVector, rules );
	QFETCH( RequestVector, expectedHandledRequests );
	QFETCH( RequestVector, expectedMatchedRequests );
	QFETCH( RequestVector, expectedUnmatchedRequests );
	QFETCH( RequestVector, expectedForwardings );

	Manager< DummyNetworkAccessManager > mnam;
	mnam.setUnmatchedRequestBehavior( Forward );
	mnam.setRules( rules );

	updateRequestTimestamps( requests );
	updateRequestTimestamps( expectedHandledRequests );
	updateRequestTimestamps( expectedMatchedRequests );
	updateRequestTimestamps( expectedUnmatchedRequests );
	updateRequestTimestamps( expectedForwardings );

	for ( auto&& request : requests )
	{
		const auto reply = sendRequest( mnam, request );
		QTRY_VERIFY( reply->isFinished() );
	}

	QVERIFY( compareRequestVectors( mnam.receivedRequests(), requests ) );
	QVERIFY( compareRequestVectors( mnam.handledRequests(), expectedHandledRequests ) );
	QVERIFY( compareRequestVectors( mnam.matchedRequests(), expectedMatchedRequests ) );
	QVERIFY( compareRequestVectors( mnam.unmatchedRequests(), expectedUnmatchedRequests ) );
	QVERIFY( compareRequestVectors( mnam.forwardedRequests(), expectedForwardings ) );

	for ( auto&& expectedMatchedRequest : expectedMatchedRequests )
	{
		int ruleRecordedRequestsCount = 0;
		for ( auto&& rule : rules )
		{
			if ( ! rule->matchedRequests().isEmpty() )
			{
				const RequestVector matchedRequests = rule->matchedRequests();
				for ( auto&& matchedRequest : matchedRequests )
				{
					if ( compareRequests( matchedRequest, expectedMatchedRequest ) )
						ruleRecordedRequestsCount += 1;
				}
			}
		}
		QCOMPARE( ruleRecordedRequestsCount, 1 );
	}

	mnam.clearRequests();

	QVERIFY( mnam.receivedRequests().empty() );
	QVERIFY( mnam.handledRequests().empty() );
	QVERIFY( mnam.matchedRequests().empty() );
	QVERIFY( mnam.unmatchedRequests().empty() );
	QVERIFY( mnam.forwardedRequests().empty() );

	for ( auto&& rule : rules )
	{
		QVERIFY( rule->matchedRequests().empty() );
	}
}

/*! Provides the data for the testRequestRecording() test.
 */
void ManagerTest::testRequestRecording_data()
{
	// We reuse the data from the testSignals() test.
	testManagerSignals_data();
}

/*! \test %Tests the recording of multiple requests.
 */
void ManagerTest::testMultipleRequestsRecording()
{
	auto matchedRequest1 = getFooRequest;
	auto matchedRequest2 = getFooRequest;
	matchedRequest2.qRequest.setRawHeader( QByteArray( "X-Custom-Header" ), QByteArray( "Foobar" ) );

	QVERIFY( ! compareRequests( matchedRequest1, matchedRequest2 ) );

	auto unmatchedRequest = getExampleRootRequest;
	const QUrl forwardUrl( "http://forward.com" );
	const QNetworkRequest forwardRequestObj( forwardUrl );
	Request forwardRequest( forwardRequestObj );

	Manager< DummyNetworkAccessManager > mnam;

	auto& matchedRule = mnam.whenGet( fooUrl );
	matchedRule.reply().withStatus( HttpStatus::OK );

	mnam.setUnmatchedRequestBehavior( PredefinedReply );
	MockReplyBuilder unmatchedRequestReply;
	unmatchedRequestReply.withStatus( HttpStatus::NotFound );
	mnam.setUnmatchedRequestBuilder( unmatchedRequestReply );

	auto& forwardRule = mnam.whenGet( forwardUrl );
	forwardRule.forward( Rule::ForwardAndReturnDelegatedReply );

	auto matchedReply1 = sendRequest( mnam, matchedRequest1 );
	matchedRequest1.timestamp = QDateTime::currentDateTime();

	auto unmatchedReply = sendRequest( mnam, unmatchedRequest );
	unmatchedRequest.timestamp = QDateTime::currentDateTime();

	auto forwardReply = sendRequest( mnam, forwardRequest );
	forwardRequest.timestamp = QDateTime::currentDateTime();

	auto matchedReply2 = sendRequest( mnam, matchedRequest2 );
	matchedRequest2.timestamp = QDateTime::currentDateTime();

	QCOMPARE( mnam.receivedRequests().count(), 4 );
	QVERIFY( compareRequests( mnam.receivedRequests().at( 0 ), matchedRequest1 ) );
	QVERIFY( compareRequests( mnam.receivedRequests().at( 1 ), unmatchedRequest ) );
	QVERIFY( compareRequests( mnam.receivedRequests().at( 2 ), forwardRequest ) );
	QVERIFY( compareRequests( mnam.receivedRequests().at( 3 ), matchedRequest2 ) );

	QCOMPARE( mnam.matchedRequests().count(), 3 );
	QVERIFY( compareRequests( mnam.matchedRequests().at( 0 ), matchedRequest1 ) );
	QVERIFY( compareRequests( mnam.matchedRequests().at( 1 ), forwardRequest ) );
	QVERIFY( compareRequests( mnam.matchedRequests().at( 2 ), matchedRequest2 ) );

	QCOMPARE( matchedRule.matchedRequests().count(), 2 );
	QVERIFY( compareRequests( matchedRule.matchedRequests().at( 0 ), matchedRequest1 ) );
	QVERIFY( compareRequests( matchedRule.matchedRequests().at( 1 ), matchedRequest2 ) );

	QCOMPARE( mnam.unmatchedRequests().count(), 1 );
	QVERIFY( compareRequests( mnam.unmatchedRequests().at( 0 ), unmatchedRequest ) );

	QCOMPARE( mnam.forwardedRequests().count(), 1 );
	QVERIFY( compareRequests( mnam.forwardedRequests().at( 0 ), forwardRequest ) );

	QCOMPARE( forwardRule.matchedRequests().count(), 1 );
	QVERIFY( compareRequests( forwardRule.matchedRequests().at( 0 ), forwardRequest ) );
}

void ManagerTest::testReplyProperties()
{
	QFETCH( Request, request );
	QFETCH( RuleVector, rules );
	QFETCH( BehaviorFlags, behaviorFlags );
	QFETCH( qint64, expectedBodySize );

	Manager< DummyNetworkAccessManager > mnam;
	mnam.setBehaviorFlags( behaviorFlags );
	mnam.setRules( rules );

	const auto reply = sendRequest( mnam, request );

	QVERIFY( reply->isRunning() );
	QVERIFY( ! reply->isOpen() );
	QVERIFY( ! reply->isReadable() );
	QVERIFY( reply->atEnd() );
	QVERIFY( ! reply->canReadLine() );
	QCOMPARE( reply->bytesAvailable(), 0 );

	QTRY_VERIFY( reply->isFinished() );

	QCOMPARE( reply->bytesAvailable(), expectedBodySize );
	QCOMPARE( reply->bytesToWrite(), 0 );
	QCOMPARE( reply->atEnd(), expectedBodySize == 0 );
	QVERIFY( ! reply->canReadLine() );
	QTEST( static_cast< int >( reply->error() ), "expectedError" );
	QTEST( reply->errorString(), "expectedErrorString" );
	QVERIFY( reply->isOpen() );
	QVERIFY( reply->isReadable() );
	QVERIFY( ! reply->isRunning() );
	QVERIFY( reply->isSequential() );
	QVERIFY( ! reply->isTextModeEnabled() );
#if QT_VERSION >= QT_VERSION_CHECK( 5,7,0 )
	QVERIFY( ! reply->isTransactionStarted() );
#endif
	QVERIFY( ! reply->isWritable() );
	QCOMPARE( reply->manager(), static_cast< QNetworkAccessManager* >( &mnam ) );
	QCOMPARE( reply->pos(), 0 );
	const auto actualRawHeaders = pairListToHash( reply->rawHeaderPairs() );
	QTEST( actualRawHeaders, "expectedRawHeaders" );
	const auto actualAttributes = NetworkReply::attributes( reply.get() );
	QTEST( actualAttributes, "expectedAttributes" );
	QCOMPARE( reply->readBufferSize(), 0 );
	QCOMPARE( reply->request(), request.qRequest );
	QCOMPARE( reply->operation(), request.operation );
	QCOMPARE( reply->size(), expectedBodySize );
	// QCOMPARE( reply->sslConfiguration(), m_expected->sslConfiguration() ); // TODO See also issue #52
	QTEST( reply->url(), "expectedReplyUrl" );
}

void ManagerTest::testReplyProperties_data()
{
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< RuleVector >( "rules" );
	QTest::addColumn< BehaviorFlags >( "behaviorFlags" );
	QTest::addColumn< qint64 >( "expectedBodySize" );
	QTest::addColumn< int >( "expectedError" );
	QTest::addColumn< QString >( "expectedErrorString" );
	QTest::addColumn< QUrl >( "expectedReplyUrl" );
	QTest::addColumn< AttributeHash >( "expectedAttributes" );
	QTest::addColumn< RawHeaderHash >( "expectedRawHeaders" );


	const auto notFoundStatusAttributes = defaultAttributes( HttpStatus::NotFound );

	const auto unknownErrorString = QStringLiteral( "Unknown error" );
	const auto requestDidNotMatchErrorString = QStringLiteral(
	    "MockNetworkAccessManager: Request did not match any rule" );

	const auto contentLengthHeader = QByteArrayLiteral( "Content-Length" );
	const auto contentTypeHeader = QByteArrayLiteral( "Content-Type" );
	const auto plainTextContentType = QByteArrayLiteral( "text/plain" );
	const auto jsonContentType = QByteArrayLiteral( "application/json" );

	const auto defaultRawHeaders = RawHeaderHash{ { contentTypeHeader, plainTextContentType },
		                                          { contentLengthHeader, QByteArray::number( statusOkJson.size() ) } };

	const auto expectedBehavior = static_cast< BehaviorFlags >( Behavior_Expected );

	QTest::newRow( "simple, succeeding GET" )
	    << getExampleRootRequest << ( RuleVector() << getExampleRootRule() ) << expectedBehavior
	    << static_cast< qint64 >( statusOkJson.size() ) << static_cast< int >( noError ) << unknownErrorString
	    << exampleRootUrl << defaultAttributes() << defaultRawHeaders;

	QTest::newRow( "failing GET" ) << getExampleRootRequest << RuleVector() << expectedBehavior << 0LL
	                               << static_cast< int >( QNetworkReply::ContentNotFoundError )
	                               << requestDidNotMatchErrorString << exampleRootUrl << notFoundStatusAttributes
	                               << RawHeaderHash{};

	const auto dateHeader = QByteArrayLiteral( "Date" );
	const auto customHeader = QByteArrayLiteral( "x-custom-header" );
	const auto firstDate = QDateTime::currentDateTime().toString().toLatin1();
	const auto secondDate = QDateTime::currentDateTime().toString().toLatin1();
	const auto thirdDate = QDateTime::currentDateTime().toString().toLatin1();
	auto firstRedirectRule = gerToFooRedirectRule();
	firstRedirectRule->reply()
	    .withRawHeader( dateHeader, firstDate )
	    .withRawHeader( customHeader, QByteArrayLiteral( "first" ) );

	auto secondRedirectRule = getFooRedirectRule();
	secondRedirectRule->reply()
	    .withRawHeader( dateHeader, secondDate )
	    .withRawHeader( customHeader, QByteArrayLiteral( "second" ) );

	auto redirectEndRule = getBarRule();
	redirectEndRule->reply().withRawHeader( dateHeader, thirdDate ).withRawHeader( customHeader, QByteArrayLiteral( "third" ) );

	const auto multiRedirectHeadersWithConcat = RawHeaderHash{
		{ contentTypeHeader, plainTextContentType },
		{ contentLengthHeader, QByteArray::number( barPayload.size() ) },
		{ customHeader, QByteArrayLiteral( "first, second, third" ) },
		{ dateHeader, firstDate + ", " + secondDate + ", " + thirdDate },
		{ HttpUtils::locationHeader(), barUrl.toEncoded() }
	};
	QTest::newRow( "multiple redirect with header concat" )
	    << gerFollowRedirectRequest() << ( RuleVector() << firstRedirectRule << secondRedirectRule << redirectEndRule )
	    << static_cast< BehaviorFlags >( Behavior_ConcatRawHeadersAfterRedirect )
	    << static_cast< qint64 >( barPayload.size() ) << static_cast< int >( noError ) << unknownErrorString << barUrl
	    << defaultAttributes() << multiRedirectHeadersWithConcat;

	const auto multiRedirectHeaders = RawHeaderHash{ { contentTypeHeader, plainTextContentType },
		                                             { contentLengthHeader, QByteArray::number( barPayload.size() ) },
		                                             { customHeader, QByteArrayLiteral( "third" ) },
		                                             { dateHeader, thirdDate } };
	QTest::newRow( "multiple redirect" ) << gerFollowRedirectRequest()
	                                     << ( RuleVector() << firstRedirectRule << secondRedirectRule << redirectEndRule )
	                                     << expectedBehavior << static_cast< qint64 >( barPayload.size() )
	                                     << static_cast< int >( noError ) << unknownErrorString << barUrl
	                                     << defaultAttributes() << multiRedirectHeaders;
}

void ManagerTest::testUserDefinedAttributes()
{
	std::random_device rng;
	const auto attributeIdMax = 1000;
	std::uniform_int_distribution< int > dist( 0, attributeIdMax );
	const auto customAttributeId = static_cast< QNetworkRequest::Attribute >( QNetworkRequest::User + dist( rng ) );
	const auto customAttributeValue = QVariant::fromValue( QString( "foo" ) );

	Manager< DummyNetworkAccessManager > mnam;
	mnam.registerUserDefinedAttribute( customAttributeId );
	QVERIFY( mnam.userDefinedAttributes().contains( customAttributeId ) );

	auto rule = getExampleRootRule();
	rule->reply().withAttribute( customAttributeId, customAttributeValue );
	mnam.addRule( rule );

	const auto reply = sendRequest( mnam, getExampleRootRequest );

	QTRY_VERIFY( reply->isFinished() );
	QCOMPARE( reply->attribute( customAttributeId ), customAttributeValue );
}

void ManagerTest::testReplySignals()
{
	QFETCH( bool, useRealNam );
	QFETCH( BehaviorFlags, behaviorFlags );
	QFETCH( Request, request );
	QFETCH( RuleVector, rules );

	std::unique_ptr< QNetworkAccessManager > manager;
	ReplyPtr reply;

	if ( useRealNam )
	{
		std::unique_ptr< Manager< QNetworkAccessManager > > mockedRealNam( new Manager< QNetworkAccessManager >() );
		mockedRealNam->setBehaviorFlags( behaviorFlags );
		mockedRealNam->setUnmatchedRequestBehavior( Forward );
		mockedRealNam->setRules( rules );
		reply = sendRequest( *mockedRealNam, request );
		manager = std::move( mockedRealNam );
	}
	else
	{
		std::unique_ptr< Manager< DummyNetworkAccessManager > > mnam( new Manager< DummyNetworkAccessManager >() );
		mnam->setBehaviorFlags( behaviorFlags );
		mnam->setUnmatchedRequestBehavior( Forward );
		mnam->setRules( rules );
		reply = sendRequest( *mnam, request );
		manager = std::move( mnam );
	}

	QSignalInspector signalInspector( reply.get() );

	QTRY_VERIFY( reply->isFinished() );

	SignalEmissionList emissions = getSignalEmissionListFromInspector( signalInspector );
	if ( useRealNam )
	{
		/* The downloadProgress signals are unreliable for real network requests.
		 * Therefore, we remove all of them except for the last one.
		 */
		removeDuplicateSignals( emissions, "downloadProgress" );
	}

	QTEST( emissions, "expectedSignals" );
}

void ManagerTest::testReplySignals_data()
{
	QTest::addColumn< bool >( "useRealNam" );
	QTest::addColumn< BehaviorFlags >( "behaviorFlags" );
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< RuleVector >( "rules" );
	QTest::addColumn< SignalEmissionList >( "expectedSignals" );

	const auto expectedBehavior = static_cast< BehaviorFlags >( Behavior_Expected );

	QTest::newRow( "basic signals" ) << false << expectedBehavior << getExampleRootRequest
	                                 << ( RuleVector() << getExampleRootRule() )
	                                 << ( SignalEmissionList()
	                                      << qMakePair( metaDataChangedSignal, QVariantList() )
	                                      << qMakePair( readyReadSignal, QVariantList() )
	                                      << qMakePair( downloadProgressSignal,
	                                                    QVariantList() << statusOkJson.size() << statusOkJson.size() )
	                                      << qMakePair( downloadProgressSignal,
	                                                    QVariantList() << statusOkJson.size() << statusOkJson.size() )
	                                      << qMakePair( readChannelFinishedSignal, QVariantList() )
	                                      << qMakePair( finishedSignal, QVariantList() ) );

	QTest::newRow( "file GET success" ) << false << expectedBehavior << Request{ QNetworkRequest{ fileUrl } }
	                                    << RuleVector{ { getFileRule() } }
	                                    << ( SignalEmissionList()
	                                         << qMakePair( metaDataChangedSignal, QVariantList() )
	                                         << qMakePair( downloadProgressSignal,
	                                                       QVariantList() << statusOkJson.size() << statusOkJson.size() )
	                                         << qMakePair( readyReadSignal, QVariantList() )
	                                         << qMakePair( finishedSignal, QVariantList() ) );

	Rule::Ptr gerNoContent( new Rule );
	gerNoContent->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( exampleRootUrl ) )
	    .reply()
	    .withStatus( HttpStatus::NoContent );
	QTest::newRow( "no content" ) << false << expectedBehavior << getExampleRootRequest
	                              << ( RuleVector() << gerNoContent )
	                              << ( SignalEmissionList()
	                                   << qMakePair( metaDataChangedSignal, QVariantList() )
	                                   << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
	                                   << qMakePair( readChannelFinishedSignal, QVariantList() )
	                                   << qMakePair( finishedSignal, QVariantList() ) );

	const QByteArray simpleBody( "foo bar" );
	Rule::Ptr gerError( new Rule );
	gerError->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( exampleRootUrl ) )
	    .reply()
	    .withStatus( HttpStatus::NotFound )
	    .withBody( simpleBody );

	QTest::newRow( "error signal" )
	    << false << expectedBehavior << getExampleRootRequest << ( RuleVector() << gerError )
	    << ( SignalEmissionList()
	         << qMakePair( metaDataChangedSignal, QVariantList() ) << qMakePair( readyReadSignal, QVariantList() )
	         << qMakePair( downloadProgressSignal, QVariantList() << simpleBody.size() << simpleBody.size() )
#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
	         << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentNotFoundError ) )
#endif
#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
	         << qMakePair( errorOccurredSignal,
	                       QVariantList() << QVariant::fromValue( QNetworkReply::ContentNotFoundError ) )
#endif // Qt >= 5.15.0
	         << qMakePair( downloadProgressSignal, QVariantList() << simpleBody.size() << simpleBody.size() )
	         << qMakePair( readChannelFinishedSignal, QVariantList() ) << qMakePair( finishedSignal, QVariantList() ) );

	Rule::Ptr gerErrorNoConent( new Rule );
	gerErrorNoConent->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( exampleRootUrl ) )
	    .reply()
	    .withStatus( HttpStatus::NotFound );

	QTest::newRow( "error signal without content" )
	    << false << expectedBehavior << getExampleRootRequest << ( RuleVector() << gerErrorNoConent )
	    << ( SignalEmissionList()
	         << qMakePair( metaDataChangedSignal, QVariantList() )
#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
	         << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentNotFoundError ) )
#endif
#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
	         << qMakePair( errorOccurredSignal,
	                       QVariantList() << QVariant::fromValue( QNetworkReply::ContentNotFoundError ) )
#endif // Qt >= 5.15.0
	         << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
	         << qMakePair( readChannelFinishedSignal, QVariantList() ) << qMakePair( finishedSignal, QVariantList() ) );

	QTest::newRow( "upload" )
	    << false << static_cast< BehaviorFlags >( Behavior_Expected ) << postFooRequest()
	    << ( RuleVector() << postFooRule() )
	    << ( SignalEmissionList()
	         << qMakePair( uploadProgressSignal, QVariantList() << fooObject.size() << fooObject.size() )
	         << qMakePair( metaDataChangedSignal, QVariantList() ) << qMakePair( readyReadSignal, QVariantList() )
	         << qMakePair( downloadProgressSignal, QVariantList() << statusOkJson.size() << statusOkJson.size() )
	         << qMakePair( downloadProgressSignal, QVariantList() << statusOkJson.size() << statusOkJson.size() )
	         << qMakePair( readChannelFinishedSignal, QVariantList() ) << qMakePair( finishedSignal, QVariantList() ) );

	QTest::newRow( "final upload(0,0) signal" )
	    << false << static_cast< BehaviorFlags >( Behavior_FinalUpload00Signal ) << postFooRequest()
	    << ( RuleVector() << postFooRule() )
	    << ( SignalEmissionList()
	         << qMakePair( uploadProgressSignal, QVariantList() << fooObject.size() << fooObject.size() )
	         << qMakePair( metaDataChangedSignal, QVariantList() ) << qMakePair( readyReadSignal, QVariantList() )
	         << qMakePair( downloadProgressSignal, QVariantList() << statusOkJson.size() << statusOkJson.size() )
	         << qMakePair( downloadProgressSignal, QVariantList() << statusOkJson.size() << statusOkJson.size() )
	         << qMakePair( uploadProgressSignal, QVariantList() << 0 << 0 )
	         << qMakePair( readChannelFinishedSignal, QVariantList() ) << qMakePair( finishedSignal, QVariantList() ) );

	Request gerFollowRedirectNoCacheRequest = gerFollowRedirectRequest();
	gerFollowRedirectNoCacheRequest.qRequest.setAttribute( QNetworkRequest::CacheLoadControlAttribute,
	                                                       QNetworkRequest::AlwaysNetwork );
	gerFollowRedirectNoCacheRequest.qRequest.setAttribute( QNetworkRequest::CacheSaveControlAttribute, false );
	const QUrl getHttpBinJsonUrl( "http://eu.httpbin.org/json" );
	const int expectedPayload = 429;
	Rule::Ptr gerRedirectHttpBin( new Rule );
	gerRedirectHttpBin->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( exampleRootUrl ) )
	    .reply()
	    .withRedirect( getHttpBinJsonUrl );
	Rule::Ptr getFooForward( new Rule );
	getFooForward->has( Verb( QNetworkAccessManager::GetOperation ) ).has( Url( fooUrl ) ).forward();

	QTest::newRow( "redirected, unmatched forward to real NAM" )
	    << true << expectedBehavior << gerFollowRedirectNoCacheRequest << ( RuleVector() << gerRedirectHttpBin )
	    << ( SignalEmissionList() << qMakePair( metaDataChangedSignal, QVariantList() )
	                              << qMakePair( redirectedSignal, QVariantList() << getHttpBinJsonUrl )
	                              << qMakePair( metaDataChangedSignal, QVariantList() )
	                              << qMakePair( readyReadSignal, QVariantList() )
	                              << qMakePair( downloadProgressSignal,
	                                            QVariantList() << expectedPayload << expectedPayload )
	                              << qMakePair( readChannelFinishedSignal, QVariantList() )
	                              << qMakePair( finishedSignal, QVariantList() ) );

	QTest::newRow( "redirected" ) << false << expectedBehavior << gerFollowRedirectNoCacheRequest
	                              << ( RuleVector() << gerToFooRedirectRule() << getFooRedirectRule() << getBarRule() )
	                              << ( SignalEmissionList()
	                                   << qMakePair( metaDataChangedSignal, QVariantList() )
	                                   << qMakePair( redirectedSignal, QVariantList() << fooUrl )
	                                   << qMakePair( metaDataChangedSignal, QVariantList() )
	                                   << qMakePair( redirectedSignal, QVariantList() << barUrl )
	                                   << qMakePair( metaDataChangedSignal, QVariantList() )
	                                   << qMakePair( readyReadSignal, QVariantList() )
	                                   << qMakePair( downloadProgressSignal,
	                                                 QVariantList() << barPayload.size() << barPayload.size() )
	                                   << qMakePair( downloadProgressSignal,
	                                                 QVariantList() << barPayload.size() << barPayload.size() )
	                                   << qMakePair( readChannelFinishedSignal, QVariantList() )
	                                   << qMakePair( finishedSignal, QVariantList() ) );

	QTest::newRow( "redirected, unmatched forward" )
	    << false << expectedBehavior << gerFollowRedirectNoCacheRequest
	    << ( RuleVector() << gerToFooRedirectRule() << getFooRedirectRule() )
	    << ( SignalEmissionList() << qMakePair( metaDataChangedSignal, QVariantList() )
	                              << qMakePair( redirectedSignal, QVariantList() << fooUrl )
	                              << qMakePair( metaDataChangedSignal, QVariantList() )
	                              << qMakePair( redirectedSignal, QVariantList() << barUrl )
	                              << qMakePair( metaDataChangedSignal, QVariantList() )
#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
	                              << qMakePair( errorSignal,
	                                            QVariantList() << QVariant::fromValue( operationCanceledError ) )
#endif // Qt < 6.0.0
#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
	                              << qMakePair( errorOccurredSignal,
	                                            QVariantList() << QVariant::fromValue( operationCanceledError ) )
#endif // Qt >= 5.15.0
	                              << qMakePair( finishedSignal, QVariantList() ) );

	QTest::newRow( "redirected, matched forward" )
	    << false << expectedBehavior << gerFollowRedirectNoCacheRequest
	    << ( RuleVector() << gerToFooRedirectRule() << getFooForward )
	    << ( SignalEmissionList() << qMakePair( metaDataChangedSignal, QVariantList() )
	                              << qMakePair( redirectedSignal, QVariantList() << fooUrl )
	                              << qMakePair( metaDataChangedSignal, QVariantList() )
#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
	                              << qMakePair( errorSignal,
	                                            QVariantList() << QVariant::fromValue( operationCanceledError ) )
#endif // Qt < 6.0.0
#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
	                              << qMakePair( errorOccurredSignal,
	                                            QVariantList() << QVariant::fromValue( operationCanceledError ) )
#endif // Qt >= 5.15.0
	                              << qMakePair( finishedSignal, QVariantList() ) );

	Request httpsGerFollowRedirectToHttps( QNetworkRequest( httpsExampleRootUrl ), QNetworkAccessManager::GetOperation );
	enableAutomaticRedirect( httpsGerFollowRedirectToHttps.qRequest );
	auto httpsGerRedirectDifferentHost = Rule::Ptr::create();
	httpsGerRedirectDifferentHost->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( httpsExampleRootUrl ) )
	    .reply()
	    .withRedirect( httpsOtherOriginUrl );
	auto httpsOtherOriginalSuccess = Rule::Ptr::create();
	httpsOtherOriginalSuccess->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( httpsOtherOriginUrl ) )
	    .reply()
	    .withStatus( HttpStatus::OK );

	QTest::newRow( "redirected HTTPS to different HTTPS host" )
	    << false << expectedBehavior << httpsGerFollowRedirectToHttps
	    << ( RuleVector() << httpsGerRedirectDifferentHost << httpsOtherOriginalSuccess )
	    << ( SignalEmissionList() << qMakePair( encryptedSignal, QVariantList() )
	                              << qMakePair( metaDataChangedSignal, QVariantList() )
	                              << qMakePair( redirectedSignal, QVariantList() << httpsOtherOriginUrl )
	                              << qMakePair( encryptedSignal, QVariantList() )
	                              << qMakePair( metaDataChangedSignal, QVariantList() )
	                              << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
	                              << qMakePair( readChannelFinishedSignal, QVariantList() )
	                              << qMakePair( finishedSignal, QVariantList() ) );

	auto httpsGerRedirectSameHost = Rule::Ptr::create();
	httpsGerRedirectSameHost->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( httpsExampleRootUrl ) )
	    .reply()
	    .withRedirect( httpsFooUrl );
	auto getHttpsFoo = Rule::Ptr::create();
	getHttpsFoo->has( Url( httpsFooUrl ) ).reply().withStatus( HttpStatus::OK );

	QTest::newRow( "redirected HTTPS to same HTTPS host" )
	    << false << expectedBehavior << httpsGerFollowRedirectToHttps
	    << ( RuleVector() << httpsGerRedirectSameHost << getHttpsFoo )
	    << ( SignalEmissionList() << qMakePair( encryptedSignal, QVariantList() )
	                              << qMakePair( metaDataChangedSignal, QVariantList() )
	                              << qMakePair( redirectedSignal, QVariantList() << httpsFooUrl )
	                              << qMakePair( metaDataChangedSignal, QVariantList() )
	                              << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
	                              << qMakePair( readChannelFinishedSignal, QVariantList() )
	                              << qMakePair( finishedSignal, QVariantList() ) );
}

/*! \test %Tests reading data from a MockNetworkAccess::NetworkReply.
 */
void ManagerTest::testReadReplyData()
{
	Manager< QNetworkAccessManager > mnam;
	mnam.addRule( getExampleRootRule() );

	const auto reply = sendRequest( mnam, getExampleRootRequest );

	QSignalSpy readyReadSpy{ reply.get(), &QNetworkReply::readyRead };

	QTRY_VERIFY( readyReadSpy.size() > 0 );

	QVERIFY( reply->isReadable() );
	QCOMPARE( reply->bytesAvailable(), static_cast< qint64 >( statusOkJson.size() ) );
	const auto firstReadData = reply->read( 2 );
	QCOMPARE( firstReadData, QByteArrayLiteral( "{ " ) );
	const auto skipAmount = QByteArrayLiteral( "\"status\": " ).size();
#if QT_VERSION >= QT_VERSION_CHECK( 5,10,0 )
	reply->skip( skipAmount );
#else
	reply->read( skipAmount );
#endif
	const auto secondReadData = reply->read( 4 );
	QCOMPARE( secondReadData, QByteArrayLiteral( "\"OK\"" ) );
	const auto finalReadData = reply->readAll();
	QCOMPARE( finalReadData, QByteArrayLiteral( " }" ) );
	QVERIFY( reply->atEnd() );

	QTRY_VERIFY( reply->isFinished() );
}

/*! \test %Tests direct aborting of a MockNetworkAccess::NetworkReply.
 */
void ManagerTest::testDirectAbort()
{
	Manager< QNetworkAccessManager > mnam;
	mnam.addRule( getExampleRootRule() );

	const auto reply = sendRequest( mnam, getExampleRootRequest );

	reply->abort();

	QVERIFY( reply->isFinished() );
	QCOMPARE( reply->error(), QNetworkReply::OperationCanceledError );
	QVERIFY( ! reply->isOpen() );
	QVERIFY( ! reply->isReadable() );
	QCOMPARE( reply->bytesAvailable(), 0 );
	const auto encryptedAttributeOnly = AttributeHash{ { QNetworkRequest::ConnectionEncryptedAttribute, false } };
	QCOMPARE( NetworkReply::attributes( reply.get() ), encryptedAttributeOnly );
	QVERIFY( reply->rawHeaderList().isEmpty() );
}

/*! \test %Tests aborting of a MockNetworkAccess::NetworkReply.
 */
void ManagerTest::testAbort()
{
	QFETCH( QMetaMethod, abortAfterSignal );
	QFETCH( Request, request );
	QFETCH( RuleVector, rules );

	Manager< QNetworkAccessManager > mnam;
	mnam.setBehaviorFlags( Behavior_Expected );
	mnam.setRules( rules );

	const auto reply = sendRequest( mnam, request );

	QSignalInspector signalInspector( reply.get() );

	const auto abortSlot = reply->metaObject()->method(
	    reply->metaObject()->indexOfSlot( QMetaObject::normalizedSignature( "abort()" ) ) );
	QObject::connect( reply.get(), abortAfterSignal, reply.get(), abortSlot );

	QTRY_VERIFY( reply->isFinished() );
	QCOMPARE( reply->error(), QNetworkReply::OperationCanceledError );
	QVERIFY( ! reply->isOpen() );
	QVERIFY( ! reply->isReadable() );
	QCOMPARE( reply->bytesAvailable(), 0 );
	const auto actualAttributes = NetworkReply::attributes( reply.get() );
	QTEST( actualAttributes, "expectedAttributes" );
	QTEST( Tests::pairListToHash( reply->rawHeaderPairs() ), "expectedHeaders" );
	SignalEmissionList emissions = getSignalEmissionListFromInspector( signalInspector );
	QTEST( emissions, "expectedSignals" );
}

/*! Provides the data for the testAbort() test.
 */
void ManagerTest::testAbort_data()
{
	QTest::addColumn< QMetaMethod >( "abortAfterSignal" );
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< RuleVector >( "rules" );
	QTest::addColumn< AttributeHash >( "expectedAttributes" );
	QTest::addColumn< RawHeaderHash >( "expectedHeaders" );
	QTest::addColumn< SignalEmissionList >( "expectedSignals" );

	const Request httpsGerRequest( QNetworkRequest( httpsExampleRootUrl ), QNetworkAccessManager::GetOperation );

	auto httpsGerRule = Rule::Ptr::create();
	httpsGerRule->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( httpsExampleRootUrl ) )
	    .reply()
	    .withBody( statusOkJson );

	const AttributeHash encryptedAttributeOnly{ { QNetworkRequest::ConnectionEncryptedAttribute, true } };

	const auto successResponseHeaders = RawHeaderHash{ { "Content-Length", QByteArray::number( statusOkJson.size() ) },
		                                               { "Content-Type", "text/plain" } };

	QTest::newRow( "after encrypted" )
	    << encryptedSignal << httpsGerRequest << RuleVector{ httpsGerRule } << encryptedAttributeOnly << RawHeaderHash{}
	    << ( SignalEmissionList()
	         << qMakePair( encryptedSignal, QVariantList{} ) << qMakePair( aboutToCloseSignal, QVariantList() )
#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
	         << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::OperationCanceledError ) )
#endif
#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
	         << qMakePair( errorOccurredSignal,
	                       QVariantList() << QVariant::fromValue( QNetworkReply::OperationCanceledError ) )
#endif // Qt >= 5.15.0
	         << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
	         << qMakePair( readChannelFinishedSignal, QVariantList() ) << qMakePair( finishedSignal, QVariantList() ) );

	QTest::newRow( "after uploadProgress" )
	    << uploadProgressSignal << postFooRequest() << RuleVector{ postFooRule() }
	    << AttributeHash{ { QNetworkRequest::ConnectionEncryptedAttribute, false } } << RawHeaderHash{}
	    << ( SignalEmissionList()
	         << qMakePair( uploadProgressSignal, QVariantList() << fooObject.size() << fooObject.size() )
	         << qMakePair( aboutToCloseSignal, QVariantList() )
#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
	         << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::OperationCanceledError ) )
#endif
#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
	         << qMakePair( errorOccurredSignal,
	                       QVariantList() << QVariant::fromValue( QNetworkReply::OperationCanceledError ) )
#endif // Qt >= 5.15.0
	         << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
	         << qMakePair( readChannelFinishedSignal, QVariantList() ) << qMakePair( finishedSignal, QVariantList() ) );

	QTest::newRow( "after metaDataChanged" )
	    << metaDataChangedSignal << getExampleRootRequest << RuleVector{ getExampleRootRule() } << defaultAttributes()
	    << successResponseHeaders
	    << ( SignalEmissionList()
	         << qMakePair( metaDataChangedSignal, QVariantList() ) << qMakePair( aboutToCloseSignal, QVariantList() )
#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
	         << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::OperationCanceledError ) )
#endif
#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
	         << qMakePair( errorOccurredSignal,
	                       QVariantList() << QVariant::fromValue( QNetworkReply::OperationCanceledError ) )
#endif // Qt >= 5.15.0
	         << qMakePair( downloadProgressSignal, QVariantList() << 0 << statusOkJson.size() )
	         << qMakePair( readChannelFinishedSignal, QVariantList() ) << qMakePair( finishedSignal, QVariantList() ) );

	QTest::newRow( "after downloadProgress" )
	    << downloadProgressSignal << getExampleRootRequest << RuleVector{ getExampleRootRule() } << defaultAttributes()
	    << successResponseHeaders
	    << ( SignalEmissionList()
	         << qMakePair( metaDataChangedSignal, QVariantList() ) << qMakePair( readyReadSignal, QVariantList() )
	         << qMakePair( downloadProgressSignal, QVariantList() << statusOkJson.size() << statusOkJson.size() )
	         << qMakePair( aboutToCloseSignal, QVariantList{} )
#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
	         << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::OperationCanceledError ) )
#endif
#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
	         << qMakePair( errorOccurredSignal,
	                       QVariantList() << QVariant::fromValue( QNetworkReply::OperationCanceledError ) )
#endif // Qt >= 5.15.0
	         << qMakePair( downloadProgressSignal, QVariantList() << statusOkJson.size() << statusOkJson.size() )
	         << qMakePair( readChannelFinishedSignal, QVariantList() ) << qMakePair( finishedSignal, QVariantList() ) );
}

void ManagerTest::testAbortAfterFinish()
{
	Manager< QNetworkAccessManager > mnam;
	mnam.addRule( getExampleRootRule() );

	const auto reply = sendRequest( mnam, getExampleRootRequest );

	QTRY_VERIFY( reply->isFinished() );

	reply->abort();

	QCOMPARE( reply->error(), QNetworkReply::NoError );
	QVERIFY( reply->isOpen() );
	QVERIFY( reply->isReadable() );
	QCOMPARE( reply->bytesAvailable(), statusOkJson.size() );
	QCOMPARE( NetworkReply::attributes( reply.get() ), defaultAttributes() );
	const auto successHeaders = RawHeaderHash{ { "Content-Length", QByteArray::number( statusOkJson.size() ) },
		                                       { "Content-Type", "text/plain" } };
	QCOMPARE( Tests::pairListToHash( reply->rawHeaderPairs() ), successHeaders );
}

/*! \test %Tests the authentication functionality of the MockNetworkAccess::Manager.
 */
void ManagerTest::testAuthentication()
{
	QFETCH( Request, request );
	QFETCH( RuleVector, rules );
	QFETCH( StringPair, credentials );

	QMutexLocker locker( &m_credentialMutex );
	m_username = credentials.first;
	m_password = credentials.second;
	m_lastAuthRealm.clear();
	m_lastProxyAuthRealm.clear();

	Manager< DummyNetworkAccessManager > mnam;
	mnam.setUnmatchedRequestBehavior( Forward );
	QObject::connect( &mnam,
	                  SIGNAL( authenticationRequired( QNetworkReply*, QAuthenticator* ) ),
	                  this,
	                  SLOT( authenticate( QNetworkReply*, QAuthenticator* ) ) );
	QObject::connect( &mnam,
	                  SIGNAL( proxyAuthenticationRequired( const QNetworkProxy&, QAuthenticator* ) ),
	                  this,
	                  SLOT( proxyAuthenticate( const QNetworkProxy&, QAuthenticator* ) ) );
	mnam.setRules( rules );

	const auto noSupportedAuthChallengeWarning = noSupportedAuthChallengeTemplate
	                                                 .arg( request.qRequest.url().toString() )
	                                                 .toUtf8();
	if ( QTest::currentDataTag() == QString( "401 without challenge" ) )
	{
		QTest::ignoreMessage( QtWarningMsg, noSupportedAuthChallengeWarning.data() );
	}
	else if ( QTest::currentDataTag() == QString( "unsupported auth scheme" ) )
	{
		QTest::ignoreMessage( QtWarningMsg, "Unsupported authentication scheme: \"fooauth\"" );
		QTest::ignoreMessage( QtWarningMsg, noSupportedAuthChallengeWarning.data() );
	}
	else if ( QTest::currentDataTag() == QString( "proxy auth" ) )
	{
		QTest::ignoreMessage( QtWarningMsg, "Proxy authentication is not supported at the moment" );
		QTest::ignoreMessage( QtWarningMsg, noSupportedAuthChallengeWarning.data() );
	}

	const auto reply = sendRequest( mnam, request );

	QTRY_VERIFY( reply->isFinished() );
	QTEST( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ), "statusCode" );
	QTEST( reply->error(), "error" );
	QTEST( m_lastAuthRealm, "expectedRealm" );
}

/*! Provides the data for the testAuthentication() test.
 */
void ManagerTest::testAuthentication_data()
{
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< RuleVector >( "rules" );
	QTest::addColumn< StringPair >( "credentials" );
	QTest::addColumn< QVariant >( "statusCode" );
	QTest::addColumn< QNetworkReply::NetworkError >( "error" );
	QTest::addColumn< QString >( "expectedRealm" );

	const QString defaultRealm( "world" );

	const QByteArray basicAuthChallenge( "Basic realm=\"world\", charset=\"UTF-8\"" );
	const QByteArray unsupportedSchemeChallenge( "FooAuth realm=\"bar\"" );
	const QByteArray multipleBasicAuthChallenges( "Basic realm=\"world\", Basic realm=\"second\"" );

	const QString username( "foo" );
	const QString password( "bar" );

	Rule::Ptr gerUnauthed( new Rule );
	gerUnauthed->has( Url( exampleRootUrl ) )
	    .has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( RawHeader( HttpUtils::authorizationHeader(), QByteArray() ) )
	    .reply()
	    .withStatus( HttpStatus::Unauthorized )
	    .withRawHeader( HttpUtils::wwwAuthenticateHeader(), basicAuthChallenge );

	Rule::Ptr gerAuthed( new Rule );
	gerAuthed->has( Url( exampleRootUrl ) )
	    .has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Authorization( username, password ) )
	    .reply()
	    .withStatus( HttpStatus::OK );

	Rule::Ptr gerUnauthedWithoutChallenge( new Rule );
	gerUnauthedWithoutChallenge->has( Url( exampleRootUrl ) )
	    .has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( RawHeader( HttpUtils::authorizationHeader(), QByteArray() ) )
	    .reply()
	    .withStatus( HttpStatus::Unauthorized );

	Rule::Ptr gerUnsupportedSchemeChallenge( new Rule );
	gerUnsupportedSchemeChallenge->has( Url( exampleRootUrl ) )
	    .has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( RawHeader( HttpUtils::authorizationHeader(), QByteArray() ) )
	    .reply()
	    .withStatus( HttpStatus::Unauthorized )
	    .withRawHeader( HttpUtils::wwwAuthenticateHeader(), unsupportedSchemeChallenge );

	Rule::Ptr gerMultipleBasicChallenges( new Rule );
	gerMultipleBasicChallenges->has( Url( exampleRootUrl ) )
	    .has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( RawHeader( HttpUtils::authorizationHeader(), QByteArray() ) )
	    .reply()
	    .withStatus( HttpStatus::Unauthorized )
	    .withRawHeader( HttpUtils::wwwAuthenticateHeader(), multipleBasicAuthChallenges );

	Rule::Ptr gerProxyAuthRequired( new Rule );
	gerProxyAuthRequired->has( Url( exampleRootUrl ) )
	    .has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( RawHeader( HttpUtils::proxyAuthorizationHeader(), QByteArray() ) )
	    .reply()
	    .withStatus( HttpStatus::ProxyAuthenticationRequired )
	    .withRawHeader( HttpUtils::proxyAuthenticateHeader(), basicAuthChallenge );

	const auto statusUnauthorizedVariant = QVariant{ static_cast< int >( HttpStatus::Unauthorized ) };


	QTest::newRow( "basic auth" ) << getExampleRootRequest << ( RuleVector() << gerUnauthed << gerAuthed )
	                              << qMakePair( username, password ) << statusOkVariant << noError << defaultRealm;

	QTest::newRow( "auth and pass through" )
	    << getExampleRootRequest << ( RuleVector() << gerUnauthed ) << qMakePair( username, password ) << QVariant()
	    << operationCanceledError << defaultRealm;

	QTest::newRow( "401 without challenge" )
	    << getExampleRootRequest << ( RuleVector() << gerUnauthedWithoutChallenge << gerAuthed )
	    << qMakePair( username, password ) << statusUnauthorizedVariant << QNetworkReply::AuthenticationRequiredError
	    << QString();

	QTest::newRow( "no credentials" ) << getExampleRootRequest << ( RuleVector() << gerUnauthed << gerAuthed )
	                                  << qMakePair( QString(), QString() ) << statusUnauthorizedVariant
	                                  << QNetworkReply::AuthenticationRequiredError << defaultRealm;

	QTest::newRow( "unsupported auth scheme" )
	    << getExampleRootRequest << ( RuleVector() << gerUnsupportedSchemeChallenge << gerAuthed )
	    << qMakePair( username, password ) << statusUnauthorizedVariant << QNetworkReply::AuthenticationRequiredError
	    << QString();

	QTest::newRow( "multiple basic auths" )
	    << getExampleRootRequest << ( RuleVector() << gerMultipleBasicChallenges << gerAuthed )
	    << qMakePair( username, password ) << statusOkVariant << noError << "second";

	QTest::newRow( "proxy auth" ) << getExampleRootRequest << ( RuleVector() << gerProxyAuthRequired )
	                              << qMakePair( username, password )
	                              << QVariant{ static_cast< int >( HttpStatus::ProxyAuthenticationRequired ) }
	                              << QNetworkReply::ProxyAuthenticationRequiredError << QString();
}

/*! \test %Tests the authentication cache functionality of the MockNetworkAccess::Manager.
 */
void ManagerTest::testAuthenticationCache()
{
	QFETCH( Request, request );

	QMutexLocker locker( &m_credentialMutex );
	m_username = "foo";
	m_password = "bar";

	Manager< DummyNetworkAccessManager > mnam;
	mnam.whenGet( exampleRootUrl )
	    .has( RawHeader( HttpUtils::authorizationHeader(), QByteArray() ) )
	    .reply()
	    .withStatus( HttpStatus::Unauthorized )
	    .withAuthenticate( "world" );
	mnam.whenGet( exampleRootUrl ).has( Authorization( m_username, m_password ) ).reply().withStatus( HttpStatus::OK );
	QObject::connect( &mnam,
	                  SIGNAL( authenticationRequired( QNetworkReply*, QAuthenticator* ) ),
	                  this,
	                  SLOT( authenticate( QNetworkReply*, QAuthenticator* ) ) );

	QSignalSpy authenticationRequiredSpy( &mnam, SIGNAL( authenticationRequired( QNetworkReply*, QAuthenticator* ) ) );

	auto reply = sendRequest( mnam, request );
	QTRY_VERIFY( reply->isFinished() );

	reply = sendRequest( mnam, request );
	QTRY_VERIFY( reply->isFinished() );

	QTEST( static_cast< int >( authenticationRequiredSpy.count() ), "expectedAuthRequiredSignals" );
	QCOMPARE( reply->error(), noError );
	QCOMPARE( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt(), static_cast< int >( HttpStatus::OK ) );
}

/*! Provides the data for the testAuthenticationCache() test.
 */
void ManagerTest::testAuthenticationCache_data()
{
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< int >( "expectedAuthRequiredSignals" );

	Request noAuthCacheRequest = getExampleRootRequest;
	noAuthCacheRequest.qRequest.setAttribute( QNetworkRequest::AuthenticationReuseAttribute,
	                                          QVariant::fromValue( static_cast< int >( QNetworkRequest::Manual ) ) );

	QTest::newRow( "no cache" ) << noAuthCacheRequest << 2;
	QTest::newRow( "with cache" ) << getExampleRootRequest << 1;
}

/*! \test %Tests the Manager::clearAuthenticationCache() method.
 */
void ManagerTest::testClearAuthenticationCache()
{
	QMutexLocker locker( &m_credentialMutex );
	m_username = "foo";
	m_password = "bar";

	Manager< DummyNetworkAccessManager > mnam;
	mnam.whenGet( exampleRootUrl )
	    .has( RawHeader( HttpUtils::authorizationHeader(), QByteArray() ) )
	    .reply()
	    .withStatus( HttpStatus::Unauthorized )
	    .withAuthenticate( "world" );
	mnam.whenGet( exampleRootUrl ).has( Authorization( m_username, m_password ) ).reply().withStatus( HttpStatus::OK );
	QObject::connect( &mnam,
	                  SIGNAL( authenticationRequired( QNetworkReply*, QAuthenticator* ) ),
	                  this,
	                  SLOT( authenticate( QNetworkReply*, QAuthenticator* ) ) );

	QSignalSpy authenticationRequiredSpy( &mnam, SIGNAL( authenticationRequired( QNetworkReply*, QAuthenticator* ) ) );

	auto reply = sendRequest( mnam, getExampleRootRequest );
	QTRY_VERIFY( reply->isFinished() );

	mnam.clearAuthenticationCache();

	reply = sendRequest( mnam, getExampleRootRequest );
	QTRY_VERIFY( reply->isFinished() );

	QCOMPARE( static_cast< int >( authenticationRequiredSpy.count() ), 2 );
	QCOMPARE( reply->error(), noError );
	QCOMPARE( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt(), static_cast< int >( HttpStatus::OK ) );
}

/*! \test %Tests the cookie handling of the Manager.
 */
void ManagerTest::testCookies()
{
	QFETCH( Request, request );
	QFETCH( int, cookieExpiry );

	QNetworkCookie cookie( "dummyCookie", "foo" );
	cookie.setDomain( ".example.com" );
	cookie.setPath( "/" );

	if ( cookieExpiry == 0 )
		cookie.setExpirationDate( QDateTime() ); // Make it a session cookie
	else
		cookie.setExpirationDate( QDateTime::currentDateTime().addMSecs( cookieExpiry ) );

	QList< QNetworkCookie > setCookies;
	setCookies << cookie;

	const QVariant setCookiesVariant = QVariant::fromValue( setCookies );

	const QByteArray cookieHeader( "Cookie" );

	Rule::Ptr gerWithCookie( getExampleRootRule() );
	gerWithCookie->has( RawHeader( cookieHeader, cookie.toRawForm( QNetworkCookie::NameAndValueOnly ) ) )
	    .reply()
	    .withBody( cookieSent );

	Rule::Ptr gerSetCookie( getExampleRootRule() );
	gerSetCookie->hasNot( RawHeader( cookieHeader, cookie.toRawForm( QNetworkCookie::NameAndValueOnly ) ) )
	    .reply()
	    .withHeader( QNetworkRequest::SetCookieHeader, setCookiesVariant )
	    .withStatus( HttpStatus::Created )
	    .withBody( cookieCreated );


	Manager< DummyNetworkAccessManager > mnam;
	mnam.addRule( gerSetCookie );
	mnam.addRule( gerWithCookie );

	auto reply = sendRequest( mnam, request );
	QTRY_VERIFY( reply->isFinished() );

	QTEST( reply->readAll(), "firstReply" );

	reply = sendRequest( mnam, request );
	QTRY_VERIFY( reply->isFinished() );

	QTEST( reply->readAll(), "secondReply" );
}

/*! Provides the data for the testCookies() test.
 */
void ManagerTest::testCookies_data()
{
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< int >( "cookieExpiry" );
	QTest::addColumn< QByteArray >( "firstReply" );
	QTest::addColumn< QByteArray >( "secondReply" );

	Request noCookieSaveRequest = getExampleRootRequest;
	noCookieSaveRequest.qRequest.setAttribute( QNetworkRequest::CookieSaveControlAttribute,
	                                           static_cast< int >( QNetworkRequest::Manual ) );
	Request noCookieLoadRequest = getExampleRootRequest;
	noCookieLoadRequest.qRequest.setAttribute( QNetworkRequest::CookieLoadControlAttribute,
	                                           static_cast< int >( QNetworkRequest::Manual ) );

	// clang-format off
	//                                   // request               // cookieExpiry // firstReply    // secondReply
	QTest::newRow( "session cookie" )    << getExampleRootRequest << 0            << cookieCreated << cookieSent;
	QTest::newRow( "expiring cookie" )   << getExampleRootRequest << 10000        << cookieCreated << cookieSent;
	QTest::newRow( "expired cookie" )    << getExampleRootRequest << -1000        << cookieCreated << cookieCreated;
	QTest::newRow( "no automatic save" ) << noCookieSaveRequest   << 0            << cookieCreated << cookieCreated;
	QTest::newRow( "no automatic load" ) << noCookieLoadRequest   << 0            << cookieCreated << cookieCreated;
	// clang-format on
}

/*! \test %Tests the detail::usesSafeRedirectRequestMethod()
 */
void ManagerTest::testUsesSafeRedirectRequestMethod()
{
	QFETCH( Request, request );

	QTEST( detail::usesSafeRedirectRequestMethod( request ), "usesSafeMethod" );
}

/*! Provides the data for the ManagerTest::testUsesSafeRedirectRequestMethod() test.
 */
void ManagerTest::testUsesSafeRedirectRequestMethod_data()
{
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< bool >( "usesSafeMethod" );

	QTest::newRow( "GET" ) << getExampleRootRequest << true;
	QTest::newRow( "POST" ) << postFooRequest() << false;

	Request deleteRequest = getExampleRootRequest;
	deleteRequest.operation = QNetworkAccessManager::DeleteOperation;
	QTest::newRow( "DELETE" ) << deleteRequest << false;

	Request headRequest = getExampleRootRequest;
	headRequest.operation = QNetworkAccessManager::HeadOperation;
	QTest::newRow( "HEAD" ) << headRequest << true;

	Request optionsRequest = getExampleRootRequest;
	optionsRequest.operation = QNetworkAccessManager::CustomOperation;
	optionsRequest.qRequest.setAttribute( QNetworkRequest::CustomVerbAttribute, QByteArray( "OPTIONS" ) );
	QTest::newRow( "OPTIONS" ) << optionsRequest << true;

	Request traceRequest = getExampleRootRequest;
	traceRequest.operation = QNetworkAccessManager::CustomOperation;
	traceRequest.qRequest.setAttribute( QNetworkRequest::CustomVerbAttribute, QByteArray( "TRACE" ) );
	QTest::newRow( "TRACE" ) << traceRequest << true;
}


/*! \test %Tests the automatic redirect (follow redirect) functionality of the MockNetworkAccessManager.
 */
void ManagerTest::testAutoRedirect()
{
	QFETCH( BehaviorFlags, behaviorFlags );
	QFETCH( int, namRedirectPolicy );
	QFETCH( Request, request );
	QFETCH( RuleVector, rules );
	QFETCH( int, statusCode );
	QFETCH( QByteArray, expectedLocationHeader );

	Manager< DummyNetworkAccessManager > mnam;
	mnam.setBehaviorFlags( behaviorFlags );
	mnam.setUnmatchedRequestBehavior( Forward );
	mnam.setRules( rules );

#if QT_VERSION >= QT_VERSION_CHECK( 5,9,0 )
	if ( namRedirectPolicy >= 0 )
		mnam.setRedirectPolicy( static_cast< QNetworkRequest::RedirectPolicy >( namRedirectPolicy ) );
#else // Qt < 5.9.0
	Q_UNUSED( namRedirectPolicy )
#endif


	if ( QTest::currentDataTag() == QString( "user verified redirect" ) )
		QTest::ignoreMessage( QtWarningMsg, "User verified redirection policy is not supported at the moment" );

	ReplyPtr reply( sendRequest( mnam, request ) );
	QTRY_VERIFY( reply->isFinished() );

	QEXPECT_FAIL( "user verified redirect",
	              "QNetworkRequest::UserVerifiedRedirectPolicy is not supported at the moment",
	              Abort );

	if ( statusCode < 0 )
		QVERIFY( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).isNull() );
	else
		QCOMPARE( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt(), statusCode );
	QTEST( reply->url(), "replyUrl" );
	QTEST( reply->error(), "error" );
	QTEST( static_cast< int >( reply->operation() ), "operation" );
	const auto locationHeader = reply->rawHeader( HttpUtils::locationHeader() );

	QCOMPARE( locationHeader.isNull(), expectedLocationHeader.isNull() );
	if ( ! expectedLocationHeader.isNull() )
		QCOMPARE( locationHeader, expectedLocationHeader );
	if ( HttpStatus::isRedirection( statusCode ) )
		QTEST( reply->attribute( QNetworkRequest::RedirectionTargetAttribute ).toUrl(), "redirectTarget" );
	else
		QVERIFY( reply->attribute( QNetworkRequest::RedirectionTargetAttribute ).isNull() );
}

/*! Provides the data for the testAutoRedirect() test.
 */
void ManagerTest::testAutoRedirect_data()
{
	QTest::addColumn< BehaviorFlags >( "behaviorFlags" );
	QTest::addColumn< int >( "namRedirectPolicy" );
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< RuleVector >( "rules" );
	QTest::addColumn< QNetworkReply::NetworkError >( "error" );
	QTest::addColumn< int >( "statusCode" );
	QTest::addColumn< QUrl >( "replyUrl" );
	QTest::addColumn< QByteArray >( "expectedLocationHeader" );
	QTest::addColumn< QUrl >( "redirectTarget" );
	QTest::addColumn< int >( "operation" );


#if QT_VERSION >= QT_VERSION_CHECK( 5,9,0 )
	const int noLessSafeRedirectPolicy = static_cast< int >( QNetworkRequest::NoLessSafeRedirectPolicy );
#endif // Qt >= 5.9.0

	const QNetworkReply::NetworkError insecureRedirectError = QNetworkReply::InsecureRedirectError;

	Rule::Ptr unsafeRedirect( new Rule );
	unsafeRedirect->has( Url( httpsExampleRootUrl ) ).reply().withRedirect( fooUrl );

	Rule::Ptr gerRedirect308( new Rule );
	gerRedirect308->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( exampleRootUrl ) )
	    .reply()
	    .withRedirect( fooUrl, HttpStatus::PermanentRedirect );

	const int unknownRedirectCode = 388;
	Rule::Ptr gerRedirectUnknown( new Rule );
	gerRedirectUnknown->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( exampleRootUrl ) )
	    .reply()
	    .withRedirect( fooUrl )
	    .withAttribute( QNetworkRequest::HttpStatusCodeAttribute, unknownRedirectCode );

	Rule::Ptr gerNotModified( new Rule );
	gerNotModified->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( exampleRootUrl ) )
	    .reply()
	    .withStatus( HttpStatus::NotModified )
	    .withRawHeader( "Date", "Sun, 06 Nov 1994 08:49:37 GMT" );

	Rule::Ptr perRedirect302( new Rule );
	perRedirect302->has( Verb( QNetworkAccessManager::PostOperation ) )
	    .has( Url( exampleRootUrl ) )
	    .reply()
	    .withRedirect( fooUrl, HttpStatus::Found );

	Rule::Ptr perRedirect307( new Rule );
	perRedirect307->has( Verb( QNetworkAccessManager::PostOperation ) )
	    .has( Url( exampleRootUrl ) )
	    .reply()
	    .withRedirect( fooUrl, HttpStatus::TemporaryRedirect );

	Rule::Ptr perRedirect303( new Rule );
	perRedirect303->has( Verb( QNetworkAccessManager::PostOperation ) )
	    .has( Url( exampleRootUrl ) )
	    .reply()
	    .withRedirect( fooUrl, HttpStatus::SeeOther );

	Rule::Ptr exampleRootRedirect( new Rule );
	exampleRootRedirect->has( Url( exampleRootUrl ) ).reply().withRedirect( fooUrl );

	Rule::Ptr fooOptionsRule( new Rule );
	fooOptionsRule->has( Verb( QNetworkAccessManager::CustomOperation, "OPTIONS" ) )
	    .has( Url( fooUrl ) )
	    .reply()
	    .withStatus( HttpStatus::OK );

	const auto fooRelative = QByteArrayLiteral( "foo" );
	Rule::Ptr gerRelativeRedirectRule( new Rule );
	gerRelativeRedirectRule->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( exampleRootUrl ) )
	    .reply()
	    .withRedirect( QUrl( QString::fromLatin1( fooRelative ) ) );

	const auto fooProtocolRelative = QByteArrayLiteral( "//example.com/foo" );
	Rule::Ptr gerProtRelRedirectRule( new Rule );
	gerProtRelRedirectRule->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( exampleRootUrl ) )
	    .reply()
	    .withRedirect( QUrl( QString::fromLatin1( fooProtocolRelative ) ) );

	const QUrl fooProtocolUrl( "foo://bar" );
	Rule::Ptr unsupportedProtocolRedirect( new Rule );
	unsupportedProtocolRedirect->has( Url( exampleRootUrl ) ).reply().withRedirect( fooProtocolUrl );

	const auto invalidUrl = QByteArrayLiteral( "http://.." );
	Rule::Ptr invalidTargetRedirect( new Rule );
	invalidTargetRedirect->has( Url( exampleRootUrl ) )
	    .reply()
	    .withRawHeader( HttpUtils::locationHeader(), invalidUrl )
	    .withStatus( HttpStatus::Found );

	const QUrl barUrl( "http://example.com/bar" );
	Rule::Ptr getFooRedirect( new Rule );
	getFooRedirect->has( Verb( QNetworkAccessManager::GetOperation ) ).has( Url( fooUrl ) ).reply().withRedirect( barUrl );

	Rule::Ptr getBarRule( new Rule );
	getBarRule->has( Verb( QNetworkAccessManager::GetOperation ) ).has( Url( barUrl ) ).reply().withStatus( HttpStatus::OK );

	Request gerNoFollowRedirectRequest = getExampleRootRequest;
	enableAutomaticRedirect( gerNoFollowRedirectRequest.qRequest, false );

	const QNetworkRequest httpsExampleRootRequest( httpsExampleRootUrl );
	Request httpsGerFollowRedirectRequest( httpsExampleRootRequest );
	enableAutomaticRedirect( httpsGerFollowRedirectRequest.qRequest );

	Request perFollowRedirectRequest( QNetworkAccessManager::PostOperation, QNetworkRequest( exampleRootUrl ), fooObject );
	enableAutomaticRedirect( perFollowRedirectRequest.qRequest );

	Request optionsFollowRedirectRequest( QNetworkAccessManager::CustomOperation, QNetworkRequest( exampleRootUrl ) );
	optionsFollowRedirectRequest.qRequest.setAttribute( QNetworkRequest::CustomVerbAttribute, QString( "OPTIONS" ) );
	enableAutomaticRedirect( optionsFollowRedirectRequest.qRequest );

	Request customVerbFollowRedirectRequest( QNetworkAccessManager::CustomOperation, QNetworkRequest( exampleRootUrl ) );
	customVerbFollowRedirectRequest.qRequest.setAttribute( QNetworkRequest::CustomVerbAttribute, QString( "FOO" ) );
	enableAutomaticRedirect( customVerbFollowRedirectRequest.qRequest );

	const BehaviorFlags qt_5_6_Behavior = Behavior_Qt_5_6_0;
	const BehaviorFlags expectedBehavior = Behavior_Expected;

	// clang-format off
	//                                                                // behaviorFlags    // namRedirectPolicy    // request                         // rules                                                                      // error                  // statusCode                                          // replyUrl            // expectedLocationHeader     // redirectTarget // operation
	QTest::newRow( "follow redirect not enabled" )                    << qt_5_6_Behavior  << manualRedirectPolicy << getExampleRootRequest           << ( RuleVector() << gerToFooRedirectRule() << getFooRule() )                 << noError                << static_cast< int >( HttpStatus::Found )             << exampleRootUrl      << fooUrl.toEncoded()         << fooUrl         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "follow redirect disabled" )                       << qt_5_6_Behavior  << manualRedirectPolicy << gerNoFollowRedirectRequest      << ( RuleVector() << gerToFooRedirectRule() << getFooRule() )                 << noError                << static_cast< int >( HttpStatus::Found )             << exampleRootUrl      << fooUrl.toEncoded()         << fooUrl         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "unsafe follow redirect" )                         << qt_5_6_Behavior  << manualRedirectPolicy << httpsGerFollowRedirectRequest   << ( RuleVector() << unsafeRedirect )                                         << insecureRedirectError  << static_cast< int >( HttpStatus::Found )             << httpsExampleRootUrl << fooUrl.toEncoded()         << fooUrl         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "redirect not to be followed" )                    << qt_5_6_Behavior  << manualRedirectPolicy << gerFollowRedirectRequest()      << ( RuleVector() << gerNotModified )                                         << noError                << static_cast< int >( HttpStatus::NotModified )       << exampleRootUrl      << QByteArray{}               << QUrl()         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "redirect match rule" )                            << expectedBehavior << manualRedirectPolicy << gerFollowRedirectRequest()      << ( RuleVector() << gerToFooRedirectRule() << getFooRule() )                 << noError                << static_cast< int >( HttpStatus::OK )                << fooUrl              << QByteArray{}               << QUrl()         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "redirect forward" )                               << expectedBehavior << manualRedirectPolicy << gerFollowRedirectRequest()      << ( RuleVector() << gerToFooRedirectRule() )                                 << operationCanceledError << -1                                                  << fooUrl              << QByteArray{}               << QUrl()         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "multiple redirect" )                              << expectedBehavior << manualRedirectPolicy << gerFollowRedirectRequest()      << ( RuleVector() << gerToFooRedirectRule() << getFooRedirect << getBarRule ) << noError                << static_cast< int >( HttpStatus::OK )                << barUrl              << QByteArray{}               << QUrl()         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "multiple redirect, then forward" )                << expectedBehavior << manualRedirectPolicy << gerFollowRedirectRequest()      << ( RuleVector() << gerToFooRedirectRule() << getFooRedirect )               << operationCanceledError << -1                                                  << barUrl              << QByteArray{}               << QUrl()         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "POST redirect 301 (Qt 5.6 behavior)" )            << qt_5_6_Behavior  << manualRedirectPolicy << perFollowRedirectRequest        << ( RuleVector() << perRedirect302 << getFooRule() )                         << noError                << static_cast< int >( HttpStatus::OK )                << fooUrl              << fooUrl.toEncoded()         << QUrl()         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "POST redirect 301 (expected behavior)" )          << expectedBehavior << manualRedirectPolicy << perFollowRedirectRequest        << ( RuleVector() << perRedirect302 << getFooRule() )                         << noError                << static_cast< int >( HttpStatus::OK )                << fooUrl              << QByteArray{}               << QUrl()         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "POST redirect 307 (Qt 5.6 behavior)" )            << qt_5_6_Behavior  << manualRedirectPolicy << perFollowRedirectRequest        << ( RuleVector() << perRedirect307 << postFooRule() )                        << operationCanceledError << -1                                                  << fooUrl              << fooUrl.toEncoded()         << QUrl()         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "POST redirect 307 (expected behavior)" )          << expectedBehavior << manualRedirectPolicy << perFollowRedirectRequest        << ( RuleVector() << perRedirect307 << postFooRule() )                        << noError                << static_cast< int >( HttpStatus::OK )                << fooUrl              << QByteArray{}               << QUrl()         << static_cast< int >( QNetworkAccessManager::PostOperation );
	QTest::newRow( "GET redirect 308 (Qt 5.6 behavior)" )             << qt_5_6_Behavior  << manualRedirectPolicy << gerFollowRedirectRequest()      << ( RuleVector() << gerRedirect308 << getFooRule() )                         << noError                << static_cast< int >( HttpStatus::PermanentRedirect ) << exampleRootUrl      << fooUrl.toEncoded()         << QUrl()         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "GET redirect 308 (expected behavior)" )           << expectedBehavior << manualRedirectPolicy << gerFollowRedirectRequest()      << ( RuleVector() << gerRedirect308 << getFooRule() )                         << noError                << static_cast< int >( HttpStatus::OK )                << fooUrl              << QByteArray{}               << QUrl()         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "POST redirect 303 (expected behavior)" )          << expectedBehavior << manualRedirectPolicy << perFollowRedirectRequest        << ( RuleVector() << perRedirect303 << getFooRule() )                         << noError                << static_cast< int >( HttpStatus::OK )                << fooUrl              << QByteArray{}               << QUrl()         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "unknown redirect code" )                          << expectedBehavior << manualRedirectPolicy << gerFollowRedirectRequest()      << ( RuleVector() << gerRedirectUnknown << getFooRule() )                     << noError                << unknownRedirectCode                                 << exampleRootUrl      << fooUrl.toEncoded()         << QUrl()         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "redirect unsupported protocol" )                  << qt_5_6_Behavior  << manualRedirectPolicy << gerFollowRedirectRequest()      << ( RuleVector() << unsupportedProtocolRedirect )                            << protocolUnknownError   << static_cast< int >( HttpStatus::Found )             << exampleRootUrl      << fooProtocolUrl.toEncoded() << QUrl()         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "no follow unsupported protocol redirect" )        << qt_5_6_Behavior  << manualRedirectPolicy << gerNoFollowRedirectRequest      << ( RuleVector() << unsupportedProtocolRedirect )                            << noError                << static_cast< int >( HttpStatus::Found )             << exampleRootUrl      << fooProtocolUrl.toEncoded() << fooProtocolUrl << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "relative redirect (Qt 5.6 behavior)" )            << qt_5_6_Behavior  << manualRedirectPolicy << gerFollowRedirectRequest()      << ( RuleVector() << gerRelativeRedirectRule << getFooRule() )                << noError                << static_cast< int >( HttpStatus::OK )                << fooUrl              << fooRelative                << QUrl()         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "relative redirect (expected behavior)" )          << expectedBehavior << manualRedirectPolicy << gerFollowRedirectRequest()      << ( RuleVector() << gerRelativeRedirectRule << getFooRule() )                << noError                << static_cast< int >( HttpStatus::OK )                << fooUrl              << QByteArray{}               << QUrl()         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "protocol relative redirect (Qt 5.6 behavior)" )   << qt_5_6_Behavior  << manualRedirectPolicy << gerFollowRedirectRequest()      << ( RuleVector() << gerProtRelRedirectRule << getFooRule() )                 << noError                << static_cast< int >( HttpStatus::OK )                << fooUrl              << fooProtocolRelative        << QUrl()         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "protocol relative redirect (expected behavior)" ) << expectedBehavior << manualRedirectPolicy << gerFollowRedirectRequest()      << ( RuleVector() << gerProtRelRedirectRule << getFooRule() )                 << noError                << static_cast< int >( HttpStatus::OK )                << fooUrl              << QByteArray{}               << QUrl()         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "OPTIONS redirect (expected behavior)" )           << expectedBehavior << manualRedirectPolicy << optionsFollowRedirectRequest    << ( RuleVector() << exampleRootRedirect << fooOptionsRule )                  << noError                << static_cast< int >( HttpStatus::OK )                << fooUrl              << QByteArray{}               << QUrl()         << static_cast< int >( QNetworkAccessManager::CustomOperation );
	QTest::newRow( "custom verb redirect (expected behavior)" )       << expectedBehavior << manualRedirectPolicy << customVerbFollowRedirectRequest << ( RuleVector() << exampleRootRedirect << getFooRule() )                    << noError                << static_cast< int >( HttpStatus::OK )                << fooUrl              << QByteArray{}               << QUrl()         << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "invalid redirect target" )                        << expectedBehavior << manualRedirectPolicy << gerFollowRedirectRequest()      << ( RuleVector() << invalidTargetRedirect )                                  << protocolUnknownError   << static_cast< int >( HttpStatus::Found )             << exampleRootUrl      << invalidUrl                 << QUrl()         << static_cast< int >( QNetworkAccessManager::GetOperation );
	// clang-format on


	const QUrl redirect1Url( "http://example.com/redir1" );

	Rule::Ptr redirect1( new Rule );
	redirect1->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( exampleRootUrl ) )
	    .reply()
	    .withRedirect( redirect1Url );
	Rule::Ptr redirect2( new Rule );
	redirect2->has( Verb( QNetworkAccessManager::GetOperation ) ).has( Url( redirect1Url ) ).reply().withRedirect( fooUrl );

	Request gerLimitedFollowRedirectRequest = gerFollowRedirectRequest();
	gerLimitedFollowRedirectRequest.qRequest.setMaximumRedirectsAllowed( 1 );

	QTest::newRow( "redirect limit" ) << qt_5_6_Behavior << manualRedirectPolicy << gerLimitedFollowRedirectRequest
	                                  << ( RuleVector() << getFooRule() << redirect1 << redirect2 )
	                                  << QNetworkReply::TooManyRedirectsError << static_cast< int >( HttpStatus::Found )
	                                  << redirect1Url << fooUrl.toEncoded() << fooUrl
	                                  << static_cast< int >( QNetworkAccessManager::GetOperation );

#if QT_VERSION >= QT_VERSION_CHECK( 5,9,0 )

	Request safeRedirectRequest( httpsExampleRootRequest );
	safeRedirectRequest.qRequest.setAttribute( QNetworkRequest::RedirectPolicyAttribute,
	                                           static_cast< int >( QNetworkRequest::NoLessSafeRedirectPolicy ) );

	Rule::Ptr otherOriginRedirect( new Rule );
	otherOriginRedirect->has( Url( httpsExampleRootUrl ) ).reply().withRedirect( httpsOtherOriginUrl );

	Rule::Ptr otherOrigin( new Rule );
	otherOrigin->has( Url( httpsOtherOriginUrl ) ).reply().withStatus( HttpStatus::OK );

	QTest::newRow( "safe redirect" ) << expectedBehavior << manualRedirectPolicy << safeRedirectRequest
	                                 << ( RuleVector() << otherOriginRedirect << otherOrigin ) << noError
	                                 << static_cast< int >( HttpStatus::OK ) << httpsOtherOriginUrl << QByteArray{}
	                                 << QUrl{} << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "unsafe redirect" ) << expectedBehavior << manualRedirectPolicy << safeRedirectRequest
	                                   << ( RuleVector() << unsafeRedirect ) << insecureRedirectError
	                                   << static_cast< int >( HttpStatus::Found ) << httpsExampleRootUrl
	                                   << fooUrl.toEncoded() << fooUrl
	                                   << static_cast< int >( QNetworkAccessManager::GetOperation );

	auto gerSafeRedirectRequest = getExampleRootRequest;
	gerSafeRedirectRequest.qRequest.setAttribute( QNetworkRequest::RedirectPolicyAttribute,
	                                              static_cast< int >( QNetworkRequest::NoLessSafeRedirectPolicy ) );

	QTest::newRow( "safe redirect invalid redirect target" )
	    << expectedBehavior << manualRedirectPolicy << gerSafeRedirectRequest
	    << ( RuleVector() << invalidTargetRedirect ) << protocolUnknownError << static_cast< int >( HttpStatus::Found )
	    << exampleRootUrl << invalidUrl << QUrl() << static_cast< int >( QNetworkAccessManager::GetOperation );

	Request policyRedirectPrecedenceRequest( getExampleRootRequest );
	policyRedirectPrecedenceRequest.qRequest.setAttribute( QNetworkRequest::RedirectPolicyAttribute,
	                                                       noLessSafeRedirectPolicy );

	QTest::newRow( "follow by policy has precedence" )
	    << expectedBehavior << manualRedirectPolicy << policyRedirectPrecedenceRequest
	    << ( RuleVector() << gerToFooRedirectRule() << getFooRule() ) << noError << static_cast< int >( HttpStatus::OK )
	    << fooUrl << QByteArray{} << QUrl{} << static_cast< int >( QNetworkAccessManager::GetOperation );

	Request policyNoRedirectPrecedenceRequest( getExampleRootRequest );
	policyNoRedirectPrecedenceRequest.qRequest.setAttribute( QNetworkRequest::RedirectPolicyAttribute,
	                                                         manualRedirectPolicy );

	QTest::newRow( "no follow by policy has precedence" )
	    << expectedBehavior << noLessSafeRedirectPolicy << policyNoRedirectPrecedenceRequest
	    << ( RuleVector() << gerToFooRedirectRule() << getFooRule() ) << noError
	    << static_cast< int >( HttpStatus::Found ) << exampleRootUrl << fooUrl.toEncoded() << fooUrl
	    << static_cast< int >( QNetworkAccessManager::GetOperation );

	Request sameOriginRedirectRequest( httpsExampleRootRequest );
	sameOriginRedirectRequest.qRequest.setAttribute( QNetworkRequest::RedirectPolicyAttribute,
	                                                 static_cast< int >( QNetworkRequest::SameOriginRedirectPolicy ) );

	Rule::Ptr sameOriginRedirect( new Rule );
	sameOriginRedirect->has( Url( httpsExampleRootUrl ) ).reply().withRedirect( httpsFooUrl );

	Rule::Ptr getHttpsFoo( new Rule );
	getHttpsFoo->has( Url( httpsFooUrl ) ).reply().withStatus( HttpStatus::OK );

	QTest::newRow( "same origin redirect" )
	    << expectedBehavior << manualRedirectPolicy << sameOriginRedirectRequest
	    << ( RuleVector() << sameOriginRedirect << getHttpsFoo ) << noError << static_cast< int >( HttpStatus::OK )
	    << httpsFooUrl << QByteArray{} << QUrl{} << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "other origin redirect" )
	    << expectedBehavior << manualRedirectPolicy << sameOriginRedirectRequest
	    << ( RuleVector() << otherOriginRedirect << otherOrigin ) << insecureRedirectError
	    << static_cast< int >( HttpStatus::Found ) << httpsExampleRootUrl << httpsOtherOriginUrl.toEncoded()
	    << httpsOtherOriginUrl << static_cast< int >( QNetworkAccessManager::GetOperation );
	QTest::newRow( "other protocol redirect" )
	    << expectedBehavior << manualRedirectPolicy << sameOriginRedirectRequest
	    << ( RuleVector() << unsafeRedirect << getFooRule() ) << insecureRedirectError
	    << static_cast< int >( HttpStatus::Found ) << httpsExampleRootUrl << fooUrl.toEncoded() << fooUrl
	    << static_cast< int >( QNetworkAccessManager::GetOperation );

	QTest::newRow( "nam policy redirect" )
	    << expectedBehavior << static_cast< int >( QNetworkRequest::NoLessSafeRedirectPolicy ) << getExampleRootRequest
	    << ( RuleVector() << getFooRule() << gerToFooRedirectRule() ) << noError << static_cast< int >( HttpStatus::OK )
	    << fooUrl << QByteArray{} << QUrl{} << static_cast< int >( QNetworkAccessManager::GetOperation );

	QTest::newRow( "nam policy invalid redirect target" )
	    << expectedBehavior << static_cast< int >( QNetworkRequest::NoLessSafeRedirectPolicy ) << getExampleRootRequest
	    << ( RuleVector() << invalidTargetRedirect ) << protocolUnknownError << static_cast< int >( HttpStatus::Found )
	    << exampleRootUrl << invalidUrl << QUrl{} << static_cast< int >( QNetworkAccessManager::GetOperation );

	Request httpsGerRequest( httpsExampleRootRequest );

	QTest::newRow( "nam same origin redirect" )
	    << expectedBehavior << static_cast< int >( QNetworkRequest::SameOriginRedirectPolicy ) << httpsGerRequest
	    << ( RuleVector() << sameOriginRedirect << getHttpsFoo ) << noError << static_cast< int >( HttpStatus::OK )
	    << httpsFooUrl << QByteArray{} << QUrl{} << static_cast< int >( QNetworkAccessManager::GetOperation );

	Request userVerifiedRedirectRequest( QNetworkAccessManager::GetOperation, QNetworkRequest( exampleRootUrl ) );
	userVerifiedRedirectRequest.qRequest.setAttribute( QNetworkRequest::RedirectPolicyAttribute,
	                                                   static_cast< int >( QNetworkRequest::UserVerifiedRedirectPolicy ) );
	QTest::newRow( "user verified redirect" )
	    << expectedBehavior << manualRedirectPolicy << userVerifiedRedirectRequest
	    << ( RuleVector() << exampleRootRedirect << getFooRule() ) << noError << static_cast< int >( HttpStatus::OK )
	    << fooUrl << QByteArray{} << QUrl{} << static_cast< int >( QNetworkAccessManager::GetOperation );
#endif // Qt >= 5.9.0
}

/*! \test %Tests automatic redirection after authentication and vice versa.
 */
void ManagerTest::testRedirectAndAuthentication()
{
	QFETCH( RuleVector, rules );

	Manager< DummyNetworkAccessManager > mnam;
	QObject::connect( &mnam,
	                  SIGNAL( authenticationRequired( QNetworkReply*, QAuthenticator* ) ),
	                  this,
	                  SLOT( authenticate( QNetworkReply*, QAuthenticator* ) ) );
	mnam.setRules( rules );

	Request unauthedRequest( getExampleRootRequest );
	enableAutomaticRedirect( unauthedRequest.qRequest );

	auto reply = sendRequest( mnam, unauthedRequest );
	QTRY_VERIFY( reply->isFinished() );

	QTEST( reply->url(), "replyUrl" );
	QTEST( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt(), "statusCode" );
}

/*! Provides the data for the testRedirectAndAuthentication() test.
 */
void ManagerTest::testRedirectAndAuthentication_data()
{
	QTest::addColumn< RuleVector >( "rules" );
	QTest::addColumn< QUrl >( "replyUrl" );
	QTest::addColumn< int >( "statusCode" );

	const QString realm( "dummyRealm" );
	const QString username( "foo" );
	const QString password( "secret" );
	this->m_username = username;
	this->m_password = password;

	Rule::Ptr erAuthenticate( new Rule );
	erAuthenticate->has( Url( exampleRootUrl ) )
	    .hasNot( Predicates::Authorization( username, password ) )
	    .reply()
	    .withAuthenticate( realm );

	Rule::Ptr erAuthedRedirect( new Rule );
	erAuthedRedirect->has( Url( exampleRootUrl ) )
	    .has( Predicates::Authorization( username, password ) )
	    .reply()
	    .withRedirect( fooUrl );

	Rule::Ptr fooAuthenticate( new Rule );
	fooAuthenticate->has( Url( fooUrl ) )
	    .hasNot( Predicates::Authorization( username, password ) )
	    .reply()
	    .withAuthenticate( realm );

	Rule::Ptr fooAuthedResponse( new Rule );
	fooAuthedResponse->has( Url( fooUrl ) )
	    .has( Predicates::Authorization( username, password ) )
	    .reply()
	    .withStatus( HttpStatus::OK );

	QTest::newRow( "redirect after authentication" )
	    << ( RuleVector() << erAuthenticate << erAuthedRedirect << getFooRule() ) << fooUrl
	    << static_cast< int >( HttpStatus::OK );
	QTest::newRow( "authentication after redirect" )
	    << ( RuleVector() << gerToFooRedirectRule() << fooAuthenticate << fooAuthedResponse ) << fooUrl
	    << static_cast< int >( HttpStatus::OK );
}


/*! \test %Tests redirection from a forwarded request.
 */
void ManagerTest::testForwardAndRedirect()
{
	QFETCH( Request, request );
	QFETCH( int, namRedirectPolicy );
	QFETCH( int, ptNamRedirectPolicy );
	QFETCH( RuleVector, mnamRules );
	QFETCH( RuleVector, ptMnamRules );

	Manager< DummyNetworkAccessManager > mnam;
	Manager< DummyNetworkAccessManager > forwardingMnam;

#if QT_VERSION >= QT_VERSION_CHECK( 5,9,0 )
	mnam.setRedirectPolicy( static_cast< QNetworkRequest::RedirectPolicy >( namRedirectPolicy ) );
	forwardingMnam.setRedirectPolicy( static_cast< QNetworkRequest::RedirectPolicy >( ptNamRedirectPolicy ) );
#else  // Qt < 5.9.0
	Q_UNUSED( namRedirectPolicy )
	Q_UNUSED( ptNamRedirectPolicy )
#endif // Qt < 5.9.0
	mnam.setRules( mnamRules );
	mnam.setUnmatchedRequestBehavior( MockNetworkAccess::Forward );
	mnam.setForwardingTargetNam( &forwardingMnam );

	forwardingMnam.setRules( ptMnamRules );
	forwardingMnam.setUnmatchedRequestBehavior( MockNetworkAccess::Forward );

	const auto reply = sendRequest( mnam, request );
	QTRY_VERIFY( reply->isFinished() );

	QTEST( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt(), "expectedStatusCode" );
	QTEST( reply->url(), "expectedReplyUrl" );
}

/*! Provides the data for the testForwardAndRedirect() test.
 */
void ManagerTest::testForwardAndRedirect_data()
{
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< int >( "namRedirectPolicy" );
	QTest::addColumn< int >( "ptNamRedirectPolicy" );
	QTest::addColumn< RuleVector >( "mnamRules" );
	QTest::addColumn< RuleVector >( "ptMnamRules" );
	QTest::addColumn< int >( "expectedStatusCode" );
	QTest::addColumn< QUrl >( "expectedReplyUrl" );

	const QUrl barUrl( "http://example.com/bar" );
	const QUrl fooBarUrl( "http://example.com/foobar" );

	Rule::Ptr fooToBarRedirectRule( new Rule );
	fooToBarRedirectRule->has( Verb( QNetworkAccessManager::GetOperation ) ).has( Url( fooUrl ) ).reply().withRedirect( barUrl );

	Rule::Ptr getBarRule( new Rule );
	getBarRule->has( Verb( QNetworkAccessManager::GetOperation ) ).has( Url( barUrl ) ).reply().withStatus( HttpStatus::OK );

	Rule::Ptr barToFooBarRedirectRule( new Rule );
	barToFooBarRedirectRule->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( barUrl ) )
	    .reply()
	    .withRedirect( fooBarUrl );

	Rule::Ptr getFooBarRule( new Rule );
	getFooBarRule->has( Verb( QNetworkAccessManager::GetOperation ) )
	    .has( Url( fooBarUrl ) )
	    .reply()
	    .withStatus( HttpStatus::OK );

	QTest::newRow( "follow redirect" ) << gerFollowRedirectRequest() << manualRedirectPolicy << manualRedirectPolicy
	                                   << ( RuleVector() << getFooRule() ) << ( RuleVector() << gerToFooRedirectRule() )
	                                   << static_cast< int >( HttpStatus::OK ) << fooUrl;

	QTest::newRow( "redirect back and forth" )
	    << gerFollowRedirectRequest() << manualRedirectPolicy << manualRedirectPolicy
	    << ( RuleVector() << fooToBarRedirectRule ) << ( RuleVector() << gerToFooRedirectRule() << getBarRule )
	    << static_cast< int >( HttpStatus::OK ) << barUrl;

	QTest::newRow( "redirect back and forth and back" )
	    << gerFollowRedirectRequest() << manualRedirectPolicy << manualRedirectPolicy
	    << ( RuleVector() << fooToBarRedirectRule << getFooBarRule )
	    << ( RuleVector() << gerToFooRedirectRule() << barToFooBarRedirectRule ) << static_cast< int >( HttpStatus::OK )
	    << fooBarUrl;

#if QT_VERSION >= QT_VERSION_CHECK( 5,9,0 )

	Request gerPolicyRedirectRequest = getExampleRootRequest;
	gerPolicyRedirectRequest.qRequest.setAttribute( QNetworkRequest::RedirectPolicyAttribute,
	                                                static_cast< int >( QNetworkRequest::NoLessSafeRedirectPolicy ) );

	QTest::newRow( "request policy redirect" )
	    << gerPolicyRedirectRequest << manualRedirectPolicy << manualRedirectPolicy << ( RuleVector() << getFooRule() )
	    << ( RuleVector() << gerToFooRedirectRule() ) << static_cast< int >( HttpStatus::OK ) << fooUrl;

	#if QT_VERSION < QT_VERSION_CHECK( 5,15,2 )
	auto manualRedirectPolicyRequest = getExampleRootRequest;
	manualRedirectPolicyRequest.qRequest.setAttribute( QNetworkRequest::FollowRedirectsAttribute, true );
	manualRedirectPolicyRequest.qRequest.setAttribute( QNetworkRequest::RedirectPolicyAttribute,
	                                                   static_cast< int >( QNetworkRequest::ManualRedirectPolicy ) );

	QTest::newRow( "request policy precedence" )
	    << manualRedirectPolicyRequest << manualRedirectPolicy << manualRedirectPolicy << ( RuleVector() << getFooRule() )
	    << ( RuleVector() << gerToFooRedirectRule() ) << static_cast< int >( HttpStatus::Found ) << exampleRootUrl;
	#endif // QT < 5.15.2

	QTest::newRow( "nam policy redirect" )
	    << getExampleRootRequest << static_cast< int >( QNetworkRequest::NoLessSafeRedirectPolicy )
	    << manualRedirectPolicy << ( RuleVector() << getFooRule() ) << ( RuleVector() << gerToFooRedirectRule() )
	    << static_cast< int >( HttpStatus::OK ) << fooUrl;

	QTest::newRow( "nam policy redirect, ignore forward target nam policy" )
	    << getExampleRootRequest << static_cast< int >( QNetworkRequest::NoLessSafeRedirectPolicy )
	    << static_cast< int >( QNetworkRequest::SameOriginRedirectPolicy ) << ( RuleVector() << getFooRule() )
	    << ( RuleVector() << gerToFooRedirectRule() ) << static_cast< int >( HttpStatus::OK ) << fooUrl;

	QTest::newRow( "no redirect by forward target nam policy" )
	    << getExampleRootRequest << manualRedirectPolicy
	    << static_cast< int >( QNetworkRequest::NoLessSafeRedirectPolicy ) << ( RuleVector() << getFooRule() )
	    << ( RuleVector() << gerToFooRedirectRule() ) << static_cast< int >( HttpStatus::Found ) << exampleRootUrl;
#endif
}

/*! \test %Tests forwarding a request to a real QNetworkAccessManager, receiving a redirect and then mocking
 * the response.
 */
void ManagerTest::testForwardAndRedirectWithRealNam()
{
	Manager< DummyNetworkAccessManager > mnam;
	QNetworkAccessManager realNam;

	mnam.whenGet( exampleRootUrl ).reply().withStatus( HttpStatus::OK ).withBody( statusOkJson );
	mnam.setForwardingTargetNam( &realNam );
	mnam.setUnmatchedRequestBehavior( Forward );

	const auto percentEncodedExampleRootUrl = QString::fromLatin1( QUrl::toPercentEncoding( exampleRootUrl.toString() ) );
	Request request( QNetworkRequest{ QUrl{ "http://httpbin.org/redirect-to?url=" + percentEncodedExampleRootUrl } } );
	enableAutomaticRedirect( request.qRequest );

	auto reply = sendRequest( mnam, request );

	QTRY_VERIFY( reply->isFinished() );
	QCOMPARE( reply->url(), exampleRootUrl );
	QCOMPARE( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ), QVariant{ HttpStatus::OK } );
	QCOMPARE( reply->readAll(), statusOkJson );
	QCOMPARE( mnam.handledRequests().size(), 2 );
}

/*! \test %Tests the Manager::resetRuntimeState() method.
 */
void ManagerTest::testResetRuntimeState()
{
	QMutexLocker locker( &m_credentialMutex );
	m_username = "foo";
	m_password = "bar";

	Manager< DummyNetworkAccessManager > mnam;
	mnam.whenGet( httpsExampleRootUrl )
	    .has( RawHeader( HttpUtils::authorizationHeader(), QByteArray() ) )
	    .reply()
	    .withStatus( HttpStatus::Unauthorized )
	    .withAuthenticate( "world" );
	mnam.whenGet( httpsExampleRootUrl ).has( Authorization( m_username, m_password ) ).reply().withStatus( HttpStatus::OK );
	QObject::connect( &mnam,
	                  SIGNAL( authenticationRequired( QNetworkReply*, QAuthenticator* ) ),
	                  this,
	                  SLOT( authenticate( QNetworkReply*, QAuthenticator* ) ) );

	QSignalSpy authenticationRequiredSpy( &mnam, &QNetworkAccessManager::authenticationRequired );
	QSignalSpy enrcyptedSpy( &mnam, &QNetworkAccessManager::encrypted );

	auto reply = sendRequest( mnam, httpsGetExampleRootRequest );
	QTRY_VERIFY( reply->isFinished() );

	mnam.resetRuntimeState();

	QVERIFY( mnam.receivedRequests().empty() );
	QVERIFY( mnam.handledRequests().empty() );
	QVERIFY( mnam.matchedRequests().empty() );

	reply = sendRequest( mnam, httpsGetExampleRootRequest );
	QTRY_VERIFY( reply->isFinished() );

	QCOMPARE( static_cast< int >( authenticationRequiredSpy.count() ), 2 );
	QCOMPARE( static_cast< int >( enrcyptedSpy.count() ), 2 );
	QCOMPARE( reply->error(), noError );
	QCOMPARE( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt(), static_cast< int >( HttpStatus::OK ) );
}

#if QT_VERSION >= QT_VERSION_CHECK( 5,9,0 )

/*! \test %Tests the request rewriting according to HTTP strict transport security.
 */
void ManagerTest::testHsts()
{
	QFETCH( Request, request );
	QFETCH( RuleVector, rules );
	QFETCH( HstsPolicyVector, hstsPolicies );

	Manager< DummyNetworkAccessManager > mnam;
	mnam.setUnmatchedRequestBehavior( Forward );
	mnam.setRules( rules );
	mnam.setStrictTransportSecurityEnabled( true );
	mnam.addStrictTransportSecurityHosts( hstsPolicies );

	auto reply = sendRequest( mnam, request );
	QTRY_VERIFY( reply->isFinished() );

	QTEST( reply->url(), "replyUrl" );
	QTEST( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt(), "statusCode" );
}

/*! Provides the data for the testHsts() test.
 */
void ManagerTest::testHsts_data()
{
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< RuleVector >( "rules" );
	QTest::addColumn< HstsPolicyVector >( "hstsPolicies" );
	QTest::addColumn< QUrl >( "replyUrl" );
	QTest::addColumn< int >( "statusCode" );


	const QHstsPolicy exampleHstsPolicy( QDateTime::currentDateTime().addDays( 1 ),
	                                     QHstsPolicy::PolicyFlags(),
	                                     "example.com" );
	const QHstsPolicy subdomainHstsPolicy( QDateTime::currentDateTime().addDays( 1 ),
	                                       QHstsPolicy::IncludeSubDomains,
	                                       "sub.example.com" );
	const QHstsPolicy expiredHstsPolicy( QDateTime::currentDateTime().addSecs( -100 ),
	                                     QHstsPolicy::PolicyFlags(),
	                                     "example.com" );

	Rule::Ptr httpsGerRule( new Rule );
	httpsGerRule->has( Url( httpsExampleRootUrl ) ).reply().withStatus( HttpStatus::ImATeapot );

	QTest::newRow( "no hsts policy" ) << getExampleRootRequest << ( RuleVector() << getExampleRootRule() << httpsGerRule )
	                                  << HstsPolicyVector() << exampleRootUrl << static_cast< int >( HttpStatus::OK );

	Rule::Ptr httpsFooRule( new Rule );
	httpsFooRule->has( Url( httpsFooUrl ) ).reply().withStatus( HttpStatus::ImATeapot );

	QTest::newRow( "example hsts policy" )
	    << getFooRequest << ( RuleVector() << getFooRule() << httpsFooRule )
	    << ( HstsPolicyVector() << exampleHstsPolicy ) << httpsFooUrl << static_cast< int >( HttpStatus::ImATeapot );

	QUrl explicitPortFooUrl( fooUrl );
	explicitPortFooUrl.setPort( HttpUtils::HttpDefaultPort );
	Request explicitPortFooRequest = getFooRequest;
	explicitPortFooRequest.qRequest.setUrl( explicitPortFooUrl );
	QUrl explicitPortHttpsFooUrl( httpsFooUrl );
	explicitPortHttpsFooUrl.setPort( HttpUtils::HttpsDefaultPort );
	QTest::newRow( "explicit port" ) << explicitPortFooRequest << ( RuleVector() << getFooRule() << httpsFooRule )
	                                 << ( HstsPolicyVector() << exampleHstsPolicy ) << explicitPortHttpsFooUrl
	                                 << static_cast< int >( HttpStatus::ImATeapot );

	const QHstsPolicy ipHstsPolicy( QDateTime::currentDateTime().addDays( 1 ), QHstsPolicy::PolicyFlags(), "127.0.0.1" );
	const QUrl ipUrl( "http://127.0.0.1" );
	const Request ipRequest( QNetworkAccessManager::GetOperation, QNetworkRequest( ipUrl ) );
	Rule::Ptr httpIpRule( new Rule );
	httpIpRule->has( Url( ipUrl ) ).reply().withStatus( HttpStatus::OK );
	Rule::Ptr httpsIpRule( new Rule );
	httpsIpRule->has( Url( "https://127.0.0.1" ) ).reply().withStatus( HttpStatus::ImATeapot );
	QTest::newRow( "ip request" ) << ipRequest << ( RuleVector() << httpIpRule << httpsIpRule )
	                              << ( HstsPolicyVector() << ipHstsPolicy ) << ipUrl
	                              << static_cast< int >( HttpStatus::OK );

	const QHstsPolicy ipV6HstsPolicy( QDateTime::currentDateTime().addDays( 1 ),
	                                  QHstsPolicy::PolicyFlags(),
	                                  "fe01::1:2:3" );
	const QUrl ipV6Url( "http://[fe01::1:2:3]" );
	const Request ipV6Request( QNetworkAccessManager::GetOperation, QNetworkRequest( ipV6Url ) );
	Rule::Ptr httpIpV6Rule( new Rule );
	httpIpV6Rule->has( Url( ipV6Url ) ).reply().withStatus( HttpStatus::OK );
	Rule::Ptr httpsIpV6Rule( new Rule );
	httpsIpV6Rule->has( Url( "https://[fe01::1:2:3]" ) ).reply().withStatus( HttpStatus::ImATeapot );
	QTest::newRow( "ip v6 request" ) << ipV6Request << ( RuleVector() << httpIpV6Rule << httpsIpV6Rule )
	                                 << ( HstsPolicyVector() << ipV6HstsPolicy ) << ipV6Url
	                                 << static_cast< int >( HttpStatus::OK );

	const QUrl subdomainUrl( "http://sub.sub.example.com" );
	const QUrl httpsSubdomainUrl( "https://sub.sub.example.com" );
	const QNetworkRequest subdomainRequestObj( subdomainUrl );
	const Request subdomainRequest( subdomainRequestObj );
	Rule::Ptr subdomainRule( new Rule );
	subdomainRule->has( Url( httpsSubdomainUrl ) ).reply().withStatus( HttpStatus::OK );

	QTest::newRow( "subdomain hsts policy" )
	    << subdomainRequest << ( RuleVector() << subdomainRule ) << ( HstsPolicyVector() << subdomainHstsPolicy )
	    << httpsSubdomainUrl << static_cast< int >( HttpStatus::OK );

	const QUrl otherSubdomainUrl( "http://other.example.com" );
	const QNetworkRequest otherSubdomainRequestObj( otherSubdomainUrl );
	const Request otherSubdomainRequest( otherSubdomainRequestObj );

	QTest::newRow( "no matching hsts policy" )
	    << otherSubdomainRequest << ( RuleVector() << subdomainRule )
	    << ( HstsPolicyVector() << subdomainHstsPolicy << exampleHstsPolicy ) << otherSubdomainUrl << 0;

	QTest::newRow( "expired hsts policy" )
	    << getExampleRootRequest << ( RuleVector() << getExampleRootRule() << httpsGerRule )
	    << ( HstsPolicyVector() << expiredHstsPolicy ) << exampleRootUrl << static_cast< int >( HttpStatus::OK );
}

/*! \test %Tests the expiration of HSTS policies.
 */
void ManagerTest::testHstsPolicyExpiry()
{
	QFETCH( Request, request );

	Manager< DummyNetworkAccessManager > mnam;
	mnam.addRule( getExampleRootRule() );
	mnam.whenGet( httpsExampleRootUrl ).reply().withStatus( HttpStatus::ImATeapot );
	mnam.whenGet( QRegularExpression( "http://(.+\\.)?sub\\.example\\.com" ) ).reply().withStatus( HttpStatus::OK );
	mnam.whenGet( QRegularExpression( "https://(.+\\.)?sub\\.example\\.com" ) ).reply().withStatus( HttpStatus::ImATeapot );
	mnam.setStrictTransportSecurityEnabled( true );

	const int policyExpiry = 200;
	const int policyExpiryBuffer = 200;

	HstsPolicyVector hstsPolicies;
	hstsPolicies << QHstsPolicy( QDateTime::currentDateTime().addMSecs( policyExpiry ),
	                             QHstsPolicy::PolicyFlags(),
	                             "example.com" );
	hstsPolicies << QHstsPolicy( QDateTime::currentDateTime().addMSecs( policyExpiry ),
	                             QHstsPolicy::IncludeSubDomains,
	                             "sub.example.com" );

	mnam.addStrictTransportSecurityHosts( hstsPolicies );

	auto reply = sendRequest( mnam, request ); // Send the request to fill the HSTS policy hash

	QTest::qWait( policyExpiry + policyExpiryBuffer ); // Ensure policies are expired

	reply = sendRequest( mnam, request );
	QTRY_VERIFY( reply->isFinished() );

	QCOMPARE( reply->error(), QNetworkReply::NoError );
	QCOMPARE( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt(), static_cast< int >( HttpStatus::OK ) );
}

/*! Provides the data for the testHstsPolicyExpiry() test.
 */
void ManagerTest::testHstsPolicyExpiry_data()
{
	QTest::addColumn< Request >( "request" );

	QTest::newRow( "full host name match" ) << getExampleRootRequest;

	const QUrl subdomainUrl( "http://sub.sub.example.com" );
	const QNetworkRequest subdomainRequestObj( subdomainUrl );
	const Request subdomainRequest( subdomainRequestObj );
	QTest::newRow( "sub-domain match" ) << subdomainRequest;
}

/*! \test %Tests the handling of HSTS response headers.
 */
void ManagerTest::testHstsResponse()
{
	QFETCH( QString, host );
	QFETCH( QByteArray, stsHeader );
	QFETCH( int, maxAge );
	QFETCH( QHstsPolicy, expectedPolicy );

	const auto url = QUrl( "https://" + host + "/foo" );
	Manager< DummyNetworkAccessManager > mnam;
	mnam.setStrictTransportSecurityEnabled( true );
	mnam.whenGet( url ).reply().withRawHeader( "Strict-Transport-Security", stsHeader );

	ReplyPtr reply( mnam.get( QNetworkRequest( url ) ) );
	expectedPolicy.setExpiry( QDateTime::currentDateTimeUtc().addSecs( maxAge ) );

	QTRY_VERIFY( reply->isFinished() );

	const auto stsHosts = mnam.strictTransportSecurityHosts();
	for ( auto&& stsHost : stsHosts )
	{
		if ( stsHost.host() == expectedPolicy.host()
		     && stsHost.includesSubDomains() == expectedPolicy.includesSubDomains()
		     && stsHost.expiry().secsTo( expectedPolicy.expiry() ) < 2 )
			return;
	}
	QFAIL( "HSTS policy missing in network access manager" ); // LCOV_EXCL_LINE
}

/*! Provides the data for the testHstsResponse() test.
 */
void ManagerTest::testHstsResponse_data()
{
	QTest::addColumn< QString >( "host" );
	QTest::addColumn< QByteArray >( "stsHeader" );
	QTest::addColumn< int >( "maxAge" );
	QTest::addColumn< QHstsPolicy >( "expectedPolicy" );

	const auto host = QUuid::createUuid().toString().mid( 1, 36 ) + ".com";

	QHstsPolicy policy;
	policy.setHost( host );
	QTest::newRow( "simple policy" ) << host << QByteArray( "max-age=17" ) << 17 << policy;

	QTest::newRow( "multiple headers" ) << host << QByteArray( "max-age=17,max-age=42" ) << 17 << policy;
	QTest::newRow( "invalid headers" ) << host << QByteArray( "foo=bar,max-age=17,includeSubdomains" ) << 17 << policy;

	policy.setIncludesSubDomains( true );
	QTest::newRow( "include subdomain" ) << host << QByteArray( "max-age=17; includeSubdomains" ) << 17 << policy;
}

/*! \test %Tests the handling of invalid HSTS response headers.
 */
void ManagerTest::testInvalidHstsHeaders()
{
	QFETCH( QByteArray, stsHeader );

	Manager< DummyNetworkAccessManager > mnam;
	mnam.setStrictTransportSecurityEnabled( true );
	mnam.whenGet( httpsFooUrl ).reply().withRawHeader( "Strict-Transport-Security", stsHeader );

	std::unique_ptr< QNetworkReply > reply( mnam.get( QNetworkRequest( httpsFooUrl ) ) );

	QSignalSpy finishedSpy( reply.get(), SIGNAL( finished() ) );

	QTRY_VERIFY2( ! finishedSpy.isEmpty(), "Reply did not finish" );
	QVector< QHstsPolicy > stsHosts = mnam.strictTransportSecurityHosts();
	QVERIFY( stsHosts.isEmpty() );
}

/*! Provides the data for the testInvalidHstsHeaders() test.
 */
void ManagerTest::testInvalidHstsHeaders_data()
{
	QTest::addColumn< QByteArray >( "stsHeader" );

	// clang-format off
	QTest::newRow( "empty header" )            << QByteArray( "" );
	QTest::newRow( "duplicate directive" )     << QByteArray( "max-age=10; max-age=20" );
	QTest::newRow( "invalid max-age" )         << QByteArray( "max-age=foo" );
	QTest::newRow( "invalid directive name" )  << QByteArray( "foo bar=test" );
	QTest::newRow( "invalid directive value" ) << QByteArray( "foo=invalid value" );
	QTest::newRow( "missing max-age" )         << QByteArray( "includeSubdomains" );
	// clang-format on
}

#endif // Qt >= 5.9.0

#if QT_VERSION >= QT_VERSION_CHECK( 5,14,0 )

void ManagerTest::testAutoDeleteReplies()
{
	Manager< DummyNetworkAccessManager > mnam;
	mnam.addRule( getExampleRootRule() );

	Request autoDeleteRequest = getExampleRootRequest;
	autoDeleteRequest.qRequest.setAttribute( QNetworkRequest::AutoDeleteReplyOnFinishAttribute, true );

	QPointer< QNetworkReply > reply( sendRequest( mnam, autoDeleteRequest ).release() );

	QVERIFY( reply );
	QTRY_VERIFY( ! reply );

	mnam.setAutoDeleteReplies( true );
	reply = sendRequest( mnam, getExampleRootRequest ).release();

	QVERIFY( reply );
	QTRY_VERIFY( ! reply );
}

#endif // Qt >= 5.14.0

} // namespace Tests


QTEST_MAIN( Tests::ManagerTest )
#include "ManagerTest.moc"
