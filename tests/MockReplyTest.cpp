#include "TestUtils.hpp"

#include "MockNetworkAccessManager.hpp"

#include "QSignalInspector.hpp"

#include <QtTest>
#include <QtDebug>
#include <QSet>
#include <QSharedPointer>
#if QT_VERSION >= QT_VERSION_CHECK( 5,10,0 )
	#include <QRandomGenerator>
#endif // Qt >= 5.10.0

#include <cstdlib>

namespace Tests {

/*! Implements unit tests for the MockReply and MockReplyBuilder classes.
 */
class MockReplyTest : public QObject
{
	Q_OBJECT

private slots:
	void initTestCase();

	void testRequestVerb();
	void testRequestVerb_data();
	void testCopy();
	void testCopy_data();
	void testIsRedirectToBeFollowed();
	void testIsRedirectToBeFollowed_data();
	void testAttributes();
	void testAttributes_data();
	void testSimulateRemoveFileLocalhost();
	void testSimulate();
	void testSimulate_data();
	void testDefaultErrorString();
	void testDefaultErrorString_data();
	void testAbort();

	void testNullBuilder();
	void testWithBodyByteArray();
	void testWithBodyByteArray_data();
	void testWithBodyJson_data();
	void testWithBodyJson();
	void testWithFile();
	void testWithFile_data();
	void testWithStatus();
	void testWithStatus_data();
	void testWithStatusMultipleTimes();
	void testWithStatusMultipleTimes_data();
	void testWithError_data();
	void testWithError();
	void testWithRedirect();
	void testWithRedirect_data();
	void testWithHeader();
	void testWithHeader_data();
	void testWithRawHeader_data();
	void testWithRawHeader();
	void testWithAttribute_data();
	void testWithAttribute();
	void testWithAuthenticate();
	void testWithAuthenticate_data();
	void testWithCookie_data();
	void testWithCookie();
	void testWithFinishDelayUntil();
	void testWithFinishDelayUntilImmediateEmit();
	void testWithFinishDelayUntilEmittedAtDifferentTimes();
	void testWithFinishDelayUntilEmittedAtDifferentTimes_data();
	void testWithFinishDelayUntilMemberPointer();
	void testEqualityOperators();
	void testEqualityOperators_data();

	void testErrorAndStatusCodeConsistencyWarning();
	void testErrorAndStatusCodeConsistencyWarning_data();

};


using namespace MockNetworkAccess;


//####### Helpers #######


class MockReplyTestHelper : public MockReply
{
	Q_OBJECT

public:
	/* Hack to access protected methods of base class: https://stackoverflow.com/a/23904680/490560
	 * For this to work, MockReplyTestHelper may not implement methods with the same name
	 * as the protected methods of MockReply we want to call.
	 * Therefore, we use a "call" prefix.
	 */

	static void callSimulate( MockReply* reply, const Request& req, detail::Connection* connection )
	{
		( reply->*&MockReplyTestHelper::simulate )( req, connection );
	}

	static void callSetBehaviorFlags( MockReply* reply, BehaviorFlags flags )
	{
		( reply->*&MockReplyTestHelper::setBehaviorFlags )( flags );
	}

};

class TestMockReply : public MockReply
{
	Q_OBJECT

	friend class MockReplyTest;

protected:
	void setRedirected( const QUrl& url, HttpStatus::Code statusCode = HttpStatus::Found )
	{
		setRawHeader( HttpUtils::locationHeader(), url.toEncoded() );
		setAttribute( QNetworkRequest::HttpStatusCodeAttribute, static_cast< int >( statusCode ) );
	}
};

class DummyObject : public QObject
{
	Q_OBJECT

public:
	DummyObject( QObject* parent = Q_NULLPTR )
		: QObject( parent )
	{}

Q_SIGNALS:
	void dummySignal();
};

typedef QSharedPointer<MockReplyBuilder> MockReplyBuilderPtr;
typedef QSharedPointer<TestMockReply> TestMockReplyPtr;
typedef std::unique_ptr<MockReply> MockReplyUniquePtr;
typedef QPair<bool, QRegularExpression> OptionalRegEx;


const auto unknownErrorString = QStringLiteral( "Unknown error" );

const QString contentTypeTextPlain( "text/plain" );
const QString contentTypeOctetstream( "application/octet-stream" );
const QString contentTypeJson( "application/json" );
const QString contentTypeXml( "application/xml" );
const QByteArray customHeader( "X-Custom-Header" );

const QUrl fileUrl( "file:///path/to/file.txt" );
const Request fileGetRequest( QNetworkRequest( fileUrl ), QNetworkAccessManager::GetOperation );
const Request fileHeadRequest( QNetworkRequest( fileUrl ), QNetworkAccessManager::HeadOperation );
const Request filePutRequest( QNetworkRequest( fileUrl ), QNetworkAccessManager::PutOperation, QByteArray( "{\"foo\":\"bar\"}" ) );

const QUrl qrcUrl( "qrc:/path/to/file.txt" );
const Request qrcGetRequest( QNetworkRequest( qrcUrl ), QNetworkAccessManager::GetOperation );
const Request qrcHeadRequest( QNetworkRequest( qrcUrl ), QNetworkAccessManager::HeadOperation );
const Request qrcPutRequest( QNetworkRequest( qrcUrl ), QNetworkAccessManager::PutOperation, QByteArray( "{\"foo\":\"bar\"}" ) );

// Possible signals
QMetaMethod encryptedSignal;
QMetaMethod metaDataChangedSignal;
QMetaMethod uploadProgressSignal;
QMetaMethod downloadProgressSignal;
QMetaMethod finishedSignal;
#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
	QMetaMethod errorSignal;
#endif // Qt < 6.0.0
#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
	QMetaMethod errorOccurredSignal;
#endif // Qt >= 5.15.0
QMetaMethod readyReadSignal;
QMetaMethod readChannelFinishedSignal;

inline void initReplySignals()
{
	const auto& replyMetaObj = QNetworkReply::staticMetaObject;

	encryptedSignal =           getSignal( replyMetaObj, "encrypted()" );
	metaDataChangedSignal =     getSignal( replyMetaObj, "metaDataChanged()" );
	uploadProgressSignal =      getSignal( replyMetaObj, "uploadProgress(qint64, qint64)" );
	downloadProgressSignal =    getSignal( replyMetaObj, "downloadProgress(qint64, qint64)" );
	finishedSignal =            getSignal( replyMetaObj, "finished()" );
	#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		errorSignal =           getSignal( replyMetaObj, "error(QNetworkReply::NetworkError)" );
	#endif // Qt < 6.0.0
	#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		errorOccurredSignal =   getSignal( replyMetaObj, "errorOccurred(QNetworkReply::NetworkError)" );
	#endif // Qt >= 5.15.0
	readyReadSignal =           getSignal( replyMetaObj, "readyRead()" );
	readChannelFinishedSignal = getSignal( replyMetaObj, "readChannelFinished()" );
}

namespace HelperMethods
{
	MockReplyBuilderPtr createFullBuilder();
	MockReplyBuilderPtr createErrorBuilder();
	MockReplyBuilder createErrorBuilder( QNetworkReply::NetworkError error );
	HeaderHash getKnownHeaders( QNetworkReply* reply );
};

OptionalRegEx makeOptionalRegEx( const QString& pattern,
                                 QRegularExpression::PatternOptions options = QRegularExpression::NoPatternOption );

detail::Connection::Ptr createConnection( const Request& request )
{
	return detail::Connection::Ptr{ new detail::Connection( request.qRequest.url().scheme(), request.qRequest.url().toString() ) };
}

} // namespace Tests


Q_DECLARE_METATYPE( MockNetworkAccess::BehaviorFlags )
Q_DECLARE_METATYPE( MockNetworkAccess::HeaderHash )
Q_DECLARE_METATYPE( Tests::MockReplyBuilderPtr )
Q_DECLARE_METATYPE( Tests::TestMockReplyPtr )
Q_DECLARE_METATYPE( QNetworkRequest::KnownHeaders )
Q_DECLARE_METATYPE( QNetworkRequest::Attribute )
Q_DECLARE_METATYPE( MockNetworkAccess::HttpUtils::Authentication::Challenge::Ptr )
Q_DECLARE_METATYPE( Tests::OptionalRegEx )
Q_DECLARE_METATYPE( QMetaMethod ) // LCOV_EXCL_LINE


namespace Tests
{

void MockReplyTest::initTestCase()
{
	initReplySignals();
}

//####### Tests #######


/*! \test %Tests the Request::verb() method.
 */
void MockReplyTest::testRequestVerb()
{
	QFETCH( Request, request );

	QTEST( request.verb(), "expectedVerb" );
}

/*! Provides the data for the testRequestVerb() test.
 */
void MockReplyTest::testRequestVerb_data()
{
	QTest::addColumn<Request>( "request" );
	QTest::addColumn<QString>( "expectedVerb" );

	const QUrl dummyUrl( "http://example.com" );
	const QNetworkRequest dummyQRequest( dummyUrl );
	QNetworkRequest customVerbRequest( dummyUrl );
	customVerbRequest.setAttribute( QNetworkRequest::CustomVerbAttribute, "foo" );

	//                         // request                                                               // expectedVerb
	QTest::newRow( "GET" )     << Request( QNetworkAccessManager::GetOperation, dummyQRequest )         << "GET";
	QTest::newRow( "POST" )    << Request( QNetworkAccessManager::PostOperation, dummyQRequest )        << "POST";
	QTest::newRow( "PUT" )     << Request( QNetworkAccessManager::PutOperation, dummyQRequest )         << "PUT";
	QTest::newRow( "DELETE" )  << Request( QNetworkAccessManager::DeleteOperation, dummyQRequest )      << "DELETE";
	QTest::newRow( "HEAD" )    << Request( QNetworkAccessManager::HeadOperation, dummyQRequest )        << "HEAD";
	QTest::newRow( "Custom" )  << Request( QNetworkAccessManager::CustomOperation, customVerbRequest  ) << "foo";
}


/*! \test %Tests the methods to copy MockReplyBuilder objects.
 * Implicitly also tests the MockReply::clone() method.
 */
void MockReplyTest::testCopy()
{
	QFETCH(MockReplyBuilderPtr, original);
	QFETCH(MockReplyBuilderPtr, copy);

	MockReplyUniquePtr originalReply(original->createReply());
	MockReplyUniquePtr copyReply(copy->createReply());

	if (!originalReply)
		QVERIFY(!copyReply);
	else
	{
		QCOMPARE(copyReply->isFinished(), originalReply->isFinished());
		QCOMPARE(copyReply->isOpen(), originalReply->isOpen());
		QCOMPARE(copyReply->body(), originalReply->body());
		QCOMPARE(copyReply->bytesAvailable(), originalReply->bytesAvailable());
		QCOMPARE(copyReply->size(), originalReply->size());
		QCOMPARE(copyReply->attributes(), originalReply->attributes());
		QCOMPARE(copyReply->rawHeaderPairs(), originalReply->rawHeaderPairs());
		QCOMPARE(HelperMethods::getKnownHeaders(copyReply.get()), HelperMethods::getKnownHeaders(originalReply.get()));
		QCOMPARE(copyReply->error(), originalReply->error());
	}
}

/*! Provides the data for the testCopy() test.
 */
void MockReplyTest::testCopy_data()
{
	QTest::addColumn< MockReplyBuilderPtr >( "original" );
	QTest::addColumn< MockReplyBuilderPtr >( "copy" );

	MockReplyBuilderPtr original;
	MockReplyBuilderPtr copy;

	// Unconfigured builder
	original.reset( new MockReplyBuilder() );
	copy.reset( new MockReplyBuilder( *original ) );
	QTest::newRow( "unconfigured; copy constructor" ) << original << copy;

	copy.reset( new MockReplyBuilder( MockReplyBuilder( *original ) ) );
	QTest::newRow( "unconfigured; move constructor" ) << original << copy;

	copy.reset( new MockReplyBuilder() );
	*copy = *original;
	QTest::newRow( "unconfigured; copy operator" ) << original << copy;

	copy.reset( new MockReplyBuilder() );
	*copy = MockReplyBuilder( *original );
	QTest::newRow( "unconfigured; move operator" ) << original << copy;

	copy.reset( new MockReplyBuilder() );
	copy->with( *original );
	QTest::newRow( "unconfigured; with method" ) << original << copy;

	copy.reset( new MockReplyBuilder() );
	copy->with( MockReplyBuilder( *original ) );
	QTest::newRow( "unconfigured; with move method" ) << original << copy;

	// Successful replies
	original.reset( new MockReplyBuilder() );
	original->withStatus( HttpStatus::OK );
	original->withBody( "foo bar" );
	original->withHeader( QNetworkRequest::ContentTypeHeader, QString( "text/plain" ) );
	original->withAttribute( QNetworkRequest::HttpStatusCodeAttribute, 200 );
	copy.reset( new MockReplyBuilder( *original ) );
	QTest::newRow( "successful replies; copy constructor" ) << original << copy;

	copy.reset( new MockReplyBuilder( MockReplyBuilder( *original ) ) );
	QTest::newRow( "successful replies; move constructor" ) << original << copy;

	copy.reset( new MockReplyBuilder() );
	*copy = *original;
	QTest::newRow( "successful replies; copy operator" ) << original << copy;

	copy.reset( new MockReplyBuilder() );
	*copy = MockReplyBuilder( *original );
	QTest::newRow( "successful replies; move operator" ) << original << copy;

	copy.reset( new MockReplyBuilder() );
	copy->with( *original );
	QTest::newRow( "successful replies; with method" ) << original << copy;

	copy.reset( new MockReplyBuilder() );
	copy->with( MockReplyBuilder( *original ) );
	QTest::newRow( "successful replies; with move method" ) << original << copy;
}


/*! \test %Tests MockReply::isRedirectToBeFollowed().
 */
void MockReplyTest::testIsRedirectToBeFollowed()
{
	QFETCH( TestMockReplyPtr, reply );
	QFETCH( BehaviorFlags, behaviorFlags );

	reply->setBehaviorFlags( behaviorFlags );

	QTEST( reply->isRedirectToBeFollowed(), "isRedirectToBeFollowed" );

}

/*! Provides the data for the MockReplyTest::testIsRedirectToBeFollowed() test.
 */
void MockReplyTest::testIsRedirectToBeFollowed_data()
{
	QTest::addColumn< TestMockReplyPtr >( "reply" );
	QTest::addColumn< BehaviorFlags >( "behaviorFlags" );
	QTest::addColumn< bool >( "isRedirectToBeFollowed" );

	const QUrl exampleRootUrl("http://example.com");

	TestMockReplyPtr noStatusCodeReply( new TestMockReply );
	noStatusCodeReply->setBody( "foo" );
	TestMockReplyPtr relativeRedirectReply( new TestMockReply );
	relativeRedirectReply->setRedirected( QUrl( "foo/bar" ) );
	TestMockReplyPtr protRelRedirectReply( new TestMockReply );
	protRelRedirectReply->setRedirected( QUrl( "//example.com/foo" ) );
	TestMockReplyPtr redirect302Reply( new TestMockReply );
	redirect302Reply->setRedirected( exampleRootUrl, HttpStatus::Found );
	TestMockReplyPtr redirect307Reply( new TestMockReply );
	redirect307Reply->setRedirected( exampleRootUrl, HttpStatus::TemporaryRedirect );
	TestMockReplyPtr redirect308Reply( new TestMockReply );
	redirect308Reply->setRedirected( exampleRootUrl, HttpStatus::PermanentRedirect );

	const auto qt_5_6_Behavior = static_cast< BehaviorFlags >( Behavior_Qt_5_6_0 );
	const auto expectedBehavior = static_cast< BehaviorFlags >( Behavior_Expected );


	//                                                       // reply                 // behaviorFlags    // isRedirectToBeFollowed
	QTest::newRow( "no status code" )                        << noStatusCodeReply     << qt_5_6_Behavior  << false;
	QTest::newRow( "relative redirect (Qt 5.6)" )            << relativeRedirectReply << qt_5_6_Behavior  << true;
	QTest::newRow( "relative redirect (expected)" )          << relativeRedirectReply << expectedBehavior << true;
	QTest::newRow( "protocol relative redirect (Qt 5.6)" )   << protRelRedirectReply  << qt_5_6_Behavior  << true;
	QTest::newRow( "protocol relative redirect (expected)" ) << protRelRedirectReply  << expectedBehavior << true;
	QTest::newRow( "redirect 302" )                          << redirect302Reply      << qt_5_6_Behavior  << true;
	QTest::newRow( "redirect 307" )                          << redirect307Reply      << qt_5_6_Behavior  << true;
	QTest::newRow( "redirect 308 (Qt 5.6)" )                 << redirect308Reply      << qt_5_6_Behavior  << false;
	QTest::newRow( "redirect 308 (expected)" )               << redirect308Reply      << expectedBehavior << true;
}

/*! \test %Tests MockReply::attributes() and MockReply::setAttribute().
 */
void MockReplyTest::testAttributes()
{
	QFETCH( AttributeHash, setAttributes );
	QFETCH( AttributeHash, expectedAttributes );

	TestMockReply reply;

	for ( auto iter = setAttributes.cbegin(); iter != setAttributes.cend(); ++iter )
		reply.setAttribute( iter.key(), iter.value() );

	const auto expectedAttributeKeys = keySet( expectedAttributes );

	QCOMPARE( reply.attributes(), expectedAttributes );
	QCOMPARE( reply.attributeKeys(), expectedAttributeKeys );
}

namespace /* anonymous */
{

template< typename KeyType, typename ValueType >
QHash< KeyType, ValueType > combineHashes( const QHash< KeyType, ValueType >& a, const QHash< KeyType, ValueType >& b )
{
	auto combination = a;
	for( auto iter = b.begin(), end = b.end(); iter != end; ++iter )
		combination.insert( iter.key(), iter.value() );
	return combination;
}

} // anonymous namespace

/*! Provides the data for the MockReplyTest::testAttributes() test.
 */
void MockReplyTest::testAttributes_data()
{
	QTest::addColumn< AttributeHash >( "setAttributes" );
	QTest::addColumn< AttributeHash >( "expectedAttributes" );

	AttributeHash defaultAttributes;
	defaultAttributes.insert( QNetworkRequest::ConnectionEncryptedAttribute, false );

	const auto combinedWithDefaultAttributes = [ &defaultAttributes ]( const AttributeHash& attributes ) {
		return combineHashes( attributes, defaultAttributes );
	};

	AttributeHash someAttributes;
	someAttributes.insert( QNetworkRequest::HttpStatusCodeAttribute, 200 );
	someAttributes.insert( QNetworkRequest::HttpReasonPhraseAttribute, QString( "foo" ) );

	AttributeHash nullValueAttribute;
	nullValueAttribute.insert( QNetworkRequest::HttpReasonPhraseAttribute, QString() );

	AttributeHash invalidValueAttribute;
	invalidValueAttribute.insert( QNetworkRequest::HttpStatusCodeAttribute, QVariant() );

	const auto maxRandomAttributeOffset = 1000;
	const auto randomAttributeOffset
	#if QT_VERSION < QT_VERSION_CHECK( 5,10,0 )
		= static_cast< int >( qrand() / RAND_MAX * maxRandomAttributeOffset ) ;
	#else
		= QRandomGenerator::global()->bounded( maxRandomAttributeOffset + 1 );
	#endif

	const auto userAttributeId = static_cast< QNetworkRequest::Attribute >( static_cast< int >( QNetworkRequest::User ) + randomAttributeOffset );
	AttributeHash userDefinedAttribute;
	userDefinedAttribute.insert( userAttributeId, QString( "foo bar" ) );

	//                                         // setAttributes         // expectedAttributes
	QTest::newRow( "no attributes" )           << AttributeHash()       << defaultAttributes;
	QTest::newRow( "some attributes" )         << someAttributes        << combinedWithDefaultAttributes( someAttributes );
	QTest::newRow( "null value attribute" )    << nullValueAttribute    << combinedWithDefaultAttributes( nullValueAttribute );
	QTest::newRow( "invalid value attribute" ) << invalidValueAttribute << combinedWithDefaultAttributes( defaultAttributes );
	QTest::newRow( "user defined attribute" )  << userDefinedAttribute  << combinedWithDefaultAttributes( userDefinedAttribute );
}

/*! \test %Tests that the host "localhost" is removed from reply URLs for file:// request.
 */
void MockReplyTest::testSimulateRemoveFileLocalhost()
{
	Request request( QNetworkRequest( QUrl( "file://localhost/path/to/file" ) ), QNetworkAccessManager::GetOperation );

	auto connection = createConnection( request );
	MockReplyUniquePtr reply( MockReplyBuilder().withError( QNetworkReply::NoError ).createReply() );

	MockReplyTestHelper::callSimulate( reply.get(), request, connection.get() );
	QTRY_VERIFY( reply->isFinished() );

	QVERIFY( reply->url().host().isEmpty() );
}


/*! \test %Tests the MockReply::simulate() method.
 */
void MockReplyTest::testSimulate()
{
	QFETCH( BehaviorFlags, behaviorFlags );
	QFETCH( MockReplyBuilder, builder );
	QFETCH( Request, request );
	QFETCH( AttributeHash, expectedAttributes );
	QFETCH( RawHeaderHash, expectedRawHeaders );
	QFETCH( HeaderHash, expectedHeaders );
	QFETCH( OptionalRegEx, expectedWarning );
	QFETCH( QByteArray, expectedReplyPayload );

	auto connection = createConnection( request );
	MockReplyUniquePtr reply( builder.createReply() );

	MockReplyTestHelper::callSetBehaviorFlags( reply.get(), behaviorFlags );

	QSignalInspector inspector( reply.get() );

	if( expectedWarning.first )
		QTest::ignoreMessage( QtWarningMsg, expectedWarning.second );

	MockReplyTestHelper::callSimulate( reply.get(), request, connection.get() );

	QTest::qWait( 1 ); // ensure that we enter the event loop also for file-like requests which finished immediately
	QTRY_VERIFY( reply->isFinished() ); // ensure the request is actually finished
	QVERIFY( reply->isReadable() );
	QVERIFY( reply->isOpen() );
	QCOMPARE( reply->bytesAvailable(), static_cast< qint64 >( expectedReplyPayload.size() ) );
	QCOMPARE( reply->readAll(), expectedReplyPayload );
	QCOMPARE( reply->QNetworkReply::request(), request.qRequest );
	QTEST( reply->error(), "expectedError" );
	QTEST( reply->errorString(), "expectedErrorString" );
	QCOMPARE( reply->url(), request.qRequest.url() );
	QCOMPARE( reply->attributes(), expectedAttributes );

	for ( RawHeaderHash::const_iterator iter = expectedRawHeaders.cbegin(); iter != expectedRawHeaders.cend(); ++iter )
		QCOMPARE( reply->rawHeader( iter.key() ), iter.value() );

	for ( HeaderHash::const_iterator iter = expectedHeaders.cbegin(); iter != expectedHeaders.cend(); ++iter )
		QCOMPARE( reply->header( iter.key() ), iter.value() );

	const auto emissions = getSignalEmissionListFromInspector( inspector );

	QTEST( emissions, "expectedSignals" );
}

/*! Provides the data for the testSimulate() test.
 */
void MockReplyTest::testSimulate_data()
{
	QTest::addColumn< MockReplyBuilder >( "builder" );
	QTest::addColumn< BehaviorFlags >( "behaviorFlags" );
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< AttributeHash >( "expectedAttributes" );
	QTest::addColumn< RawHeaderHash >( "expectedRawHeaders" );
	QTest::addColumn< HeaderHash >( "expectedHeaders" );
	QTest::addColumn< QNetworkReply::NetworkError >( "expectedError" );
	QTest::addColumn< QString >( "expectedErrorString" );
	QTest::addColumn< OptionalRegEx >( "expectedWarning" );
	QTest::addColumn< QByteArray >( "expectedReplyPayload" );
	QTest::addColumn< SignalEmissionList >( "expectedSignals" );

	const Request getReq( QNetworkRequest( QUrl( "http://example.com" ) ), QNetworkAccessManager::GetOperation );
	const Request getHttpsReq( QNetworkRequest( QUrl( "https://example.com" ) ), QNetworkAccessManager::GetOperation );

	const OptionalRegEx noExpectedWarning = qMakePair( false, QRegularExpression() );

	const QString httpErrorStringTemplate = "Error transferring %1 - server replied: %2";

	const auto noError = QNetworkReply::NoError;
	const auto notFoundError = QNetworkReply::ContentNotFoundError;
	const auto accessDeniedError = QNetworkReply::ContentAccessDenied;
	const auto temporaryNetworkError = QNetworkReply::TemporaryNetworkFailureError;
	const auto internalServerError = QNetworkReply::InternalServerError;
	const auto internalServErrorCode = HttpStatus::InternalServerError;
	const auto successStatusCode = HttpStatus::OK;
	const auto forbiddenStatusCode = HttpStatus::Forbidden;
	const auto notFoundCode = HttpStatus::NotFound;
	const QByteArray customReasonPhrase( "foo" );
	const QByteArray contentLengthHeader( "Content-Length" );
	const QByteArray successPayload( "{\"status\":\"ok\"}" );
	const QByteArray dummyPayload( "dummy" );

	const BehaviorFlags expectedBehavior = Behavior_Expected;

	MockReplyBuilder successBuilder;
	successBuilder.withStatus( successStatusCode ).withBody( successPayload );
	AttributeHash defaultAttributes;
	defaultAttributes.insert( QNetworkRequest::HttpStatusCodeAttribute, successStatusCode );
	defaultAttributes.insert( QNetworkRequest::HttpReasonPhraseAttribute, HttpStatus::reasonPhrase( successStatusCode ).toLatin1() );
	defaultAttributes.insert( QNetworkRequest::ConnectionEncryptedAttribute, false );
	defaultAttributes.insert( QNetworkRequest::HttpPipeliningWasUsedAttribute, false );
	#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		defaultAttributes.insert( QNetworkRequest::SpdyWasUsedAttribute, false );
	#endif // Qt < 6.0.0
	#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		defaultAttributes.insert( QNetworkRequest::Http2WasUsedAttribute, false );
	#elif QT_VERSION >= QT_VERSION_CHECK( 5,9,0 )
		defaultAttributes.insert( QNetworkRequest::HTTP2WasUsedAttribute, false );
	#endif // Qt >= 5.9.0
	RawHeaderHash defaultRawHeaders;
	defaultRawHeaders.insert( contentLengthHeader, QByteArray::number( successPayload.size() ) );
	HeaderHash defaultHeaders;
	defaultHeaders.insert( QNetworkRequest::ContentLengthHeader, successPayload.size() );

	QTest::newRow( "simple, succeeding GET" ) << successBuilder << expectedBehavior  << getReq
		<< defaultAttributes << defaultRawHeaders << defaultHeaders << noError << unknownErrorString << noExpectedWarning
		<< successPayload <<
		( SignalEmissionList() << qMakePair( metaDataChangedSignal, QVariantList() )
		                       << qMakePair( readyReadSignal, QVariantList() )
		                       << qMakePair( downloadProgressSignal, QVariantList() << successPayload.size() << successPayload.size() )
		                       << qMakePair( downloadProgressSignal, QVariantList() << successPayload.size() << successPayload.size() )
		                       << qMakePair( readChannelFinishedSignal, QVariantList() )
		                       << qMakePair( finishedSignal, QVariantList() ) );

	auto httpsAttributes = defaultAttributes;
	httpsAttributes.insert( QNetworkRequest::ConnectionEncryptedAttribute, true );
	QTest::newRow( "simple, succeeding HTTPS GET" ) << successBuilder << expectedBehavior  << getHttpsReq
		<< httpsAttributes << defaultRawHeaders << defaultHeaders << noError << unknownErrorString << noExpectedWarning
		<< successPayload <<
		( SignalEmissionList() << qMakePair( encryptedSignal, QVariantList() )
		                       << qMakePair( metaDataChangedSignal, QVariantList() )
		                       << qMakePair( readyReadSignal, QVariantList() )
		                       << qMakePair( downloadProgressSignal, QVariantList() << successPayload.size() << successPayload.size() )
		                       << qMakePair( downloadProgressSignal, QVariantList() << successPayload.size() << successPayload.size() )
		                       << qMakePair( readChannelFinishedSignal, QVariantList() )
		                       << qMakePair( finishedSignal, QVariantList() ) );

	const QByteArray requestPayload( "{\"request\":\"foo\"}" );

	const Request postReq( QNetworkRequest( QUrl( "http://example.com" ) ), QNetworkAccessManager::PostOperation, requestPayload );
	QTest::newRow( "simple, succeeding POST" ) << successBuilder << expectedBehavior  << postReq
		<< defaultAttributes << defaultRawHeaders << defaultHeaders << noError << unknownErrorString << noExpectedWarning
		<< successPayload <<
		( SignalEmissionList() << qMakePair( uploadProgressSignal, QVariantList() << postReq.body.size() << postReq.body.size() )
		                       << qMakePair( metaDataChangedSignal, QVariantList() )
		                       << qMakePair( readyReadSignal, QVariantList() )
		                       << qMakePair( downloadProgressSignal, QVariantList() << successPayload.size() << successPayload.size() )
		                       << qMakePair( downloadProgressSignal, QVariantList() << successPayload.size() << successPayload.size() )
		                       << qMakePair( readChannelFinishedSignal, QVariantList() )
		                       << qMakePair( finishedSignal, QVariantList() ) );

	const Request httpsPostReq( QNetworkRequest( QUrl( "https://example.com" ) ), QNetworkAccessManager::PostOperation, requestPayload );
	QTest::newRow( "simple, succeeding HTTPS POST" ) << successBuilder << expectedBehavior << httpsPostReq
		<< httpsAttributes << defaultRawHeaders << defaultHeaders << noError << unknownErrorString << noExpectedWarning
		<< successPayload <<
		( SignalEmissionList() << qMakePair( encryptedSignal, QVariantList() )
		                       << qMakePair( uploadProgressSignal, QVariantList() << httpsPostReq.body.size() << httpsPostReq.body.size() )
		                       << qMakePair( metaDataChangedSignal, QVariantList() )
		                       << qMakePair( readyReadSignal, QVariantList() )
		                       << qMakePair( downloadProgressSignal, QVariantList() << successPayload.size() << successPayload.size() )
		                       << qMakePair( downloadProgressSignal, QVariantList() << successPayload.size() << successPayload.size() )
		                       << qMakePair( readChannelFinishedSignal, QVariantList() )
		                       << qMakePair( finishedSignal, QVariantList() ) );

	MockReplyBuilder dontOverrideBuilder;
	const unsigned int fakeContentLength = 1337;
	const QString unusualErrorString( "unusual error" );
	dontOverrideBuilder.withError( notFoundError, unusualErrorString );
	dontOverrideBuilder.withBody( dummyPayload ).withHeader( QNetworkRequest::ContentLengthHeader, fakeContentLength );
	dontOverrideBuilder.withStatus( notFoundCode, customReasonPhrase );
	auto dontOverrideAttributes = defaultAttributes;
	dontOverrideAttributes.insert( QNetworkRequest::HttpStatusCodeAttribute, notFoundCode );
	dontOverrideAttributes.insert( QNetworkRequest::HttpReasonPhraseAttribute, customReasonPhrase );
	RawHeaderHash dontOverrideRawHeaders;
	dontOverrideRawHeaders.insert( contentLengthHeader, "1337" );
	HeaderHash dontOverrideHeaders;
	dontOverrideHeaders.insert( QNetworkRequest::ContentLengthHeader, fakeContentLength );
	QTest::newRow( "don't override explicitly set properties" ) << dontOverrideBuilder << expectedBehavior << getReq
		<< dontOverrideAttributes << dontOverrideRawHeaders << dontOverrideHeaders << notFoundError << unusualErrorString
		<< noExpectedWarning << dummyPayload <<
		( SignalEmissionList() << qMakePair( metaDataChangedSignal, QVariantList() )
		                       << qMakePair( readyReadSignal, QVariantList() )
		                       << qMakePair( downloadProgressSignal, QVariantList() << dummyPayload.size() << dummyPayload.size() )
		#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		                       << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentNotFoundError ) )
		#endif // Qt < 6.0.0
		#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		                       << qMakePair( errorOccurredSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentNotFoundError ) )
		#endif // Qt >= 5.15.0
		                       << qMakePair( downloadProgressSignal, QVariantList() << dummyPayload.size() << dummyPayload.size() )
		                       << qMakePair( readChannelFinishedSignal, QVariantList() )
		                       << qMakePair( finishedSignal, QVariantList() ) );

	const auto notFoundReasonPhrase = HttpStatus::reasonPhrase( HttpStatus::NotFound );

	MockReplyBuilder defaultErrorStringBuilder;
	defaultErrorStringBuilder.withError( notFoundError ).withStatus( HttpStatus::NotFound );
	auto notFoundStatusAttributes = defaultAttributes;
	notFoundStatusAttributes.insert( QNetworkRequest::HttpStatusCodeAttribute, notFoundCode );
	notFoundStatusAttributes.insert( QNetworkRequest::HttpReasonPhraseAttribute, notFoundReasonPhrase.toLatin1() );
	SignalEmissionList getReqErrorSignals;
	getReqErrorSignals << qMakePair( metaDataChangedSignal, QVariantList() )
		#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		               << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentNotFoundError ) )
		#endif // Qt < 6.0.0
		#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		               << qMakePair( errorOccurredSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentNotFoundError ) )
		#endif // Qt >= 5.15.0
		               << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
		               << qMakePair( readChannelFinishedSignal, QVariantList() )
		               << qMakePair( finishedSignal, QVariantList() );
	QTest::newRow( "simple, failing GET" ) << defaultErrorStringBuilder << expectedBehavior << getReq
		<< notFoundStatusAttributes << RawHeaderHash() << HeaderHash() << notFoundError
		<< httpErrorStringTemplate.arg( "http://example.com", "Not Found" ) << noExpectedWarning << QByteArray()
		<< getReqErrorSignals;

	QTest::newRow( "simple, failing POST" ) << defaultErrorStringBuilder << expectedBehavior << postReq
		<< notFoundStatusAttributes << RawHeaderHash() << HeaderHash() << notFoundError
		<< httpErrorStringTemplate.arg( "http://example.com", "Not Found" ) << noExpectedWarning << QByteArray() <<
		( SignalEmissionList() << qMakePair( uploadProgressSignal, QVariantList() << postReq.body.size() << postReq.body.size() )
		                       << qMakePair( metaDataChangedSignal, QVariantList() )
		#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		                       << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentNotFoundError ) )
		#endif // Qt < 6.0.0
		#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		                       << qMakePair( errorOccurredSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentNotFoundError ) )
		#endif // Qt >= 5.15.0
		                       << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
		                       << qMakePair( readChannelFinishedSignal, QVariantList() )
		                       << qMakePair( finishedSignal, QVariantList() ) );


	MockReplyBuilder explicitErrorStringBuilder;
	explicitErrorStringBuilder.withStatus( HttpStatus::NotFound );
	explicitErrorStringBuilder.withError( HttpStatus::statusCodeToNetworkError( HttpStatus::NotFound ),
	                                      notFoundReasonPhrase ); // explicit error string (see issue #37)
	QTest::newRow( "explicit error string" ) << explicitErrorStringBuilder << expectedBehavior << getReq
		<< notFoundStatusAttributes << RawHeaderHash() << HeaderHash() << notFoundError << notFoundReasonPhrase
		<< noExpectedWarning << QByteArray() << getReqErrorSignals;

	MockReplyBuilder invalidStatusCodeBuilder;
	invalidStatusCodeBuilder.withAttribute( QNetworkRequest::HttpStatusCodeAttribute, QByteArray( "Foo" ) );
	AttributeHash networkErrorAttributes;
	networkErrorAttributes.insert( QNetworkRequest::ConnectionEncryptedAttribute, false );
	QTest::newRow( "invalid status code attribute" ) << invalidStatusCodeBuilder << expectedBehavior << getReq
		<< networkErrorAttributes << RawHeaderHash() << HeaderHash() << QNetworkReply::RemoteHostClosedError
		<< "Connection closed" << makeOptionalRegEx( "Invalid type for HttpStatusCodeAttribute. Must be int but was: QByteArray" )
		<< QByteArray() <<
		( SignalEmissionList() << qMakePair( metaDataChangedSignal, QVariantList() )
		#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		                       << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::RemoteHostClosedError ) )
		#endif // Qt < 6.0.0
		#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		                       << qMakePair( errorOccurredSignal, QVariantList() << QVariant::fromValue( QNetworkReply::RemoteHostClosedError ) )
		#endif // Qt >= 5.15.0
		                       << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
		                       << qMakePair( readChannelFinishedSignal, QVariantList() )
		                       << qMakePair( finishedSignal, QVariantList() ) );


	MockReplyBuilder accessDeniedBuilder;
	accessDeniedBuilder.withError( accessDeniedError );
	auto forbiddenStatusAttributes = defaultAttributes;
	forbiddenStatusAttributes.insert( QNetworkRequest::HttpStatusCodeAttribute, forbiddenStatusCode );
	forbiddenStatusAttributes.insert( QNetworkRequest::HttpReasonPhraseAttribute, HttpStatus::reasonPhrase( forbiddenStatusCode ).toLatin1() );
	QTest::newRow( "set status code from client error" ) << accessDeniedBuilder << expectedBehavior << getReq
		<< forbiddenStatusAttributes << RawHeaderHash() << HeaderHash() << accessDeniedError
		<< httpErrorStringTemplate.arg( "http://example.com", "Forbidden" ) << noExpectedWarning
		<< QByteArray() <<
		( SignalEmissionList() << qMakePair( metaDataChangedSignal, QVariantList() )
		#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		                       << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( accessDeniedError ) )
		#endif // Qt < 6.0.0
		#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		                       << qMakePair( errorOccurredSignal, QVariantList() << QVariant::fromValue( accessDeniedError ) )
		#endif // Qt >= 5.15.0
		                       << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
		                       << qMakePair( readChannelFinishedSignal, QVariantList() )
		                       << qMakePair( finishedSignal, QVariantList() ) );

#if QT_VERSION >= QT_VERSION_CHECK( 5,3,0 )
	MockReplyBuilder internalServerErrorBuilder;
	internalServerErrorBuilder.withError( internalServerError );
	auto internalServerErrorStatusAttributes = defaultAttributes;
	internalServerErrorStatusAttributes.insert( QNetworkRequest::HttpStatusCodeAttribute, internalServErrorCode );
	internalServerErrorStatusAttributes.insert( QNetworkRequest::HttpReasonPhraseAttribute, HttpStatus::reasonPhrase( internalServErrorCode ).toLatin1() );
	QTest::newRow( "set status code from server error" ) << internalServerErrorBuilder << expectedBehavior << getReq
		<< internalServerErrorStatusAttributes << RawHeaderHash() << HeaderHash() << internalServerError
		<< httpErrorStringTemplate.arg( "http://example.com", "Internal Server Error" ) << noExpectedWarning
		<< QByteArray() <<
		( SignalEmissionList() << qMakePair( metaDataChangedSignal, QVariantList() )
		#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		                       << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( internalServerError ) )
		#endif // Qt < 6.0.0
		#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		                       << qMakePair( errorOccurredSignal, QVariantList() << QVariant::fromValue( internalServerError ) )
		#endif // Qt >= 5.15.0
		                       << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
		                       << qMakePair( readChannelFinishedSignal, QVariantList() )
		                       << qMakePair( finishedSignal, QVariantList() ) );
#endif // Qt >= 5.3.0

	MockReplyBuilder temporaryNetworkErrorBuilder;
	temporaryNetworkErrorBuilder.withError( temporaryNetworkError );
	QTest::newRow( "set status code from network error" ) << temporaryNetworkErrorBuilder << expectedBehavior << getReq
		<< networkErrorAttributes << RawHeaderHash() << HeaderHash() << temporaryNetworkError
		<< "Temporary network failure." << noExpectedWarning << QByteArray() <<
		( SignalEmissionList() << qMakePair( metaDataChangedSignal, QVariantList() )
		#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		                       << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( temporaryNetworkError ) )
		#endif // Qt < 6.0.0
		#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		                       << qMakePair( errorOccurredSignal, QVariantList() << QVariant::fromValue( temporaryNetworkError ) )
		#endif // Qt >= 5.15.0
		                       << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
		                       << qMakePair( readChannelFinishedSignal, QVariantList() )
		                       << qMakePair( finishedSignal, QVariantList() ) );

	MockReplyBuilder notFoundBuilder;
	notFoundBuilder.withStatus( notFoundCode );
	QTest::newRow( "set error from status code" ) << notFoundBuilder << expectedBehavior << getReq
		<< notFoundStatusAttributes << RawHeaderHash() << HeaderHash() << notFoundError
		<< httpErrorStringTemplate.arg( "http://example.com", "Not Found" ) << noExpectedWarning << QByteArray()
		<< getReqErrorSignals;

	AttributeHash fileDefaultAttributes;
	fileDefaultAttributes.insert( QNetworkRequest::ConnectionEncryptedAttribute, false );

	MockReplyBuilder fileGetSuccessBuilder;
	fileGetSuccessBuilder.withBody( successPayload );
	QTest::newRow( "file GET success" ) << fileGetSuccessBuilder << expectedBehavior << fileGetRequest
		<< fileDefaultAttributes << RawHeaderHash() << defaultHeaders << noError << unknownErrorString << noExpectedWarning
		<< successPayload << ( SignalEmissionList()
		                       << qMakePair( metaDataChangedSignal, QVariantList() )
		                       << qMakePair( downloadProgressSignal, QVariantList() << successPayload.size() << successPayload.size() )
		                       << qMakePair( readyReadSignal, QVariantList() )
		                       << qMakePair( finishedSignal, QVariantList() ) );

	QTest::newRow( "file HEAD success" ) << fileGetSuccessBuilder << expectedBehavior << fileHeadRequest
		<< fileDefaultAttributes << RawHeaderHash() << defaultHeaders << noError << unknownErrorString << noExpectedWarning
		<< successPayload << ( SignalEmissionList()
		                       << qMakePair( metaDataChangedSignal, QVariantList() )
		                       << qMakePair( downloadProgressSignal, QVariantList() << successPayload.size() << successPayload.size() )
		                       << qMakePair( readyReadSignal, QVariantList() )
		                       << qMakePair( finishedSignal, QVariantList() ) );

	MockReplyBuilder filePutSuccessBuilder;
	filePutSuccessBuilder.withError( noError );
	QTest::newRow( "file PUT success" ) << filePutSuccessBuilder << expectedBehavior << filePutRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << noError << unknownErrorString << noExpectedWarning
		<< QByteArray() <<
		( SignalEmissionList() << qMakePair( uploadProgressSignal, QVariantList() << filePutRequest.body.size() << filePutRequest.body.size() )
		                       << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
		                       << qMakePair( readChannelFinishedSignal, QVariantList() )
		                       << qMakePair( finishedSignal, QVariantList() ) );

	const Request fileEmptyPutRequest( QNetworkRequest( fileUrl ), QNetworkAccessManager::PutOperation );
	QTest::newRow( "file empty PUT success" ) << filePutSuccessBuilder << expectedBehavior << fileEmptyPutRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << noError << unknownErrorString << noExpectedWarning
		<< QByteArray() <<
		( SignalEmissionList() << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
		                       << qMakePair( uploadProgressSignal, QVariantList() << 0 << 0 )
		                       << qMakePair( readChannelFinishedSignal, QVariantList() )
		                       << qMakePair( finishedSignal, QVariantList() ) );

	MockReplyBuilder filePutProtocolFailureBuilder;
	filePutProtocolFailureBuilder.withError( QNetworkReply::ProtocolFailure );

	SignalEmissionList fileProtocolFailureSignals;
	fileProtocolFailureSignals
		#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		 << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ProtocolFailure ) )
		#endif // Qt < 6.0.0
		#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		 << qMakePair( errorOccurredSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ProtocolFailure ) )
		#endif // Qt >= 5.15.0
		 << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
		 << qMakePair( uploadProgressSignal, QVariantList() << 0 << 0 )
		 << qMakePair( readChannelFinishedSignal, QVariantList() )
		 << qMakePair( finishedSignal, QVariantList() );


	QTest::newRow( "file PUT protocol failure" ) << filePutProtocolFailureBuilder << expectedBehavior << filePutRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << QNetworkReply::ProtocolFailure
		<< QStringLiteral( "Write error writing to %1: %2" ).arg( fileUrl.toString(), unknownErrorString )
		<< noExpectedWarning << QByteArray() << fileProtocolFailureSignals;


	const auto fileErrorStringTemplate = QStringLiteral( "Error opening %1: %2" ).arg( fileUrl.toString() );
	const auto fileAccessDeniedErrorString = fileErrorStringTemplate.arg( "Access denied" );
	MockReplyBuilder fileAccessDeniedBuilder;
	fileAccessDeniedBuilder.withError( QNetworkReply::ContentAccessDenied, fileAccessDeniedErrorString );
	QTest::newRow( "file GET access denied" ) << fileAccessDeniedBuilder << expectedBehavior << fileGetRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << accessDeniedError
		<< fileAccessDeniedErrorString << noExpectedWarning << QByteArray()
		<< ( SignalEmissionList()
		     #if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		         << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentAccessDenied ) )
		     #endif // Qt < 6.0.0
		     #if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		         << qMakePair( errorOccurredSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentAccessDenied ) )
		     #endif // Qt >= 5.15.0
		     << qMakePair( finishedSignal, QVariantList() ) );

	const auto fileNotFoundErrorString = fileErrorStringTemplate.arg( "No such file or directory" );
	MockReplyBuilder fileNotExistsBuilder;
	fileNotExistsBuilder.withError( QNetworkReply::ContentNotFoundError, fileNotFoundErrorString );
	QTest::newRow( "file GET not found" ) << fileNotExistsBuilder << expectedBehavior << fileGetRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << notFoundError
		<< fileNotFoundErrorString << noExpectedWarning << QByteArray()
		<< ( SignalEmissionList()
		     #if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		         << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentNotFoundError ) )
		     #endif // Qt < 6.0.0
		     #if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		         << qMakePair( errorOccurredSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentNotFoundError ) )
		     #endif // Qt >= 5.15.0
		     << qMakePair( finishedSignal, QVariantList() ) );

	QTest::newRow( "file HEAD not found" ) << fileNotExistsBuilder << expectedBehavior << fileHeadRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << notFoundError
		<< fileNotFoundErrorString << noExpectedWarning << QByteArray()
		<< 	( SignalEmissionList()
		     #if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		         << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentNotFoundError ) )
		     #endif // Qt < 6.0.0
		     #if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		         << qMakePair( errorOccurredSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentNotFoundError ) )
		     #endif // Qt >= 5.15.0
		     << qMakePair( finishedSignal, QVariantList() ) );

	SignalEmissionList filePutAccessDeniedSignals;
	filePutAccessDeniedSignals
		#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		 << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentAccessDenied ) )
		#endif // Qt < 6.0.0
		#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		 << qMakePair( errorOccurredSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentAccessDenied ) )
		#endif // Qt >= 5.15.0
		 << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
		 << qMakePair( uploadProgressSignal, QVariantList() << 0 << 0 )
		 << qMakePair( readChannelFinishedSignal, QVariantList() )
		 << qMakePair( finishedSignal, QVariantList() );

	QTest::newRow( "file PUT access denied" ) << fileAccessDeniedBuilder << expectedBehavior << filePutRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << accessDeniedError
		<< fileAccessDeniedErrorString << noExpectedWarning << QByteArray() << filePutAccessDeniedSignals;

	SignalEmissionList fileProtocolUnknownSignals;
	fileProtocolUnknownSignals
		#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		 << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ProtocolUnknownError ) )
		#endif // Qt < 6.0.0
		#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		 << qMakePair( errorOccurredSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ProtocolUnknownError ) )
		#endif // Qt >= 5.15.0
		 << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
		 << qMakePair( readChannelFinishedSignal, QVariantList() )
		 << qMakePair( finishedSignal, QVariantList() );

	const Request fileDeleteRequest( QNetworkRequest( fileUrl ), QNetworkAccessManager::DeleteOperation );
	const Request filePostRequest( QNetworkRequest( fileUrl ), QNetworkAccessManager::PostOperation, successPayload );
	const Request fileCustomRequest( QNetworkRequest( fileUrl ), QNetworkAccessManager::CustomOperation );
	MockReplyBuilder fileProtocolErrorBuilder;
	const QNetworkReply::NetworkError protocolUnknownError = QNetworkReply::ProtocolUnknownError;
	const QString fileProtocolUnknownError( "Protocol \"file\" is unknown" );
	fileProtocolErrorBuilder.withError( protocolUnknownError, fileProtocolUnknownError );
	QTest::newRow( "file DELETE" ) << fileProtocolErrorBuilder << expectedBehavior << fileDeleteRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << protocolUnknownError << fileProtocolUnknownError
		<< noExpectedWarning << QByteArray() << fileProtocolUnknownSignals;
	QTest::newRow( "file POST" ) << fileProtocolErrorBuilder << expectedBehavior << filePostRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << protocolUnknownError << fileProtocolUnknownError
		<< noExpectedWarning << QByteArray() << fileProtocolUnknownSignals;
	QTest::newRow( "file custom verb" ) << fileProtocolErrorBuilder << expectedBehavior << fileCustomRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << protocolUnknownError << fileProtocolUnknownError
		<< noExpectedWarning << QByteArray() << fileProtocolUnknownSignals;

	MockReplyBuilder fileDeleteSuccessBuilder;
	fileDeleteSuccessBuilder.withError( noError );
	QTest::newRow( "file DELETE success" ) << fileDeleteSuccessBuilder << expectedBehavior << fileDeleteRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << protocolUnknownError << fileProtocolUnknownError
		<< makeOptionalRegEx( "Reply was configured to reply with error (?:0|QNetworkReply::(?:NetworkError\\()?NoError\\)?) "
		                      "but a request DELETE file:///path/to/file.txt must be replied with "
		                      "(?:301|QNetworkReply::(?:NetworkError\\()?ProtocolUnknownError\\)?) . "
		                      "Overriding configured behavior." )
		<< QByteArray() << fileProtocolUnknownSignals;

	QTest::newRow( "file POST, final upload00" ) << fileProtocolErrorBuilder
		<< ( expectedBehavior | Behavior_FinalUpload00Signal ) << filePostRequest << fileDefaultAttributes
		<< RawHeaderHash() << HeaderHash() << protocolUnknownError << fileProtocolUnknownError
		<< noExpectedWarning << QByteArray() <<
		( SignalEmissionList()
			#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
			 << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ProtocolUnknownError ) )
			#endif // Qt < 6.0.0
			#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
			 << qMakePair( errorOccurredSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ProtocolUnknownError ) )
			#endif // Qt >= 5.15.0
			 << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
			 << qMakePair( uploadProgressSignal, QVariantList() << 0 << 0 )
			 << qMakePair( readChannelFinishedSignal, QVariantList() )
			 << qMakePair( finishedSignal, QVariantList() ) );


	MockReplyBuilder qrcGetSuccessBuilder;
	qrcGetSuccessBuilder.withBody( successPayload );
	QTest::newRow( "qrc GET success" ) << qrcGetSuccessBuilder << expectedBehavior << qrcGetRequest
		<< fileDefaultAttributes << RawHeaderHash() << defaultHeaders << noError << unknownErrorString << noExpectedWarning
		<< successPayload << ( SignalEmissionList()
		                       << qMakePair( metaDataChangedSignal, QVariantList() )
		                       << qMakePair( downloadProgressSignal, QVariantList() << successPayload.size() << successPayload.size() )
		                       << qMakePair( readyReadSignal, QVariantList() )
		                       << qMakePair( finishedSignal, QVariantList() ) );

	QTest::newRow( "qrc HEAD success" ) << qrcGetSuccessBuilder << expectedBehavior << qrcHeadRequest
		<< fileDefaultAttributes << RawHeaderHash() << defaultHeaders << noError << unknownErrorString << noExpectedWarning
		<< successPayload << ( SignalEmissionList()
		                       << qMakePair( metaDataChangedSignal, QVariantList() )
		                       << qMakePair( downloadProgressSignal, QVariantList() << successPayload.size() << successPayload.size() )
		                       << qMakePair( readyReadSignal, QVariantList() )
		                       << qMakePair( finishedSignal, QVariantList() ) );

	MockReplyBuilder qrcPutSuccessBuilder;
	qrcPutSuccessBuilder.withError( noError );
	QTest::newRow( "qrc PUT invalid success" ) << qrcPutSuccessBuilder << expectedBehavior << qrcPutRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << accessDeniedError << unknownErrorString
		<< makeOptionalRegEx( "Reply was configured to reply with error (?:0|QNetworkReply::(?:NetworkError\\()?NoError\\)?) "
		                      "but a qrc request does not support writing operations and therefore has to reply with "
		                      "(?:201|QNetworkReply::(?:NetworkError\\()?ContentAccessDenied\\)?) . "
		                      "Overriding configured behavior." )
		<< QByteArray() << filePutAccessDeniedSignals;

	MockReplyBuilder qrcPutFailureBuilder;
	qrcPutFailureBuilder.withError( accessDeniedError, "Custom error message" );
	QTest::newRow( "qrc PUT failure" ) << qrcPutFailureBuilder << expectedBehavior << qrcPutRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << accessDeniedError << "Custom error message"
		<< noExpectedWarning << QByteArray() << filePutAccessDeniedSignals;

	const QString qrcErrorStringTemplate = QStringLiteral( "Error opening %1: %2" ).arg( qrcUrl.toString() );
	QTest::newRow( "qrc GET access denied" ) << accessDeniedBuilder << expectedBehavior << qrcGetRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << accessDeniedError
		<< qrcErrorStringTemplate.arg( unknownErrorString ) << noExpectedWarning << QByteArray()
		<< ( SignalEmissionList()
		     #if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		         << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( accessDeniedError ) )
		     #endif // Qt < 6.0.0
		     #if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		         << qMakePair( errorOccurredSignal, QVariantList() << QVariant::fromValue( accessDeniedError ) )
		     #endif // Qt >= 5.15.0
		     << qMakePair( finishedSignal, QVariantList() ) );

	const Request qrcDeleteRequest( QNetworkRequest( qrcUrl ), QNetworkAccessManager::DeleteOperation );
	const Request qrcPostRequest( QNetworkRequest( qrcUrl ), QNetworkAccessManager::PostOperation, successPayload );
	const Request qrcCustomRequest( QNetworkRequest( qrcUrl ), QNetworkAccessManager::CustomOperation );
	MockReplyBuilder qrcProtocolErrorBuilder;
	const QString qrcProtocolUnknownError( "Protocol \"qrc\" is unknown" );
	qrcProtocolErrorBuilder.withError( protocolUnknownError, qrcProtocolUnknownError );
	QTest::newRow( "qrc DELETE" ) << qrcProtocolErrorBuilder << expectedBehavior << qrcDeleteRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << protocolUnknownError << qrcProtocolUnknownError
		<< noExpectedWarning << QByteArray() << fileProtocolUnknownSignals;
	QTest::newRow( "qrc POST" ) << qrcProtocolErrorBuilder << expectedBehavior << qrcPostRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << protocolUnknownError << qrcProtocolUnknownError
		<< noExpectedWarning << QByteArray() << fileProtocolUnknownSignals;
	QTest::newRow( "qrc custom verb" ) << qrcProtocolErrorBuilder << expectedBehavior << qrcCustomRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << protocolUnknownError << qrcProtocolUnknownError
		<< noExpectedWarning << QByteArray() << fileProtocolUnknownSignals;

	MockReplyBuilder qrcPostSuccessBuilder;
	qrcPostSuccessBuilder.withError( noError );
	QTest::newRow( "qrc POST success" ) << qrcPostSuccessBuilder << expectedBehavior << qrcPostRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << protocolUnknownError << qrcProtocolUnknownError
		<< makeOptionalRegEx( "Reply was configured to reply with error (?:0|QNetworkReply::(?:NetworkError\\()?NoError\\)?) "
		                      "but a request POST qrc:/path/to/file.txt must be replied with "
		                      "(?:301|QNetworkReply::(NetworkError\\()?ProtocolUnknownError\\)?) . "
		                      "Overriding configured behavior." )
		<< QByteArray() << fileProtocolUnknownSignals;
}


/*! \test %Tests the default error strings.
 */
void MockReplyTest::testDefaultErrorString()
{
	QFETCH( MockReplyBuilder, builder );
	QFETCH( Request, request );

	auto connection = createConnection( request );
	MockReplyUniquePtr reply( builder.createReply() );

	MockReplyTestHelper::callSimulate( reply.get(), request, connection.get() );
	QTRY_VERIFY( reply->isFinished() );

	QTEST( reply->errorString(), "expectedErrorString" );
}


/*! Provides the data for the testDefaultErrorString() test.
 */
void MockReplyTest::testDefaultErrorString_data()
{
	QTest::addColumn< MockReplyBuilder >( "builder" );
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< QString >( "expectedErrorString" );

	const Request httpRequest( QNetworkRequest( QUrl( "http://example.com" ) ), QNetworkAccessManager::GetOperation );
	const Request ftpRequest( QNetworkRequest( QUrl( "ftp://example.com" ) ), QNetworkAccessManager::GetOperation );
	const QString filePath = fileGetRequest.qRequest.url().toString();

	using namespace HelperMethods;

	QTest::newRow( "http remote host closed" ) << createErrorBuilder( QNetworkReply::RemoteHostClosedError ) << httpRequest << "Connection closed";
	QTest::newRow( "http operation canceled" ) << createErrorBuilder( QNetworkReply::OperationCanceledError ) << httpRequest << "Operation canceled";
	QTest::newRow( "http connection refused" ) << createErrorBuilder( QNetworkReply::ConnectionRefusedError ) << httpRequest << "Connection refused";
	QTest::newRow( "http ssl handshake failed" ) << createErrorBuilder( QNetworkReply::SslHandshakeFailedError ) << httpRequest << "SSL handshake failed";
	QTest::newRow( "http network session failed" ) << createErrorBuilder( QNetworkReply::NetworkSessionFailedError ) << httpRequest << "Network session error.";
	QTest::newRow( "http background request" ) << createErrorBuilder( QNetworkReply::BackgroundRequestNotAllowedError ) << httpRequest << "Background request not allowed.";
	QTest::newRow( "http too many redirects" ) << createErrorBuilder( QNetworkReply::TooManyRedirectsError ) << httpRequest << "Too many redirects";
	QTest::newRow( "http insecure redirect" ) << createErrorBuilder( QNetworkReply::InsecureRedirectError ) << httpRequest << "Insecure redirect";
	QTest::newRow( "http proxy connection refused" ) << createErrorBuilder( QNetworkReply::ProxyConnectionRefusedError ) << httpRequest << "Proxy connection refused";
	QTest::newRow( "http proxy connection closed" ) << createErrorBuilder( QNetworkReply::ProxyConnectionClosedError ) << httpRequest << "Proxy connection closed prematurely";
	QTest::newRow( "http proxy not found" ) << createErrorBuilder( QNetworkReply::ProxyNotFoundError ) << httpRequest << "No suitable proxy found";
	QTest::newRow( "http proxy timeout" ) << createErrorBuilder( QNetworkReply::ProxyTimeoutError ) << httpRequest << "Proxy server connection timed out";
	QTest::newRow( "http protocol unknown" ) << createErrorBuilder( QNetworkReply::ProtocolUnknownError ) << httpRequest << "Unknown protocol specified";
	QTest::newRow( "http unknown network error" ) << createErrorBuilder( QNetworkReply::UnknownNetworkError ) << httpRequest << "Unknown network error";
	QTest::newRow( "http unknown proxy error" ) << createErrorBuilder( QNetworkReply::UnknownProxyError ) << httpRequest << "Unknown proxy error";
	QTest::newRow( "http timeout" ) << createErrorBuilder( QNetworkReply::TimeoutError ) << httpRequest << "Socket operation timed out";
	QTest::newRow( "http authentication required" ) << createErrorBuilder( QNetworkReply::AuthenticationRequiredError ) << httpRequest << "Host requires authentication";
	QTest::newRow( "http authentication required" ) << createErrorBuilder( QNetworkReply::AuthenticationRequiredError ) << httpRequest << "Host requires authentication";
	QTest::newRow( "ftp remote host closed" )  << createErrorBuilder( QNetworkReply::RemoteHostClosedError ) << ftpRequest  << "Connection closed";
	QTest::newRow( "ftp connection refused" )  << createErrorBuilder( QNetworkReply::ConnectionRefusedError ) << ftpRequest  << "Connection refused to host example.com";
	QTest::newRow( "ftp timeout" )  << createErrorBuilder( QNetworkReply::TimeoutError ) << ftpRequest  << "Connection timed out to host example.com";
	QTest::newRow( "ftp authentication required" )  << createErrorBuilder( QNetworkReply::AuthenticationRequiredError ) << ftpRequest  << "Logging in to example.com failed: authentication required";
	QTest::newRow( "file protocol unknown" ) << createErrorBuilder( QNetworkReply::ProtocolUnknownError ) << fileGetRequest << "Protocol \"file\" is unknown";
	QTest::newRow( "file operation not permitted" ) << createErrorBuilder( QNetworkReply::ContentOperationNotPermittedError ) << fileGetRequest \
		<< QStringLiteral( "Cannot open %1: Path is a directory" ).arg( filePath );
	QTest::newRow( "file content not found" ) << createErrorBuilder( QNetworkReply::ContentNotFoundError ) << fileGetRequest \
		<< QStringLiteral( "Error opening %1: No such file or directory" ).arg( filePath );
	QTest::newRow( "file access denied" ) << createErrorBuilder( QNetworkReply::ContentAccessDenied ) << fileGetRequest \
		<< QStringLiteral( "Error opening %1: Access denied" ).arg( filePath );
	QTest::newRow( "file read error" ) << createErrorBuilder( QNetworkReply::ProtocolFailure ) << fileGetRequest \
		<< QStringLiteral( "Read error reading from %1: Unknown error" ).arg( filePath );

}

/*! \test %Tests the MockReply::abort() method.
 */
void MockReplyTest::testAbort()
{
	TestMockReply reply;
	reply.setAttribute( QNetworkRequest::HttpStatusCodeAttribute, static_cast< int >( HttpStatus::OK ) );

	QVERIFY( reply.isRunning() );
	QVERIFY( !reply.isFinished() );
	QCOMPARE( reply.error(), QNetworkReply::NoError );

	reply.abort();

	QVERIFY( !reply.isRunning() );
	QVERIFY( reply.isFinished() );
	QVERIFY( !reply.isOpen() );
	QCOMPARE( reply.error(), QNetworkReply::OperationCanceledError );
}



/*! \test %Tests the behavior of an unconfigured MockReplyBuilder.
 */
void MockReplyTest::testNullBuilder()
{
	MockReplyBuilder builder;

	const auto createdReply = builder.createReply();
	QVERIFY( !createdReply );
}

/*! \test %Tests the MockReplyBuilder::withBody(const QByteArray&) method.
 */
void MockReplyTest::testWithBodyByteArray()
{
	QFETCH( Request, request );
	QFETCH( QByteArray, body );
	QFETCH( QString, contentType );

	MockReplyBuilder builder;
	builder.withBody( body, contentType );

	const auto connection = createConnection( request );
	const auto reply = builder.createReply();

	MockReplyTestHelper::callSimulate( reply.get(), request, connection.get() );
	QTRY_VERIFY( reply->isFinished() );

	QCOMPARE( reply->bytesAvailable(), body.size() );
	QCOMPARE( reply->body(), body );
	QCOMPARE( reply->readAll(), body );
	QTEST( reply->header( QNetworkRequest::ContentLengthHeader ), "expectedContentLength" );
	#if QT_VERSION < QT_VERSION_CHECK( 5,7,0 ) // Not sure if 5.7.0 is the correct version here; 5.6 is known to fail
		QEXPECT_FAIL( "json body", "JSON is not detected by QMimeDatabase::mimeTypeForFileNameAndData() in older Qt versions", Continue );
	#endif // Qt <= 5.6.0
	QTEST( reply->header( QNetworkRequest::ContentTypeHeader ), "expectedContentType" );
}

/*! Provides the data for the testWithBodyByteArray() test.
 */
void MockReplyTest::testWithBodyByteArray_data()
{
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< QByteArray >( "body" );
	QTest::addColumn< QString >( "contentType" );
	QTest::addColumn< QVariant >( "expectedContentLength" );
	QTest::addColumn< QVariant >( "expectedContentType" );

	const auto plainText = QByteArrayLiteral( "foo bar" );
	const auto json = QByteArrayLiteral( "{\"status\": \"ok\"}" );
	const auto xml = QByteArrayLiteral( "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><foo></foo>" );
	const auto customContentType = QStringLiteral( "application/x-custom" );
	Request simpleRequest( QNetworkRequest( QUrl{ "http://example.com" } ) );
	Request jsonRequest( QNetworkRequest( QUrl{ "http://example.com/some.json" } ) );

	QTest::newRow( "null body" )  << Request( QNetworkRequest() ) << QByteArray()     << QString() << QVariant()                              << QVariant();
	QTest::newRow( "empty body" ) << Request( QNetworkRequest() ) << QByteArray( "" ) << QString() << QVariant(0)                             << QVariant();
	QTest::newRow( "plain text" ) << simpleRequest                << plainText        << QString() << QVariant::fromValue( plainText.size() ) << QVariant::fromValue( contentTypeTextPlain );
	QTest::newRow( "json body" )  << jsonRequest                  << json             << QString() << QVariant::fromValue( json.size() )      << QVariant::fromValue( contentTypeJson );
	QTest::newRow( "xml body" )   << simpleRequest                << xml              << QString() << QVariant::fromValue( xml.size() )       << QVariant::fromValue( contentTypeXml );
	QTest::newRow( "explicit content type" ) << simpleRequest << xml << customContentType << QVariant::fromValue( xml.size() ) << QVariant::fromValue( customContentType );
}

/*! Provides the data for the testWithBodyJson() test.
 */
void MockReplyTest::testWithBodyJson_data()
{
	QTest::addColumn< QJsonDocument >( "body" );
	QTest::addColumn< QVariant >( "expectedContentLength" );

	QTest::newRow( "null body" )  << QJsonDocument()                     << QVariant();
	QTest::newRow( "empty body" ) << QJsonDocument::fromJson( "" )       << QVariant();
	const QByteArray fooBarJson( "{\"foo\":\"bar\"}" );
	QTest::newRow( "object" )     << QJsonDocument::fromJson(fooBarJson) << QVariant(fooBarJson.size());
}

/*! \test %Tests the MockReplyBuilder::withBody(const QJsonDocument&) method.
 */
void MockReplyTest::testWithBodyJson()
{
	QFETCH( QJsonDocument, body );

	MockReplyBuilder builder;
	builder.withBody( body );

	const auto request = Request( QNetworkRequest() );
	const auto connection = createConnection( request );
	const auto reply = builder.createReply();

	MockReplyTestHelper::callSimulate( reply.get(), request, connection.get() );
	QTRY_VERIFY( reply->isFinished() );

	QByteArray rawBody = body.toJson( QJsonDocument::Compact );
	QCOMPARE( reply->bytesAvailable(), rawBody.size() );
	QCOMPARE( reply->readAll(), rawBody);
	QCOMPARE( reply->header( QNetworkRequest::ContentTypeHeader ).toString(), contentTypeJson );
	QTEST( reply->header( QNetworkRequest::ContentLengthHeader ), "expectedContentLength" );
}

/*! \test %Tests the MockReplyBuilder::withFile() method.
 */
void MockReplyTest::testWithFile()
{
	QFETCH( QString, file );

	QString filePath;
	if ( QTest::currentDataTag() == QString( "non existing file" ) )
		filePath = file;
	else
	{
		filePath = QFINDTESTDATA( file );
		Q_ASSERT( !filePath.isEmpty() );
	}

	MockReplyBuilder builder;

	if ( QTest::currentDataTag() == QString( "non existing file" ) )
	{
		const QString warningMsg = QString( "QIODevice::read (QFile, \"%1\"): device not open" ).arg( QDir::toNativeSeparators( filePath ) );
		QTest::ignoreMessage( QtWarningMsg, warningMsg.toLatin1() );
	}

	builder.withFile( filePath );

	const auto request = Request( QNetworkRequest() );
	const auto connection = createConnection( request );
	const auto reply = builder.createReply();

	QFile fileObj( filePath );
	fileObj.open( QIODevice::ReadOnly );

	QByteArray fileContent = fileObj.readAll();

	MockReplyTestHelper::callSimulate( reply.get(), request, connection.get() );
	QTRY_VERIFY( reply->isFinished() );

	QCOMPARE( reply->bytesAvailable(), fileContent.size() );
	QCOMPARE( reply->readAll(), fileContent );
	QTEST( reply->header( QNetworkRequest::ContentTypeHeader ).toString(), "contentType" );
}

/*! Provides the data for the testWithFile() test.
 */
void MockReplyTest::testWithFile_data()
{
	QTest::addColumn< QString >( "file" );
	QTest::addColumn< QString >( "contentType" );

	QTest::newRow( "plain text" )        << "data/plainText.txt"                << contentTypeTextPlain;
	QTest::newRow( "xml" )               << "data/simple.xml"                   << "application/xml";
	QTest::newRow( "empty text file" )   << "data/empty.txt"                    << contentTypeTextPlain;
	QTest::newRow( "non existing file" ) << "data/this_file_does_not_exist.foo" << QString();
}

/*! \test %Tests the MockReplyBuilder::withStatus() method.
 */
void MockReplyTest::testWithStatus()
{
	QFETCH( int, statusCode );
	QFETCH( QString, reasonPhrase );

	MockReplyBuilder builder;
	builder.withStatus( statusCode, reasonPhrase );

	const auto request = Request( QNetworkRequest() );
	const auto connection = createConnection( request );
	const auto reply = builder.createReply();

	QSignalSpy metaDataChangedSpy( reply.get(), &QNetworkReply::metaDataChanged );

	MockReplyTestHelper::callSimulate( reply.get(), request, connection.get() );
	QTRY_VERIFY( !metaDataChangedSpy.isEmpty() );

	QCOMPARE( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt(), statusCode );
	if ( reasonPhrase.isNull() )
		QCOMPARE( reply->attribute( QNetworkRequest::HttpReasonPhraseAttribute ).toString(), HttpStatus::reasonPhrase( statusCode ) );
	else
		QCOMPARE( reply->attribute( QNetworkRequest::HttpReasonPhraseAttribute ).toString(), reasonPhrase );
	QTEST( reply->error(), "expectedError" );
	QTEST( reply->errorString(), "expectedErrorString" );

	// Nothing should change even after the reply finished
	QTRY_VERIFY( reply->isFinished() );
	QCOMPARE( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt(), statusCode );
	if ( reasonPhrase.isNull() )
		QCOMPARE( reply->attribute( QNetworkRequest::HttpReasonPhraseAttribute ).toString(), HttpStatus::reasonPhrase( statusCode ) );
	else
		QCOMPARE( reply->attribute( QNetworkRequest::HttpReasonPhraseAttribute ).toString(), reasonPhrase );
	QTEST( reply->error(), "expectedError" );
	QTEST( reply->errorString(), "expectedErrorString" );
}

/*! Provides the data for the testWithStatus() test.
 */
void MockReplyTest::testWithStatus_data()
{
	QTest::addColumn< int >( "statusCode" );
	QTest::addColumn< QString >( "reasonPhrase" );
	QTest::addColumn< QNetworkReply::NetworkError >( "expectedError" );
	QTest::addColumn< QString >( "expectedErrorString" );

	QTest::newRow( "success" )                         << static_cast<int>( HttpStatus::OK )                << "OK"         << QNetworkReply::NoError << unknownErrorString;
	QTest::newRow( "Qt unknown content error" )        << static_cast<int>( HttpStatus::ExpectationFailed ) << QString()    << QNetworkReply::UnknownContentError << "Error transferring  - server replied: Expectation Failed";
	QTest::newRow( "Qt unknown server error" )         << static_cast<int>( HttpStatus::NotExtended )       << QString()    << QNetworkReply::UnknownServerError << "Error transferring  - server replied: Not Extended";
	QTest::newRow( "custom error code" )               << 666                                               << QString()    << QNetworkReply::ProtocolFailure << "Data corrupted";
	QTest::newRow( "default reason phrase" )           << static_cast<int>( HttpStatus::Created )           << QString()    << QNetworkReply::NoError << unknownErrorString;
	QTest::newRow( "custom reason phrase" )            << static_cast<int>( HttpStatus::MovedPermanently )  << "Go there"   << QNetworkReply::NoError << unknownErrorString;
	QTest::newRow( "error" )                           << static_cast<int>( HttpStatus::NotFound )          << QString()    << QNetworkReply::ContentNotFoundError << "Error transferring  - server replied: Not Found";
	QTest::newRow( "error with custom reason phrase" ) << static_cast<int>( HttpStatus::BadRequest )        << "Oops Error" << QNetworkReply::ProtocolInvalidOperationError << "Error transferring  - server replied: Oops Error";
}

void MockReplyTest::testWithStatusMultipleTimes()
{
	QFETCH( int, firstStatusCode );
	QFETCH( int, secondStatusCode );
	QFETCH( QString, firstReasonPhrase );
	QFETCH( QString, secondReasonPhrase );

	MockReplyBuilder builder;
	builder.withStatus( firstStatusCode, firstReasonPhrase );
	builder.withStatus( secondStatusCode, secondReasonPhrase );

	const auto request = Request( QNetworkRequest() );
	const auto connection = createConnection( request );
	const auto reply = builder.createReply();

	QSignalSpy metaDataChangedSpy( reply.get(), &QNetworkReply::metaDataChanged );

	MockReplyTestHelper::callSimulate( reply.get(), request, connection.get() );
	QTRY_VERIFY( !metaDataChangedSpy.isEmpty() );

	QTEST( reply->attribute( QNetworkRequest::HttpReasonPhraseAttribute ).toString(), "expectedReasonPhrase" );
	QTEST( reply->error(), "expectedError" );
	QTEST( reply->errorString(), "expectedErrorString" );
}

void MockReplyTest::testWithStatusMultipleTimes_data()
{
	QTest::addColumn< int >( "firstStatusCode" );
	QTest::addColumn< QString >( "firstReasonPhrase" );
	QTest::addColumn< int >( "secondStatusCode" );
	QTest::addColumn< QString >( "secondReasonPhrase" );
	QTest::addColumn< QString >( "expectedReasonPhrase" );
	QTest::addColumn< QNetworkReply::NetworkError >( "expectedError" );
	QTest::addColumn< QString >( "expectedErrorString" );

	QTest::newRow( "error -> error" )    << static_cast< int >( HttpStatus::NotFound ) << QString{} << static_cast< int >( HttpStatus::Forbidden ) << QString{} << HttpStatus::reasonPhrase( HttpStatus::Forbidden ) << QNetworkReply::ContentAccessDenied  << "Error transferring  - server replied: Forbidden";
	QTest::newRow( "error -> no error" ) << static_cast< int >( HttpStatus::NotFound ) << QString{} << static_cast< int >( HttpStatus::OK )        << QString{} << HttpStatus::reasonPhrase( HttpStatus::OK )        << QNetworkReply::NoError              << unknownErrorString;
	QTest::newRow( "no error -> error" ) << static_cast< int >( HttpStatus::OK )       << QString{} << static_cast< int >( HttpStatus::NotFound )  << QString{} << HttpStatus::reasonPhrase( HttpStatus::NotFound )  << QNetworkReply::ContentNotFoundError << "Error transferring  - server replied: Not Found";
	QTest::newRow( "standard reason -> custom reason" ) << static_cast< int >( HttpStatus::NotFound ) << QString{} << static_cast< int >( HttpStatus::Forbidden ) << "PRIVATE AREA" << "PRIVATE AREA" << QNetworkReply::ContentAccessDenied  << "Error transferring  - server replied: PRIVATE AREA";
	QTest::newRow( "custom reason -> standard reason" ) << static_cast< int >( HttpStatus::NotFound ) << "Missing" << static_cast< int >( HttpStatus::Forbidden ) << QString{} << HttpStatus::reasonPhrase( HttpStatus::Forbidden ) << QNetworkReply::ContentAccessDenied << "Error transferring  - server replied: Forbidden";
}

/*! Provides the data for the testWithError() test.
 */
void MockReplyTest::testWithError_data()
{
	QTest::addColumn<QNetworkReply::NetworkError>("error");
	QTest::addColumn<QString>("errorString");
	QTest::addColumn<int>("finishedStatusCode");

	QTest::newRow("no error")              << QNetworkReply::NoError              << "There is no error" << static_cast<int>(HttpStatus::OK);
	QTest::newRow("content not found")     << QNetworkReply::ContentNotFoundError << "Content not found" << static_cast<int>(HttpStatus::NotFound);
	QTest::newRow("unknown content error") << QNetworkReply::UnknownContentError  << "Broken content"    << static_cast<int>(HttpStatus::BadRequest);
	QTest::newRow("unknown server error")  << QNetworkReply::UnknownServerError   << unknownErrorString  << static_cast<int>(HttpStatus::InternalServerError);
	QTest::newRow("host not found")        << QNetworkReply::HostNotFoundError    << "Host not found"    << -1;
}

/*! \test %Tests the MockReplyBuilder::withError() method.
 */
void MockReplyTest::testWithError()
{
	QFETCH( QNetworkReply::NetworkError, error );
	QFETCH( QString, errorString );
	QFETCH( int, finishedStatusCode );

	MockReplyBuilder builder;
	builder.withError( error, errorString );

	const auto request = Request( QNetworkRequest() );
	const auto connection = createConnection( request );
	const auto reply = builder.createReply();

	QCOMPARE( reply->error(), error );
	QCOMPARE( reply->errorString(), errorString );
	QVERIFY( !reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).isValid() );

	MockReplyTestHelper::callSimulate( reply.get(), request, connection.get() );
	QTRY_VERIFY( reply->isFinished() );

	if ( finishedStatusCode < 0 )
		QVERIFY( !reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).isValid() );
	else
	{
		QVERIFY( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).isValid() );
		QCOMPARE( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt(), finishedStatusCode );
		QCOMPARE( reply->attribute( QNetworkRequest::HttpReasonPhraseAttribute ).toString(), HttpStatus::reasonPhrase( finishedStatusCode ) );
	}
}

/*! \test %Tests the MockReplyBuilder::withRedirect() method.
 */
void MockReplyTest::testWithRedirect()
{
	QFETCH( QUrl, redirectTarget );
	QFETCH( int, statusCode );

	MockReplyBuilder builder;
	builder.withRedirect( redirectTarget, static_cast<HttpStatus::Code>( statusCode ) );

	const QUrl reqTarget( "http://foo.bar/test" );
	const auto request = Request( QNetworkRequest( reqTarget ) );
	const auto connection = createConnection( request );
	const auto reply = builder.createReply();

	QSignalSpy metaDataChangedSpy( reply.get(), &QNetworkReply::metaDataChanged );

	const QUrl locationHeader = QUrl::fromEncoded( reply->rawHeader( HttpUtils::locationHeader() ), QUrl::StrictMode );
	QCOMPARE( locationHeader, redirectTarget );
	QCOMPARE( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt(), statusCode );


	MockReplyTestHelper::callSimulate( reply.get(), request, connection.get() );
	QTRY_VERIFY( !metaDataChangedSpy.isEmpty() );

	QCOMPARE( locationHeader, redirectTarget );
	QCOMPARE( reply->attribute( QNetworkRequest::RedirectionTargetAttribute ).toUrl(), redirectTarget );
}

/*! Provides the data for the testWithRedirect() test.
 */
void MockReplyTest::testWithRedirect_data()
{
	QTest::addColumn<QUrl>( "redirectTarget" );
	QTest::addColumn<int>( "statusCode" );

	//                                    // redirectTarget               // statusCode
	QTest::newRow( "relative url" )       << QUrl( "see/other" )          << static_cast<int>( HttpStatus::MovedPermanently );
	QTest::newRow( "absolute url" )       << QUrl( "http://example.com" ) << static_cast<int>( HttpStatus::MovedPermanently );
	QTest::newRow( "temporary redirect" ) << QUrl( "http://example.com" ) << static_cast<int>( HttpStatus::TemporaryRedirect );
}

/*! \test %Tests the MockReplyBuilder::withHeader() method.
 */
void MockReplyTest::testWithHeader()
{
	QFETCH( QNetworkRequest::KnownHeaders, header );
	QFETCH( QVariant, value );

	MockReplyBuilder builder;
	builder.withHeader( header, value );

	MockReplyUniquePtr reply( builder.createReply() );

	QCOMPARE( reply->header( header ), value );
}

/*! Provides the data for the testWithHeader() test.
 */
void MockReplyTest::testWithHeader_data()
{
	QTest::addColumn<QNetworkRequest::KnownHeaders>( "header" );
	QTest::addColumn<QVariant>( "value" );

	QTest::newRow( "content type" )      << QNetworkRequest::ContentTypeHeader   << QVariant( "text/plain" );
	QTest::newRow( "content length" )    << QNetworkRequest::ContentLengthHeader << QVariant::fromValue( 42 );
	QTest::newRow( "null value" )        << QNetworkRequest::LocationHeader      << QVariant();
	QTest::newRow( "relative location" ) << QNetworkRequest::LocationHeader      << QVariant( QUrl( "//example.com" ) );
}

/*! Provides the data for the testWithRawHeader() test.
 */
void MockReplyTest::testWithRawHeader_data()
{
	QTest::addColumn<QByteArray>("header");
	QTest::addColumn<QByteArray>("value");

	QTest::newRow("content type")    << QByteArray("Content-Type") << QByteArray("text/plain");
	QTest::newRow("case is ignored") << QByteArray("conTent-Type") << QByteArray("text/plain");
	QTest::newRow("custom header")   << QByteArray("X-Foo") << QByteArray("17");
	QTest::newRow("null value")      << QByteArray("Content-Length") << QByteArray();
	QTest::newRow("empty value")     << QByteArray("Location") << QByteArray("");
}

/*! \test %Tests the MockReplyBuilder::withRawHeader() method.
 */
void MockReplyTest::testWithRawHeader()
{
	QFETCH(QByteArray, header);
	QFETCH(QByteArray, value);

	MockReplyBuilder builder;
	builder.withRawHeader(header, value);

	MockReplyUniquePtr reply(builder.createReply());

	QCOMPARE(reply->rawHeader(header), value);
}

/*! Provides the data for the testWithAttribute() test.
 */
void MockReplyTest::testWithAttribute_data()
{
	QTest::addColumn<QNetworkRequest::Attribute>("attribute");
	QTest::addColumn<QVariant>("value");

	QTest::newRow("status code") << QNetworkRequest::HttpStatusCodeAttribute   << QVariant::fromValue(200);
	QTest::newRow("null value")  << QNetworkRequest::HttpReasonPhraseAttribute << QVariant();
	QTest::newRow("empty value") << QNetworkRequest::HttpReasonPhraseAttribute << QVariant("");
}

/*! \test %Tests the MockReplyBuilder::withAttribute() method.
 */
void MockReplyTest::testWithAttribute()
{
	QFETCH(QNetworkRequest::Attribute, attribute);
	QFETCH(QVariant, value);

	MockReplyBuilder builder;
	builder.withAttribute(attribute, value);

	MockReplyUniquePtr reply(builder.createReply());

	QCOMPARE(reply->attribute(attribute), value);
}


/*! \test %Tests the MockReplyBuilder::withAuthenticate() method.
 */
void MockReplyTest::testWithAuthenticate()
{
	QFETCH( QString, realm );

	MockReplyBuilder builder;

	if ( QTest::currentDataTag() == QString( "invalid challenge" ) )
		QTest::ignoreMessage( QtWarningMsg, "Invalid authentication header: Missing value for parameter: \"realm\"" );

	builder.withAuthenticate( realm );

	MockReplyUniquePtr reply( builder.createReply() );

	QTEST( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt(), "statusCode" );
	QTEST( reply->rawHeader( "WWW-Authenticate" ), "authenticateHeader" );
}

/*! Provides the data for the testWithAuthenticate() test.
 */
void MockReplyTest::testWithAuthenticate_data()
{
	QTest::addColumn< QString >( "realm" );
	QTest::addColumn< int >( "statusCode" );
	QTest::addColumn< QByteArray >( "authenticateHeader" );

	QTest::newRow( "basic auth" )        << "world"   << static_cast< int >( HttpStatus::Unauthorized ) << QByteArray( "Basic realm=\"world\"" );
	QTest::newRow( "invalid challenge" ) << QString() << 0                                              << QByteArray();
}


/*! Provides the data for the testWithCookie() test.
 */
void MockReplyTest::testWithCookie_data()
{
	QTest::addColumn<QList<QNetworkCookie> >("cookies");

	const QByteArray cookieName("foo");

	QNetworkCookie sessionCookie(cookieName, "bar");
	sessionCookie.setPath("/foo");
	sessionCookie.setDomain(".example.com");

	QNetworkCookie expiringCookie("expiring", "tick tack");
	expiringCookie.setExpirationDate(QDateTime::currentDateTime().addDays(2));

	QNetworkCookie expiredCookie("expired", "boom");
	expiredCookie.setExpirationDate(QDateTime::currentDateTime().addSecs(-10));

	QNetworkCookie overridingCookie(cookieName, "overridden");
	overridingCookie.setPath("/foo");
	overridingCookie.setDomain(".example.com");


	//                                  // cookies
	QTest::newRow("session cookie")     << (QList<QNetworkCookie>() << sessionCookie);
	QTest::newRow("expiring cookie")    << (QList<QNetworkCookie>() << expiringCookie);
	QTest::newRow("expired cookie")     << (QList<QNetworkCookie>() << expiredCookie);
	QTest::newRow("multiple cookies")   << (QList<QNetworkCookie>() << sessionCookie << expiringCookie);
	QTest::newRow("overriding cookies") << (QList<QNetworkCookie>() << sessionCookie << overridingCookie);
}

/*! \test %Tests the MockReplyBuilder::withCookie() method.
 */
void MockReplyTest::testWithCookie()
{
	QFETCH(QList<QNetworkCookie>, cookies);

	MockReplyBuilder builder;
	QList<QNetworkCookie>::Iterator iter;
	for (iter = cookies.begin(); iter != cookies.end(); ++iter)
		builder.withCookie(*iter);

	MockReplyUniquePtr reply(builder.createReply());
	const QList<QNetworkCookie> actualCookies = reply->header(QNetworkRequest::SetCookieHeader).value<QList<QNetworkCookie> >();

	QCOMPARE(actualCookies, cookies);
}

/*! \test %Tests the MockReplyBuilder::withFinishDelayUntil() method.
 */
void MockReplyTest::testWithFinishDelayUntil()
{
	DummyObject helper;

	MockReplyBuilder builder;
	builder.withStatus( HttpStatus::OK ).withFinishDelayUntil( &helper, "dummySignal()" );

	const Request request( QNetworkRequest( QUrl( "http://example.com" ) ) );
	const auto connection = createConnection( request );
	const auto reply = builder.createReply();

	QSignalSpy finishedSpy( reply.get(), &QNetworkReply::finished );
	QSignalSpy metaDataChangedSpy( reply.get(), &QNetworkReply::metaDataChanged );
	MockReplyTestHelper::callSimulate( reply.get(), request, connection.get() );

	QTest::qWait( 100 );

	QVERIFY( finishedSpy.isEmpty() );
	QVERIFY( !reply->isFinished() );
	QCOMPARE( metaDataChangedSpy.size(), 1 );

	helper.dummySignal();

	QTRY_COMPARE_WITH_TIMEOUT( finishedSpy.size(), 1, 100 );
	QVERIFY( reply->isFinished() );
}

/*! \test %Tests the MockReplyBuilder::withFinishDelayUntil() method when the delay signal is emitted immediately.
 */
void MockReplyTest::testWithFinishDelayUntilImmediateEmit()
{
	DummyObject helper;

	MockReplyBuilder builder;
	builder.withStatus( HttpStatus::OK ).withFinishDelayUntil( &helper, "dummySignal()" );

	const Request request( QNetworkRequest( QUrl( "http://example.com" ) ) );
	const auto connection = createConnection( request );
	const auto reply = builder.createReply();

	QSignalSpy finishedSpy( reply.get(), &QNetworkReply::finished );
	QSignalSpy metaDataChangedSpy( reply.get(), &QNetworkReply::metaDataChanged );
	MockReplyTestHelper::callSimulate( reply.get(), request, connection.get() );

	QVERIFY( finishedSpy.isEmpty() );
	QVERIFY( !reply->isFinished() );

	helper.dummySignal();

	QTRY_COMPARE_WITH_TIMEOUT( finishedSpy.size(), 1, 100 );
	QCOMPARE( metaDataChangedSpy.size(), 1 );
	QVERIFY( reply->isFinished() );
}

/*! \test %Tests the MockReplyBuilder::withFinishDelayUntil() method when the delay signal is emitted at different times.
 */
void MockReplyTest::testWithFinishDelayUntilEmittedAtDifferentTimes()
{
	QFETCH( QMetaMethod, triggerSignal );

	DummyObject helper;
	const auto dummySignal = getSignal( DummyObject::staticMetaObject, "dummySignal()" );

	MockReplyBuilder builder;
	builder.withBody( "something" ).withFinishDelayUntil( &helper, dummySignal );

	const Request request( QNetworkRequest( QUrl( "https://example.com" ) ) );
	const auto connection = createConnection( request );
	const auto reply = builder.createReply();

	QSignalSpy finishedSpy( reply.get(), &QNetworkReply::finished );
	QSignalSpy metaDataChangedSpy( reply.get(), &QNetworkReply::metaDataChanged );
	MockReplyTestHelper::callSimulate( reply.get(), request, connection.get() );

	QObject::connect( reply.get(), triggerSignal, &helper, dummySignal );

	QVERIFY( finishedSpy.isEmpty() );
	QVERIFY( !reply->isFinished() );

	if( QTest::currentDataTag() == QStringLiteral( "after encrypted" ) )
		qt_noop();

	QTRY_COMPARE_WITH_TIMEOUT( finishedSpy.size(), 1, 100 );
	QVERIFY( reply->isFinished() );
}

/*! Provides the data for the testWithFinishDelayUntilEmittedAtDifferentTimes() test.
 */
void MockReplyTest::testWithFinishDelayUntilEmittedAtDifferentTimes_data()
{
	QTest::addColumn<QMetaMethod>( "triggerSignal" );

	//                                        // triggerSignal
	QTest::newRow( "after encrypted" )        << encryptedSignal;
	QTest::newRow( "after metaDataChanged" )  << metaDataChangedSignal;
	QTest::newRow( "after downloadProgress" ) << downloadProgressSignal;
}

/*! \test %Tests the MockReplyBuilder::withFinishDelayUntil() pointer to member function overload.
 */
void MockReplyTest::testWithFinishDelayUntilMemberPointer()
{
	DummyObject helper;

	MockReplyBuilder builder;
	builder.withFinishDelayUntil( &helper, "dummySignal()" );

	MockReplyBuilder builderWithMemberPointer;
	builderWithMemberPointer.withFinishDelayUntil( &helper, &DummyObject::dummySignal );

	QCOMPARE( builderWithMemberPointer, builder );
}


/*! \test %Tests the MockReplyBuilder::operator==() and MockReplyBuilder::operator!=().
 */
void MockReplyTest::testEqualityOperators()
{
	QFETCH( MockReplyBuilderPtr, left );
	QFETCH( MockReplyBuilderPtr, right );
	QFETCH( bool, equal );

	QVERIFY( *left  == *left );
	QVERIFY( *right == *right );
	QCOMPARE( *left  == *right, equal );
	QCOMPARE( *right == *left,  equal );
	QCOMPARE( *left  != *right, !equal );
	QCOMPARE( *right != *left,  !equal );
}

/*! Provides the data for the testOperatorEquals() test.
 */
void MockReplyTest::testEqualityOperators_data()
{
	QTest::addColumn< MockReplyBuilderPtr >( "left" );
	QTest::addColumn< MockReplyBuilderPtr >( "right" );
	QTest::addColumn< bool >( "equal" );

	MockReplyBuilderPtr nullBuilder( new MockReplyBuilder );
	MockReplyBuilderPtr nullBuilder2( new MockReplyBuilder );
	MockReplyBuilderPtr simpleBuilder( new MockReplyBuilder );
	simpleBuilder->withStatus( HttpStatus::OK );
	MockReplyBuilderPtr simpleBuilder2( new MockReplyBuilder );
	simpleBuilder2->withStatus( HttpStatus::OK );
	MockReplyBuilderPtr differentSimpleBuilder( new MockReplyBuilder );
	differentSimpleBuilder->withStatus( HttpStatus::Created );

	MockReplyBuilderPtr fullBuilder = HelperMethods::createFullBuilder();
	MockReplyBuilderPtr fullBuilder2 = HelperMethods::createFullBuilder();
	MockReplyBuilderPtr differentBodyBuilder = HelperMethods::createFullBuilder();
	differentBodyBuilder->withBody( "This is different" );
	MockReplyBuilderPtr differentHeaderBuilder = HelperMethods::createFullBuilder();
	differentHeaderBuilder->withHeader( QNetworkRequest::ContentTypeHeader, QByteArray( "something/different" ) );
	MockReplyBuilderPtr additionalHeaderBuilder = HelperMethods::createFullBuilder();
	additionalHeaderBuilder->withHeader( QNetworkRequest::ServerHeader, QByteArray( "TestServer" ) );
	MockReplyBuilderPtr differentRawHeaderBuilder = HelperMethods::createFullBuilder();
	differentRawHeaderBuilder->withRawHeader( customHeader, "different" );
	MockReplyBuilderPtr additionalRawHeaderBuilder = HelperMethods::createFullBuilder();
	additionalRawHeaderBuilder->withRawHeader( "X-X", "o-o" );
	MockReplyBuilderPtr differentAttributeBuilder = HelperMethods::createFullBuilder();
	differentAttributeBuilder->withAttribute( QNetworkRequest::HttpReasonPhraseAttribute, QByteArray( "Created" ) );
	MockReplyBuilderPtr additionalAttributeBuilder = HelperMethods::createFullBuilder();
	additionalAttributeBuilder->withAttribute( QNetworkRequest::RedirectionTargetAttribute, QUrl( "http://example.com" ) );

	MockReplyBuilderPtr errorBuilder = HelperMethods::createErrorBuilder();
	MockReplyBuilderPtr errorBuilder2 = HelperMethods::createErrorBuilder();
	MockReplyBuilderPtr differentErrorCodeBuilder = HelperMethods::createErrorBuilder();
	differentErrorCodeBuilder->withError( QNetworkReply::ContentAccessDenied, "Content resend" );
	MockReplyBuilderPtr differentErrorMessageBuilder = HelperMethods::createErrorBuilder();
	differentErrorMessageBuilder->withError( QNetworkReply::ContentReSendError, "Some other error message" );
	MockReplyBuilderPtr defaultErrorMessageBuilder = HelperMethods::createErrorBuilder();
	defaultErrorMessageBuilder->withError( QNetworkReply::ContentReSendError );


	//                                           // left                   // right                        // equal
	QTest::newRow( "equal simple builders" )     << simpleBuilder          << simpleBuilder2               << true;
	QTest::newRow( "equal full builders" )       << fullBuilder            << fullBuilder2                 << true;
	QTest::newRow( "equal error builders" )      << errorBuilder           << errorBuilder2                << true;
	QTest::newRow( "two null" )                  << nullBuilder            << nullBuilder2                 << true;
	QTest::newRow( "different simple builders" ) << simpleBuilder          << differentSimpleBuilder       << false;
	QTest::newRow( "different body" )            << fullBuilder            << differentBodyBuilder         << false;
	QTest::newRow( "different header" )          << fullBuilder            << differentHeaderBuilder       << false;
	QTest::newRow( "additional header" )         << fullBuilder            << additionalHeaderBuilder      << false;
	QTest::newRow( "different raw header" )      << fullBuilder            << differentRawHeaderBuilder    << false;
	QTest::newRow( "additional raw header" )     << fullBuilder            << additionalRawHeaderBuilder   << false;
	QTest::newRow( "different attribute" )       << fullBuilder            << differentAttributeBuilder    << false;
	QTest::newRow( "additional attribute" )      << fullBuilder            << additionalAttributeBuilder   << false;
	QTest::newRow( "different error code" )      << errorBuilder           << differentErrorCodeBuilder    << false;
	QTest::newRow( "different error message" )   << errorBuilder           << differentErrorMessageBuilder << false;
	QTest::newRow( "left null" )                 << nullBuilder            << simpleBuilder                << false;
	QTest::newRow( "right null" )                << simpleBuilder          << nullBuilder                  << false;
	QTest::newRow( "default error message" )     << errorBuilder           << defaultErrorMessageBuilder   << false;
}

void MockReplyTest::testErrorAndStatusCodeConsistencyWarning()
{
	QFETCH( QNetworkReply::NetworkError, error );
	QFETCH( HttpStatus::Code, statusCode );
	QFETCH( bool, expectWarning );

	const QRegularExpression expectedWarning( "HTTP status code and QNetworkReply::error\\(\\) do not match!.*" );

	MockReplyBuilder errorFirstBuilder;
	errorFirstBuilder.withError( error );

	if( expectWarning )
		QTest::ignoreMessage( QtWarningMsg, expectedWarning );
	errorFirstBuilder.withStatus( statusCode );

	MockReplyBuilder statusFirstBuilder;
	statusFirstBuilder.withStatus( statusCode );

	if( expectWarning )
		QTest::ignoreMessage( QtWarningMsg, expectedWarning );
	statusFirstBuilder.withError( error );
}

void MockReplyTest::testErrorAndStatusCodeConsistencyWarning_data()
{
	QTest::addColumn< QNetworkReply::NetworkError >( "error" );
	QTest::addColumn< HttpStatus::Code >( "statusCode" );
	QTest::addColumn< bool >( "expectWarning" );

	QTest::newRow( "no error with client error code" )                         << QNetworkReply::NoError              << HttpStatus::BadRequest << true;
	QTest::newRow( "client error with mismatching client error code" )         << QNetworkReply::ContentNotFoundError << HttpStatus::BadRequest << true;
	QTest::newRow( "client error with matching client error code" )            << QNetworkReply::ContentNotFoundError << HttpStatus::NotFound   << false;
	QTest::newRow( "protocol error with client error code" )                   << QNetworkReply::ProtocolFailure      << HttpStatus::BadRequest << true;
	QTest::newRow( "unknown client error with matching client error code" )    << QNetworkReply::UnknownContentError  << HttpStatus::Locked     << false;
	QTest::newRow( "unknown client error with mismatching client error code" ) << QNetworkReply::UnknownContentError  << HttpStatus::Forbidden  << true;
	QTest::newRow( "unknown server error with unknown client error code" ) << QNetworkReply::UnknownServerError   << HttpStatus::Locked  << true;
}

MockReplyBuilderPtr HelperMethods::createFullBuilder()
{
	MockReplyBuilderPtr fullBuilder(new MockReplyBuilder);
	fullBuilder->withStatus(HttpStatus::Created);
	fullBuilder->withBody("foo bar");
	fullBuilder->withHeader(QNetworkRequest::ContentTypeHeader, QByteArray("foo/bar"));
	fullBuilder->withRawHeader(customHeader, "woot");
	fullBuilder->withAttribute(QNetworkRequest::HttpReasonPhraseAttribute, QByteArray("Spawned"));
	return fullBuilder;
}

MockReplyBuilderPtr HelperMethods::createErrorBuilder()
{
	MockReplyBuilderPtr errorBuilder( new MockReplyBuilder );
	errorBuilder->withError( QNetworkReply::ContentReSendError, "Content resend" );
	errorBuilder->withBody( "Error" );
	return errorBuilder;
}

MockReplyBuilder HelperMethods::createErrorBuilder( QNetworkReply::NetworkError error )
{
	MockReplyBuilder errorBuilder;
	errorBuilder.withError( error );
	return errorBuilder;
}

HeaderHash HelperMethods::getKnownHeaders(QNetworkReply* reply)
{
	HeaderHash result;
	KnownHeadersSet headers;
	headers << QNetworkRequest::ContentTypeHeader
	        << QNetworkRequest::ContentLengthHeader
	        << QNetworkRequest::LocationHeader
	        << QNetworkRequest::LastModifiedHeader
	        << QNetworkRequest::CookieHeader
	        << QNetworkRequest::SetCookieHeader
	        << QNetworkRequest::ContentDispositionHeader
	        << QNetworkRequest::UserAgentHeader
	        << QNetworkRequest::ServerHeader;
#if QT_VERSION >= QT_VERSION_CHECK(5,12,0)
	headers << QNetworkRequest::IfModifiedSinceHeader
	        << QNetworkRequest::ETagHeader
	        << QNetworkRequest::IfMatchHeader
	        << QNetworkRequest::IfNoneMatchHeader;
#endif // Qt >= 5.12.0

	for (KnownHeadersSet::const_iterator iter = headers.cbegin(); iter != headers.cend(); ++iter)
		result.insert(*iter, reply->header(*iter));

	return result;
}

OptionalRegEx makeOptionalRegEx( const QString& pattern, QRegularExpression::PatternOptions options )
{
	return qMakePair( true, QRegularExpression( pattern, options ) );
}



} // namespace Tests

QTEST_MAIN(Tests::MockReplyTest)
#include "MockReplyTest.moc"
