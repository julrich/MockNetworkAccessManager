#ifndef MOCKNETWORKACCESSMANAGER_TESTS_TESTUTILS_HPP
#define MOCKNETWORKACCESSMANAGER_TESTS_TESTUTILS_HPP

#include "MockNetworkAccessManager.hpp"

#include "QSignalInspector.hpp"

#include <QList>
#include <QMetaMethod>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QPair>
#include <QRegularExpression>
#include <QTest>
#include <QVariant>

namespace Tests {

template< typename K, typename V >
QSet< K > keySet( const QHash< K, V >& hash )
{
	const auto keys = hash.keys();
#if QT_VERSION < QT_VERSION_CHECK( 5,14,0 )
	return keys.toSet();
#else
	return QSet< K >{ keys.begin(), keys.end() };
#endif
}

template< typename K, typename V >
QHash< K, V > pairListToHash( const QList< QPair< K, V > >& list )
{
#if QT_VERSION >= QT_VERSION_CHECK( 5,14,0 )
	return QHash< K, V >( list.cbegin(), list.cend() );
#else
	QHash< K, V > result;
	result.reserve( list.size() );
	for ( auto&& ele : list )
	{
		result.insert( ele.first, ele.second );
	}
	return result;
#endif
}

using SignalEmission = QPair< QMetaMethod, QVariantList >;

class SignalEmissionList : public QList< SignalEmission >
{
public:
	QStringList toStringList() const
	{
		QStringList stringList;
		for ( auto&& emission : *this )
		{
			const auto signalName = QString::fromUtf8( emission.first.name() );
			QStringList parameterList;
			for ( auto&& param : MockNetworkAccess::detail::asConst( emission.second ) )
				parameterList << param.toString();
			const auto invokationString = parameterList.isEmpty()
			                                  ? QStringLiteral( "%1()" ).arg( signalName )
			                                  : QStringLiteral( "%1( %2 )" ).arg( signalName, parameterList.join( ", " ) );
			stringList << invokationString;
		}
		return stringList;
	}

	inline SignalEmissionList& operator<<( const SignalEmission& signalEmission )
	{
		QList< SignalEmission >::operator<<( signalEmission );
		return *this;
	}
};

inline QMetaMethod getSignal( const QMetaObject& metaObj, const char* signal )
{
	const int signalIndex = metaObj.indexOfSignal( QMetaObject::normalizedSignature( signal ).data() );
	return metaObj.method( signalIndex );
}

inline SignalEmissionList getSignalEmissionListFromInspector( const QSignalInspector& inspector )
{
	static const auto signalBlacklist = ( QStringList() << "startHttpRequest"
	                                                    << "haveUploadData" );

	SignalEmissionList emissions;
	for ( auto&& emission : inspector )
	{
		if ( ! signalBlacklist.contains( emission.signal.name() ) )
			emissions << qMakePair( emission.signal, emission.parameters );
	}
	return emissions;
}

/*! Removes duplicate signals from a SignalEmissionList and keeps only the last one of the duplicates.
 *
 * Some signals (like QNetworkReply::downloadProgress()) are unreliable for real network requests.
 * This method allows removing all of them except for the last one.
 *
 * \param[in,out] emissionList The list of signal emission potentially containing duplicates.
 * \param signalName The name of the signal whose duplicates should be removed.
 */
inline void removeDuplicateSignals( SignalEmissionList& emissionList, const QByteArray& signalName )
{
	QVector< int > signalIndexes;
	for ( int i = 0; i < emissionList.size(); ++i )
	{
		if ( emissionList.at( i ).first.name() == signalName )
			signalIndexes << i;
	}

	if ( ! signalIndexes.isEmpty() )
		signalIndexes.removeLast();

	auto signalIndexIter = signalIndexes.constEnd();
	--signalIndexIter;
	for ( ; signalIndexIter >= signalIndexes.constBegin(); --signalIndexIter )
		emissionList.removeAt( *signalIndexIter );
}

class DummyNetworkAccessManager;

/*! Dummy QNetworkReply.
 * Helper class for the DummyNetworkAccessManager.
 */
class DummyReply : public QNetworkReply
{
	Q_OBJECT

	friend class DummyNetworkAccessManager;

public:
	/*! Creates a DummyReply object.
	 * \param parent Parent QObject.
	 */
	DummyReply( QObject* parent = Q_NULLPTR )
	    : QNetworkReply( parent )
	{
	}

public slots:
	virtual void abort() {}

protected:
	virtual qint64 readData( char*, qint64 )
	{
		return -1;
	}
};

const auto operationCanceledError = QNetworkReply::OperationCanceledError;

/*! Helper class for the MockNetworkAccessManagerTest.
 */
class DummyNetworkAccessManager : public QNetworkAccessManager
{
	Q_OBJECT

public:
	/*! Smart pointer to DummyNetworkAccessManager */
	typedef QSharedPointer< DummyNetworkAccessManager > Ptr;

	/*! Creates a DummyNetworkAccessManager
	 * \param parent Parent QObject
	 */
	DummyNetworkAccessManager( QObject* parent = Q_NULLPTR )
	    : QNetworkAccessManager( parent )
	{
	}

protected:
	/*! Records the request and creates a DummyReply.
	 * \param op The HTTP verb.
	 * \param req The QNetworkRequest object.
	 * \param ioDevice The body of the request.
	 * \return A DummyReply. Ownership is with the caller.
	 */
	virtual QNetworkReply* createRequest( QNetworkAccessManager::Operation op,
	                                      const QNetworkRequest& req,
	                                      QIODevice* ioDevice )
	{
		if ( req.url().scheme() == MockNetworkAccess::DataUrlUtils::dataScheme() )
			return QNetworkAccessManager::createRequest( op, req, ioDevice );

		MockNetworkAccess::Request request;
		request.operation = op;
		request.qRequest = req;
		if ( ioDevice )
			request.body = ioDevice->readAll();
		requests.append( request );

		auto* reply = new DummyReply( this );
		reply->setError( operationCanceledError, QString( "DummyReply" ) );
		reply->setRequest( request.qRequest );
		reply->setUrl( request.qRequest.url() );
		reply->setOperation( request.operation );
		reply->setSslConfiguration( request.qRequest.sslConfiguration() );
		reply->setOpenMode( QIODevice::ReadOnly );
		reply->setFinished( true );

		QMetaObject::invokeMethod( reply, "metaDataChanged", Qt::QueuedConnection );
#if QT_VERSION < QT_VERSION_CHECK( 5,15,0 )
		QMetaObject::invokeMethod( reply,
		                           "error",
		                           Qt::QueuedConnection,
		                           Q_ARG( QNetworkReply::NetworkError, operationCanceledError ) );
#else
		QMetaObject::invokeMethod( reply,
		                           "errorOccurred",
		                           Qt::QueuedConnection,
		                           Q_ARG( QNetworkReply::NetworkError, operationCanceledError ) );
#endif
		QMetaObject::invokeMethod( reply, "finished", Qt::QueuedConnection );

		return reply;
	}

public:
	/*! The requests received by this manager.
	 */
	QVector< MockNetworkAccess::Request > requests;
};

template< class MNAM >
std::unique_ptr< QNetworkReply > sendRequest( MNAM& mnam, const MockNetworkAccess::Request& request )
{
	QBuffer* bodyBuffer = Q_NULLPTR;
	std::unique_ptr< QNetworkReply > reply;
	switch ( request.operation )
	{
		case QNetworkAccessManager::GetOperation:
			reply.reset( mnam.get( request.qRequest ) );
			break;
		case QNetworkAccessManager::PostOperation:
			reply.reset( mnam.post( request.qRequest, request.body ) );
			break;
		case QNetworkAccessManager::PutOperation:
			reply.reset( mnam.put( request.qRequest, request.body ) );
			break;
		case QNetworkAccessManager::DeleteOperation:
			reply.reset( mnam.deleteResource( request.qRequest ) );
			break;
		case QNetworkAccessManager::HeadOperation:
			reply.reset( mnam.head( request.qRequest ) );
			break;
		case QNetworkAccessManager::CustomOperation:
		{
			if ( ! request.body.isEmpty() )
			{
				bodyBuffer = new QBuffer();
				bodyBuffer->setData( request.body );
				bodyBuffer->open( QIODevice::ReadOnly );
			}
			reply.reset( mnam.sendCustomRequest(
			    request.qRequest,
			    request.qRequest.attribute( QNetworkRequest::CustomVerbAttribute ).toByteArray(),
			    bodyBuffer ) );
			if ( bodyBuffer )
			{
				QObject::connect( reply.get(), SIGNAL( finished() ), bodyBuffer, SLOT( deleteLater() ) );
				bodyBuffer->setParent( reply.get() );
			}
			break;
		}
		default:
			return Q_NULLPTR;
	}

	auto* dummyReply = qobject_cast< DummyReply* >( reply.get() );
	if ( dummyReply )
	{
		if ( dummyReply->error() != QNetworkReply::NoError )
		{
#if QT_VERSION < QT_VERSION_CHECK( 5,15,0 )
			QMetaObject::invokeMethod( dummyReply,
			                           "error",
			                           Qt::QueuedConnection,
			                           Q_ARG( QNetworkReply::NetworkError, dummyReply->error() ) );
#else
			QMetaObject::invokeMethod( dummyReply,
			                           "errorOccurred",
			                           Qt::QueuedConnection,
			                           Q_ARG( QNetworkReply::NetworkError, dummyReply->error() ) );
#endif
		}
		if ( dummyReply->isFinished() )
			QMetaObject::invokeMethod( dummyReply, "finished", Qt::QueuedConnection );
	}
	return reply;
}

inline bool compareQRequestEssentials( const QNetworkRequest& left, const QNetworkRequest& right )
{
	using namespace MockNetworkAccess;

	if ( left.url() != right.url() )
		return false;
	if ( left.originatingObject() != right.originatingObject() )
		return false;
	if ( left.attribute( QNetworkRequest::CustomVerbAttribute ) != right.attribute( QNetworkRequest::CustomVerbAttribute ) )
		return false;
	const auto headersToCompare = KnownHeadersSet{ QNetworkRequest::ContentLengthHeader,
		                                           QNetworkRequest::ContentTypeHeader,
		                                           QNetworkRequest::CookieHeader };
	for ( auto&& header : headersToCompare )
	{
		if ( left.header( header ) != right.header( header ) )
			return false;
	}

	return true;
}

inline bool compareRequestEssentials( const MockNetworkAccess::Request& left, const MockNetworkAccess::Request& right )
{
	if ( left.operation != right.operation )
		return false;

	if ( ! compareQRequestEssentials( left.qRequest, right.qRequest ) )
		return false;

	if ( left.body != right.body )
		return false;

	qint64 timestampDiff = qAbs( left.timestamp.msecsTo( right.timestamp ) );
	if ( timestampDiff > 100 )
		return false;

	return true;
}

} // namespace Tests


namespace Tests {
inline char* toString( const Tests::SignalEmissionList& list )
{
	return qstrdup( list.toStringList().join( '\n' ).toLatin1().data() );
}

inline bool qCompare( const Tests::SignalEmissionList& actual,
                      const Tests::SignalEmissionList& expected,
                      const char* actualExpr,
                      const char* expectedExpr,
                      const char* file,
                      int line )
{
	const auto lineJoiner = QStringLiteral( "\n   " );
	if ( actual.isEmpty() != expected.isEmpty() )
	{
		QString message;
		if ( actual.isEmpty() )
			message = QStringLiteral( "Actual signals are empty. Expected signals were:" ) + lineJoiner
			          + expected.toStringList().join( lineJoiner );
		else
			message = QStringLiteral( "Actual signals are not empty but none were expected. Actual signals are:" )
			          + lineJoiner + actual.toStringList().join( lineJoiner );
		return QTest::compare_helper( false,
		                              message.toUtf8().constData(),
		                              nullptr,
		                              nullptr,
		                              actualExpr,
		                              expectedExpr,
		                              file,
		                              line );
	}

	bool equal = true;
	const bool haveEqualSize = actual.size() == expected.size();
	QStringList diffList;
	const auto commonSize = qMin( actual.size(), expected.size() );
	for ( int i = 0; i < commonSize; ++i )
	{
		if ( actual.at( i ) != expected.at( i ) )
		{
			equal = false;
			diffList.append( QStringLiteral( "<>" ) );
		}
		else
		{
			diffList.append( QString{} );
		}
	}

	if ( equal && haveEqualSize )
	{
		return QTest::compare_helper( true, nullptr, nullptr, nullptr, actualExpr, expectedExpr, file, line );
	}

	const auto actualList = actual.toStringList();
	const auto expectedList = expected.toStringList();

	int actualWidth = 0;
	for ( auto&& actual : actualList )
	{
		if ( actual.size() > actualWidth )
			actualWidth = actual.size();
	}

	const auto maxSize = qMax( actual.size(), expected.size() );
	for ( int i = 0; i < maxSize; ++i )
	{
		if ( i < diffList.size() )
		{
			diffList[ i ] = QStringLiteral( "%1 %2 %3" )
			                    .arg( actualList.at( i ), -1 * actualWidth )
			                    .arg( diffList.at( i ), 2 )
			                    .arg( expectedList.at( i ) );
		}
		else
		{
			diffList << QStringLiteral( "%1 <  %2" )
			                .arg( i < actual.size() ? actualList.at( i ) : QString{}, -1 * actualWidth )
			                .arg( i < expected.size() ? expectedList.at( i ) : QString{} );
		}
	}

	const auto message = QStringLiteral( "Actual and expected signals differ:%1%2    Expected%3%4    ********" )
	                         .arg( lineJoiner )
	                         .arg( "Actual", -1 * actualWidth )
	                         .arg( lineJoiner )
	                         .arg( "******", -1 * actualWidth )
	                     + lineJoiner + diffList.join( lineJoiner );
	return QTest::compare_helper( false, message.toUtf8().constData(), nullptr, nullptr, actualExpr, expectedExpr, file, line );
}

} // namespace Tests


inline char* toString( const QNetworkReply::RawHeaderPair& rawHeaderPair )
{
	return QTest::toString( rawHeaderPair.first + ": " + rawHeaderPair.second );
}

inline char* toString( const QNetworkRequest::Attribute& attribute )
{
	return QTest::toString( static_cast< int >( attribute ) );
}

namespace QTest {

template< template< typename > class ContainerType, typename ValueType >
inline QStringList containerToStringList( const ContainerType< ValueType >& container )
{
	QStringList result;
	for ( auto&& value : container )
	{
		char* keyStr = toString( value );
		if ( keyStr )
			result << QString::fromUtf8( keyStr );
		else
			result << QStringLiteral( "<null>" );
		delete[] keyStr;
	}
	return result;
}


template< typename KeyType, typename ValueType >
inline bool qCompare( const QHash< KeyType, ValueType >& actual,
                      const QHash< KeyType, ValueType >& expected,
                      const char* actualExpr,
                      const char* expectedExpr,
                      const char* file,
                      int line )
{
	bool failed = false;
	QString message;

	const auto actualKeys = ::Tests::keySet( actual );
	const auto expectedKeys = ::Tests::keySet( expected );
	const auto listSeparator = QStringLiteral( ", " );
	if ( ! failed )
	{
		// LCOV_EXCL_START
		if ( actualKeys != expectedKeys )
		{
			auto actualKeysStrings = containerToStringList( actualKeys );
			actualKeysStrings.sort();
			auto expectedKeysStrings = containerToStringList( expectedKeys );
			expectedKeysStrings.sort();

			message = QStringLiteral( "Compared hashes contain different keys.\n"
			                          "   Actual   (%1) unique keys: %2\n"
			                          "   Expected (%3) unique keys: %4" )
			              .arg( actualExpr )
			              .arg( actualKeysStrings.join( listSeparator ) )
			              .arg( expectedExpr )
			              .arg( expectedKeysStrings.join( listSeparator ) );
			failed = true;
		}
		// LCOV_EXCL_STOP
	}
	if ( ! failed )
	{
		for ( auto&& key : actualKeys )
		{
			const auto* keyStr = toString( key );
			const auto actualValue = actual.value( key );
			const auto expectedValue = expected.value( key );
			// LCOV_EXCL_START
			if ( actualValue != expectedValue )
			{
				message = QStringLiteral( "Compared hashes contain different values for key: %1\n"
				                          "   Actual   (%2) value: %3\n"
				                          "   Expected (%4) value: %5" )
				              .arg( keyStr ? keyStr : "<null>" )
				              .arg( actualExpr )
				              .arg( toString( actualValue ) )
				              .arg( expectedExpr )
				              .arg( toString( expectedValue ) );
				failed = true;
			}
			// LCOV_EXCL_STOP
			delete[] keyStr;
		}
	}
	return compare_helper( ! failed, message.toUtf8().constData(), Q_NULLPTR, Q_NULLPTR, actualExpr, expectedExpr, file, line );
}

} // namespace QTest

Q_DECLARE_METATYPE( Tests::SignalEmissionList )


#endif // MOCKNETWORKACCESSMANAGER_TESTS_TESTUTILS_HPP
