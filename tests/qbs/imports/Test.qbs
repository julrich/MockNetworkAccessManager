import qbs
import qbs.FileInfo

QtApplication {
	type: base.concat( [ "autotest" ] )

	Depends { name: "Qt"
		submodules: [
			"core",
			"network",
			"test",
		]
	}

	Depends { name: "MockNetworkAccessManager" }

	cpp.includePaths: [ ".." ]
	cpp.systemIncludePaths: {
		var sysIncludePaths = [ Qt.core.incPath ];
		if( Qt.core.frameworkBuild ) {
			[ 'Core', 'Network', 'Test' ].forEach( function( lib ) {
				sysIncludePaths.push( FileInfo.joinPaths( Qt.core.libPath, 'Qt' + lib + '.framework', 'Headers' ) );
			} );
		}
		return sysIncludePaths;
	}
	cpp.defines: [ "QT_DEPRECATED_WARNINGS" ]
	cpp.cxxLanguageVersion: [ 'c++11' ]
	cpp.treatWarningsAsErrors: true
	cpp.driverFlags: {
		if( qbs.configurationName === "sanitize" ) {
			if( qbs.toolchain.contains( "msvc" ) ) {
				return [ "/fsanitize=address" ];
			}
			if( qbs.toolchain.contains( "gcc" ) ) {
				return [ "-fsanitize=address,undefined,leak" ];
			}
		}
	}
	cpp.commonCompilerFlags: {
		if( qbs.configurationName === "sanitize" ) {
			if( qbs.toolchain.contains( "gcc" ) ) {
				return [
					"-fno-omit-frame-pointer",
					"-fno-sanitize-recover",
					"-fno-sanitize=alignment"
				];
			}
		}
	}

	cpp.rpaths: qbs.targetOS.contains( "unix" ) ? [ Qt.core.libPath ] : undefined

	files: [
		name + ".cpp"
	]
}