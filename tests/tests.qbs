import qbs
import qbs.Utilities

Project {

	Product {
		name: "hippomocks"

		Export {
			Depends { name: "cpp" }

			cpp.cxxFlags: qbs.toolchain.contains( "msvc" ) ? [ "/Ob0" ]
			            : qbs.toolchain.contains( "gcc" ) ? [ "-fno-inline" ]
			            : base
		}
	}

	StaticLibrary {
		name: "QSignalInspector"

		Depends { name: "Qt"
			submodules: [
				"core",
				"test",
			]
		}

		Export {
			Depends { name: "Qt"
				submodules: [
					"core",
					"test",
				]
			}
			Depends { name: "cpp" }

			cpp.includePaths: [ path ]
		}

		files: [
			"QSignalInspector.hpp"
		]
	}

	StaticLibrary {
		name: "TestUtils"

		Depends { name: "QSignalInspector" }
		Depends { name: "MockNetworkAccessManager" }
		Depends { name: "cpp" }
		Depends { name: "Qt"
			submodules: [
				"core",
				"test",
			]
		}

		Export {
			Depends { name: "QSignalInspector" }
			Depends { name: "MockNetworkAccessManager" }
			Depends { name: "cpp" }

			cpp.includePaths: [ path ]
		}

		files: [
			"TestUtils.hpp"
		]

	}

	Test {
		name: "BearerAuthenticationTest"
	}

	Test {
		name: "CompatibilityTest"

		cpp.defines: base.concat( [
			"QT_NO_CAST_TO_ASCII",
			"QT_NO_CAST_FROM_ASCII",
			"QT_NO_CAST_FROM_BYTEARRAY",
			"QT_NO_KEYWORDS",
		] )

		cpp.cxxFlags: qbs.toolchain.contains( "gcc" ) ? [ "-Wconversion" ] : base
		cpp.cxxLanguageVersion: {
			if ( Utilities.versionCompare( Qt.core.version, "6.0.0" ) < 0 )
				return "c++11";
			else if ( Utilities.versionCompare( Qt.core.version, "6.6.0" ) < 0 )
				return "c++14";
			else
				return "c++17";
		}
	}

	Test {
		name: "HttpAuthenticationTest"
	}

	Test {
		name: "HttpUtilsTest"
	}

	Test {
		name: "ManagerTest"

		Depends { name: "hippomocks" }
		Depends { name: "QSignalInspector" }
		Depends { name: "TestUtils" }
	}

	Test {
		name: "MockReplyTest"

		Depends { name: "QSignalInspector" }
		Depends { name: "TestUtils" }
	}

	Test {
		name: "MyNetworkClientTest"

		files: base.concat( [
			"MyNetworkClient.hpp"
		] )
	}

	Test {
		name: "PredicateTest"
	}

	Test {
		name: "RuleTest"
	}

	Test {
		name: "VersionNumberTest"
	}

	QtApplication {
		name: "QNetworkReplyBehaviorTest"

		Depends { name: "QSignalInspector" }
		Depends { name: "Qt"
			submodules: [
				"core",
				"network"
			]
		}
		Depends { name: "cpp" }

		files: "QNetworkReplyBehaviorTest.cpp"
	}

}