import qbs
import qbs.Environment

Project {

	qbsSearchPaths: [
		"qbs"
	]

	references: [
		"tests.qbs"
	]

	AutotestRunner {
		Depends { name: "Qt.core" }
		Depends { name: "cpp" }
		environment: {
			if ( qbs.targetOS.contains( "windows" ) ) {
				return [ "PATH=" + Qt.core.binPath + qbs.pathListSeparator + cpp.toolchainInstallPath + qbs.pathListSeparator + Environment.getEnv( "PATH" ) ];
			}
			return base;
		}
	}

}